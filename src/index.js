import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {Provider} from 'react-redux'
import store from './store'
import * as serviceWorker from './serviceWorker';
import theme from './theme'
// Using the createMuiTheme() method, we can override the default theme
const themes = createMuiTheme({
  palette: {
    primary: {
      main: '#ffffff',
    },
    secondary: {
      main: '#14213d',
    },
    error: {
      main: '#470021',
    },
    warning: {
      main: '#fca311',
    },
    info: {
      main: '#e5e5e5',
    },
    success: {
      main: '#004726',
    },
    background: {
      default: '#f1faee',
    },
  },
  overrides: {
    MuiFormHelperText: {
      contained: {
        marginLeft: 0,
        marginRight: 0,
      },
    }
  }
});

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {/* The rest of the application */}
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
