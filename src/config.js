export const BASE_URL = 'http://13.250.113.12:8080/cloudcrm/';
export const hintQuestions = [
    {title:'What is the name of your first school?',value:1},
    {title:'What is your birth Place?',value:2},
    {title:'What is your favourite food?',value:3},
    {title:'What is your favourite game?',value:4},
    {title:'What is your favourite movie?',value:5},
    {title:'What is your favourite pass-time?',value:6},
    {title:"What is your Mother's Maiden Name?",value:7},
    {title:'What was your favorite place to visit as a child?',value:8},
    {title:'What is the country of your ultimate dream vacation?',value:9},
    {title:'What is the name of your favorite childhood teacher?',value:10},
    {title:'To what city did you go on your honeymoon?',value:11},
    {title:'What time of the day were you born?',value:12},
    {title:'What was your dream job as a child?',value:13},
    {title:'What are the last 5 digits of your driving license number?',value:14},
    {title:'What month and day is your anniversary? (e.g., January 2)',value:15},
    {title:"What is your grandmother's first name?",value:16},
    {title:'Where did you vacation last year?',value:17},
    {title:'What is your preferred musical genre?',value:18}
]