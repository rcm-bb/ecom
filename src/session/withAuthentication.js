import React from 'react';

import AuthUserContext from './context';



const withAUthentication = Component => {
  class WithAUthentication extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        authUser: JSON.parse(localStorage.getItem('authUser')),
      };
    }

    componentDidMount() {
      
    }
  
    componentWillUnmount() {
      // this.listener();
    }

    render() {
      const { authUser } = this.state;

      return (
        <AuthUserContext.Provider value={authUser}>
          <Component {...this.props} />
        </AuthUserContext.Provider>
      );
    }
  }

  return WithAUthentication;
}

export default withAUthentication;