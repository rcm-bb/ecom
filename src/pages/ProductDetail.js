import React from 'react'
import clsx from 'clsx'
import {Link} from 'react-router-dom'
import { Container, Grid, Typography, Hidden, Button } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { makeStyles, useTheme,withStyles } from '@material-ui/core/styles'
import ProductOptionSelector from './../components/option/ProductOptionSelector'
import client from './../utils/client'
import Row from './../components/Row'
import { Hbox } from './../components/Box'
import Label from './../components/Label'
import TabPanel from './../components/TabPanel'
import CmsSlot from './../components/CmsSlot'
import Lazy from './../components/Lazy'
import SuggestedProducts from './../components/product/SuggestedProducts'
import QuantitySelector from './../components/QuantitySelector'
import MediaCarousel from './../components/Carousel/MediaCarousel'
import SpinnerButton from '../components/SpinnerButton'
import store from './../store'
import { setCartCount } from '../store/actions';
import CommonSnackbar from './../components/Snackbar';
import { isAuthenticated } from '../utils/auth'
import cookie from 'react-cookies'

const styles = theme => ({
    carousel: {
      [theme.breakpoints.down('xs')]: {
        margin: theme.spacing(0, -2),
        width: '100vw',
      },
    },
    lightboxCarousel: {
      [theme.breakpoints.down('xs')]: {
        margin: 0,
        width: '100%',
      },
    },
    confirmation: {
      padding: '2px 0',
    },
    dockedSnack: {
      [theme.breakpoints.down('xs')]: {
        left: '0',
        bottom: '0',
        right: '0',
      },
    },
    docked: {
      [theme.breakpoints.down('xs')]: {
        fontSize: theme.typography.subtitle1.fontSize,
        padding: `${theme.spacing(2)}px`,
        position: 'fixed',
        left: 0,
        bottom: 0,
        width: '100%',
        zIndex: 10,
        borderRadius: '0',
      },
    },
    noShadow: {
      [theme.breakpoints.down('xs')]: {
        boxShadow: 'none',
      },
    },
  })


  const dummyProduct = {
    id: '1',
    url: '/p/1',
    name: 'Product 1',
    price: 10.99,
    priceText: '$10.99',
    rating: 4.5,
    colors: [
      {
        disabled: false,
        id: "red",
        image: { src: "https://via.placeholder.com/48x48/f44336?text=%20", alt: "red" },
        media: {
          full: [
            {
              alt: "Product 1",
              magnify: { height: 1200, width: 1200, src: "https://via.placeholder.com/1200x1200/f44336/ffffff?text=Product%201" },
              src: "https://via.placeholder.com/600x600/f44336/ffffff?text=Product%201"
            }
          ],
          thumbnail: { src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSV2xad0CRt03g90EcMwRYtuEg9zG80o7hs6Q&usqp=CAU", alt: "Product 1" },
          thumbnails: [
            {
              alt: "red",
              src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSV2xad0CRt03g90EcMwRYtuEg9zG80o7hs6Q&usqp=CAU"
            }
          ]
        },
      },
      {
        disabled: false,
        id: "green",
        image: { src: "https://via.placeholder.com/48x48/4caf50?text=%20", alt: "green" },
        media: {
          full: [
            {
              alt: "Product 1",
              magnify: { height: 1200, width: 1200, src: "https://via.placeholder.com/1200x1200/4caf50/ffffff?text=Product%201" },
              src: "https://via.placeholder.com/600x600/4caf50/ffffff?text=Product%201"
            },
            {
              alt: "Product 1",
              magnify:{
              height: 800,width: 1200,src: "https://via.placeholder.com/1200x800/4caf50/ffffff?text=Product%201"},
              src: "https://via.placeholder.com/600x400/4caf50/ffffff?text=Product%201"
            }
          ],
          thumbnail: {src: "https://via.placeholder.com/400x400/4caf50/ffffff?text=Product%201", alt: "Product 1" },
          thumbnails: [
            {
              alt: "green",
              src: "https://via.placeholder.com/400x400/4caf50/ffffff?text=Product%201"
            }
          ]
        },
      }
    ],
    media: {
      full: [
        {
          alt: "Product 1",
          magnify: { height: 1200, width: 1200, src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ8A3aBSCZnL0d9UeU3DxpNUnDJwSFOlEkDIw&usqp=CAU" },
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ8A3aBSCZnL0d9UeU3DxpNUnDJwSFOlEkDIw&usqp=CAU"
        },
        {
          alt: "Product 2",
          magnify: { height: 1200, width: 1200, src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuWksd9LBPyHJR7H1ZZyEEP_q2yERrnfiLJw&usqp=CAU" },
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuWksd9LBPyHJR7H1ZZyEEP_q2yERrnfiLJw&usqp=CAU"
        },
        {
          alt: "Product 3",
          magnify: { height: 1200, width: 1200, src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTwYeitU1mwl7K7AHSWiviuC7qnK3SpaPAaxA&usqp=CAU" },
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTwYeitU1mwl7K7AHSWiviuC7qnK3SpaPAaxA&usqp=CAU"
        }
      ],
      thumbnails: [
        {
          alt: "Product 1",
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ8A3aBSCZnL0d9UeU3DxpNUnDJwSFOlEkDIw&usqp=CAU"
        },
        {
          alt: "Product 2",
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQuWksd9LBPyHJR7H1ZZyEEP_q2yERrnfiLJw&usqp=CAU"
        },
        {
          alt: "Product 3",
          src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTwYeitU1mwl7K7AHSWiviuC7qnK3SpaPAaxA&usqp=CAU"
        }
      ]
    },

    sizes: [
        {id: "XS", text: "XS", disabled: false, price: 99.99},
        {id: "S", text: "S", disabled: false, price: 99.99},
        {id: "M", text: "M", disabled: false, price: 99.99},
        {id: "L", text: "L", disabled: false, price: 109.99},
        {id: "XL", text: "XL", disabled: false, price: 109.99},
    ],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    thumbnail: { src: "https://via.placeholder.com/400x400/4caf50/ffffff?text=Product%201", alt: "Product 1" },
    specs: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}


class Product extends React.Component{
    constructor(){
        super();
        this.state = {
            loading:true,
            product:{},
            addToCartInProgress:false,
            dialog:false,
            quantity:1
        }
        this._snackbar = null
        this.isAuth = isAuthenticated();
        let cartId = cookie.load('clid');
        this.cartId = cartId ? cartId:0;
    }

    componentWillMount(){
        this.fetchProduct();
    }

    openModal = () =>  this.setState({openModal:true})
  
    closeModal = () =>  this.setState({openModal:false})

    async fetchProduct(){
        const {match} = this.props;
        const {objectMap} = await client.get(`pu/getProductRE?id=${match.params.id}`);
        this.setState({
            product:{
                ...dummyProduct,
                ...objectMap.material
            },
            loading:false
        })
    }

    componentWillReceiveProps(nextProps){
        if(this.props.match.params && nextProps.match.params && this.props.match.params != nextProps.match.params){
            this.setState({ loading: true, product:{} }, () => this.fetchProduct());
        }
    }

    handleSubmit = async event => {
        event.preventDefault();
    }

    addToCart = async () => {
      let {product} = this.state;
      this.setState({addToCartInProgress:true})
      try{
        let url = this.isAuth ? `pu/storeMaterialsToCart/${product.id}/1`:`pu/storeMaterialsToCartForNewUser/${product.id}/1/${this.cartId}`;
         await client.post(url)
        this.setState({addToCartInProgress:false})
        store.dispatch(setCartCount('increase'))
        this._snackbar.openSnack('Item added in your card successfully')
      }catch(err){
        this.setState({addToCartInProgress:false})
        this._snackbar.openSnack('Item not added in your card','error')
      }
    }

    
    header = () => {
        const {theme} = this.props;
        const {product} = this.state 
        return (
            <Row>
              <Typography variant="h6" component="h1" gutterBottom>
                {product ? product.name : <Skeleton style={{ height: '1em' }} />}
              </Typography>
              <Hbox>
                <Typography style={{ marginRight: theme.spacing(2) }}>{product.priceText}</Typography>
              </Hbox>
            </Row>
        )
    }

    addToCartConfirmation = () => this.openModal();

    render(){
        const {classes,theme} = this.props;
        const {product,loading,addToCartInProgress,openModal} = this.state;
        const color = product.color|| {}
        const size = product.size || {}
        const quantity = product.quantity;
        return (
            <Container maxWidth="lg" style={{ paddingTop: theme.spacing(2) }}>
             <CommonSnackbar ref={ref => this._snackbar = ref} />
            
    
          <Grid container spacing={4}>
                  <Grid item xs={12} sm={6} md={5}>
                    <Hidden implementation="css" smUp>
                      {this.header()}
                    </Hidden>
                    <MediaCarousel 
                      className={classes.carousel}
                      lightboxClassName={classes.lightboxCarousel}
                      height="100%"
                      media={product.media}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={7}>
                    <Grid container spacing={4}>
                      <Grid item xs={12}>
                        <Hidden implementation="css" xsDown>
                          <div style={{ paddingBottom: theme.spacing(1) }}>{this.header()}</div>
                        </Hidden>
                        {!loading ? (
                          <>
                            <Hbox style={{ marginBottom: 10 }}>
                              <Label>COLOR: </Label>
                              <Typography>{color.text}</Typography>
                            </Hbox>
                            <ProductOptionSelector
                              options={product.colors}
                              value={color}
                              onChange={value =>
                                this.setState({ product: { ...this.state.product, color: value } })
                              }
                              strikeThroughDisabled
                              optionProps={{
                                showLabel: false,
                              }}
                            />
                          </>
                        ) : (
                            <div>
                              <Skeleton style={{ height: 14, marginBottom: theme.spacing(2) }}></Skeleton>
                              <Hbox>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                              </Hbox>
                            </div>
                          )}
                      </Grid>
                      <Grid item xs={12}>
                        {!loading ? (
                          <>
                            <Hbox style={{ marginBottom: 10 }}>
                              <Label>SIZE: </Label>
                              <Typography>{size.text}</Typography>
                            </Hbox>
                            <ProductOptionSelector
                              options={product.sizes}
                              value={size}
                              strikeThroughDisabled
                              onChange={value =>
                                this.setState({  product: { ...this.state.product, size: value } })
                              }
                            />
                          </>
                        ) : (
                            <div>
                              <Skeleton style={{ height: 14, marginBottom: theme.spacing(2) }}></Skeleton>
                              <Hbox>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                                <Skeleton style={{ height: 48, width: 48, marginRight: 10 }}></Skeleton>
                              </Hbox>
                            </div>
                          )}
                      </Grid>
                      <Grid item xs={12}>
                        <Hbox>
                          <Label>QTY:</Label>
                          <QuantitySelector
                            value={quantity}
                            onChange={value =>
                              this.setState({ product: { ...this.state.product, quantity: value } })
                            }
                          />
                        </Hbox>
                      </Grid>
                      <Grid item xs={12}>

                        <SpinnerButton title="Add To Cart" loading={addToCartInProgress} handleButtonClick={this.addToCart}  />
                        
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>

            <Grid item xs={12}>
                <TabPanel>
                <CmsSlot label="Description">{product.description}</CmsSlot>
                <CmsSlot label="Specs">{product.specs}</CmsSlot>
                </TabPanel>
            </Grid>

            <Grid item xs={12}>
                <Lazy style={{ minHeight: 285 }}>
                <SuggestedProducts product={product} />
                </Lazy>
            </Grid>

            </Container>
        )
    }
}

export default withStyles(styles,{withTheme:true})(Product)