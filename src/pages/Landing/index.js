import React, { Component, Suspense } from 'react';
import { AuthUserContext } from '../../session';
import * as ROUTES from '../../constants/routes';
import ImageSlider from './../../components/ImageSlider';
import { Box, Grid, Paper } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  width: {
    width: '100%'
  },
  m:{
    margin: '2%'
  },
  rootColor:{
    backgroundColor:'#eeeeee'
  }
});
const ImageBox = React.lazy(() => import('./../../components/ImageBox'));

const DataImage = [
  {
    count: 30, images: [
      'https://storage.sg.content-cdn.io/cdn-cgi/image/width=1000,height=1500,quality=75,format=auto/in-resources/6c57599f-2c43-4c82-806a-e07c3410f5d3/Images/ProductImages/Source/skd6034aw19lbeg-1.jpg',
      'https://storage.sg.content-cdn.io/cdn-cgi/image/width=undefined,height=undefined,quality=75,format=auto/in-resources/6c57599f-2c43-4c82-806a-e07c3410f5d3/Images/ProductImages/Source/skd6034aw19lbeg-6.jpg',
      'https://images-na.ssl-images-amazon.com/images/I/81CNw-ZzCwL._UL1500_.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/7146642/2018/8/14/a9423310-3ac3-4fac-9051-e811ec5cb5241534244230690-StyleStone-Womens-Black-and-White-Check-Shirt-Dress-with-Belt-6671534244230459-3.jpg'
    ]
  },
  {
    count: 48, images: [
      'https://static.cilory.com/427671-thickbox_default/estonished-maroon-sequins-party-dress.jpg',
      'https://static.cilory.com/427678-thickbox_default/estonished-cutout-back-sequin-party-dress.jpg',
      'https://static.cilory.com/373091-thickbox_default/estonished-silver-black-sequin-mini-dress.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/7146642/2018/8/14/a9423310-3ac3-4fac-9051-e811ec5cb5241534244230690-StyleStone-Womens-Black-and-White-Check-Shirt-Dress-with-Belt-6671534244230459-3.jpg'
    ]
  },
  {
    count: 29, images: [
      'https://static.cilory.com/430532-thickbox_default/estonished-sequins-party-dress.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/11005736/2019/11/26/f1852bb3-1264-46c7-8ea7-726529f38aed1574763802903-SIRIKIT-Women-Dresses-4021574763801741-1.jpg',
      'https://dynamic.zacdn.com/bsu20GLxCLhKfJ_2dUQ3u4wCIlg=/fit-in/762x1100/filters:quality(95):fill(ffffff)/http://static.sg.zalora.net/p/crystal-korea-fashion-6340-4439711-1.jpg'
    ]
  },
  {
    count: 29, images: [
      'https://static.cilory.com/430532-thickbox_default/estonished-sequins-party-dress.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/11005736/2019/11/26/f1852bb3-1264-46c7-8ea7-726529f38aed1574763802903-SIRIKIT-Women-Dresses-4021574763801741-1.jpg',
      'https://dynamic.zacdn.com/bsu20GLxCLhKfJ_2dUQ3u4wCIlg=/fit-in/762x1100/filters:quality(95):fill(ffffff)/http://static.sg.zalora.net/p/crystal-korea-fashion-6340-4439711-1.jpg'
    ]
  },
  {
    count: 29, images: [
      'https://static.cilory.com/430532-thickbox_default/estonished-sequins-party-dress.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/11005736/2019/11/26/f1852bb3-1264-46c7-8ea7-726529f38aed1574763802903-SIRIKIT-Women-Dresses-4021574763801741-1.jpg',
      'https://dynamic.zacdn.com/bsu20GLxCLhKfJ_2dUQ3u4wCIlg=/fit-in/762x1100/filters:quality(95):fill(ffffff)/http://static.sg.zalora.net/p/crystal-korea-fashion-6340-4439711-1.jpg'
    ]
  },
  {
    count: 29, images: [
      'https://static.cilory.com/430532-thickbox_default/estonished-sequins-party-dress.jpg',
      'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/11005736/2019/11/26/f1852bb3-1264-46c7-8ea7-726529f38aed1574763802903-SIRIKIT-Women-Dresses-4021574763801741-1.jpg',
      'https://dynamic.zacdn.com/bsu20GLxCLhKfJ_2dUQ3u4wCIlg=/fit-in/762x1100/filters:quality(95):fill(ffffff)/http://static.sg.zalora.net/p/crystal-korea-fashion-6340-4439711-1.jpg'
    ]
  }
]

class Landing extends Component {
  static contextType = AuthUserContext;
  componentDidMount() {
    // If signed in, redirect to Home
    let authUser = this.context;
    authUser && this.props.history.push(ROUTES.HOME);
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.rootColor} >
        <ImageSlider />
        <Grid  justify="center" container>
          {
            DataImage.map((item, index) => (
              <Grid className={classes.m} xs={12} sm={6} md={3} key={index} item>
                 <ImageBox images={item.images} />
              </Grid>
            ))
          }
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Landing);
