import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import SpinnerButton from './../../components/SpinnerButton';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField,
  Checkbox,
  Button,
  FormControlLabel
} from '@material-ui/core';
import {isEmpty,unset} from 'lodash'
import validate from 'validate.js';
import client from './../../utils/client';
import CommonSnackbar from './../../components/Snackbar';
import { hintQuestions } from '../../config';
import SelectBox from '../../components/Form/SelectBox';
import TextFieldBox from './../../components/Form/TextFieldBox';
import FileUploadBox from './../../components/Form/FileUploadBox';
import DialogBox from './../../components/Common/DialogBox';

const schema = {
  gender:{
    presence:{ allowEmpty:false, message:'is required' }
  }
}

const Genders = [
    {title:'Male',value:'M'},
    {title:'Female',value:'F'},
    {title:'Transgender',value:'T'}
];

const MaritalStatus = [
    {title:'Married',value:'Y'},
    {title:'Unmarried',value:'N'}
];

const NameTitles = [
    {title:'Mr.',value:'Mr.'},
    {title:'Miss',value:'Miss'},
    {title:'Mrs.',value:'Mrs.'}
];

const IdProofs = [
    {title:'Adhar Card',value:18},
    {title:'Central/State Government certified ID proof', value:16},
    {title:'Certification from any of the Authorities', value:17},
    {title:'Domicile certificate with communication address and photograph',value:15},
    {title:'Driving License (Valid)',value:10},
    {title:'Pan Card',value:11},
    {title:'Passport (Valid)',value:13},
    {title:'Ration Card With Photogarh',value:20},
    {title:"Voter's Identity Card",value:12},
    {title:'Written confirmation from the banks certifying identity proof',value:14}
];

const addressProofs = [
    {title:'Adhar Card',value:12},
    {title:'Bank account/Passbook With Photograph',value:15},
    {title:'Central/State Government certified Address proof.',value:21},
    {title:'Current employer’s certificate mentioning residence',value:19},
    {title:'Domicile certificate with communication address and photograph',value:20},
    {title:'Driving License (Valid)',value:13},
    {title:'Electricity bill not older than 6 months',value:23},
    {title:'Electricity bill not older than six months',value:11},
    {title:'Lease agreement along with rent receipt not older than 3 months',value:18},
    {title:'Passport (Valid)',value:12},
    {title:'Ration Card',value:10},
    {title:'Telephone bill not older than 6 months',value:14},
    {title:"Voter's Identity Card",value:16},
    {title:'Written confirmation from the banks (Attested by Bank)',value:17}
]

const AddressComponent = () => {
  return (
    <>

        <Grid item md={6}  xs={12} >

        <TextFieldBox name="maritalStatus" label="Room Number/Building"
        multiline
        rows={2}
        // error={this.hasError('maritalStatus')}
        />

        </Grid>

        <Grid item md={6}  xs={12} >

        <TextFieldBox name="nameTitle" label="Area/Locality"
         multiline
         rows={2}
        // error={this.hasError('nameTitle')}
        />

        </Grid>

        <Grid item md={6}  xs={12} >

        <TextFieldBox name="maritalStatus" label="State"
        // error={this.hasError('maritalStatus')}
        />

        </Grid>

        <Grid item md={6}  xs={12} >

        <TextFieldBox name="nameTitle" label="District"
        // error={this.hasError('nameTitle')}
        />

        </Grid>


        <Grid item md={6}  xs={12} >

        <TextFieldBox name="maritalStatus" label="City"
        // error={this.hasError('maritalStatus')}
        />

        </Grid>

        <Grid item md={6}  xs={12} >

        <TextFieldBox name="nameTitle" label="Pincode"
        // error={this.hasError('nameTitle')}
        />

        </Grid>

    </>
  )
}


const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    },
    formControl: {
        width: '100%',
    },
});

class KYCApplication extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:false,
            errors:{},
            termsDialog:false
        }
        this._snackbar = null;
    }

    onSubmit = async () => {

      const {form} = this.state;
      let errors = validate(form,schema);

      errors = isEmpty(errors) ? {}:errors;
      if(!isEmpty(errors)) return this.setState({errors});
      console.log('on submit',form,errors);
      this.setState({errors})

        // const {refId, hintAnswer,hintQuestion} = this.state.form;
        // let errors = validate(this.state.form,schema);
        // errors = isEmpty(errors) ? {}:errors;
        // if(!isEmpty(errors)) return this.setState({errors});
        // this.setState({loading:true,errors})
        // const res = await client.post(`pu/forgotRefPassword/${refId}/${hintQuestion}/${hintAnswer}`);
        // console.log('res',res)
        // if(res.status == true){
        //     this._snackbar.openSnack('data fetch successfully')
        // }else{
        //     this._snackbar.openSnack(res.message,'error')
        // }
        // this.setState({loading:false})
    }

    onInputChange = (name,event) => {
      const {form,errors} = this.state;
      unset(errors,name);
      this.setState({
        form:{
          ...form,
          [name]: event.target.value
        },
        errors
      })
    }

    onTermsDialogClose = () => this.setState({termsDialog:false})

    onTermsDialogOpen = () => this.setState({termsDialog:true})

    hasError = field =>  this.state.errors[field];

    render(){
        const { classes } = this.props;
        const { form,errors } = this.state;

        return (
            <div className={classes.root}>
                <CommonSnackbar ref={ ref => this._snackbar = ref }/>
            
            <Grid
              container
              spacing={4}
            >
        
            <Grid item  md={12} xs={12} >
            <Card>
              <form
                autoComplete="off"
                noValidate
              >
                <CardHeader title="KYC APPLICATION" />
                <Divider />
                <CardContent>
                <form
                  autoComplete="off"
                  noValidate
                >

                  <Grid
                    container
                    spacing={3}
                  >

                    <Grid item  md={6} xs={12} >

                    <SelectBox items={Genders} value={form.gender} name="gender" 
                    label="Sponsor Number" onChange={this.onInputChange} error={this.hasError('gender')} />

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <SelectBox items={MaritalStatus} name="maritalStatus" label="Proposer Number"
                      error={this.hasError('maritalStatus')}
                    />

                    </Grid>
                    

                    <Grid item  md={4} xs={12} >

                    <SelectBox items={Genders} value={form.gender} label="Gender" name="gender" onChange={this.onInputChange} error={this.hasError('gender')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <SelectBox items={MaritalStatus} label="Marital Status" value={form.maritalStatus} name="maritalStatus" onChange={this.onInputChange} error={this.hasError('maritalStatus')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <SelectBox items={NameTitles} name="nameTitle" label="Name Title" value={form.nameTitle} onChange={this.onInputChange} error={this.hasError('nameTitle')}/>

                    </Grid>

                    <Grid item  md={4} xs={12} >

                    <TextFieldBox name="name" label="Direct Seller Name" value={form.name} onChange={this.onInputChange} error={this.hasError('name')}/>

                    </Grid>

                    <Grid item  md={4} xs={12} >

                    <TextFieldBox name="middleName" label="Direct Seller Middle Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                    </Grid>

                    <Grid item  md={4} xs={12} >

                    <TextFieldBox name="lastName" label="Direct Seller Last Name" value={form.lastName} onChange={this.onInputChange} error={this.hasError('lastName')}/>

                    </Grid>

                    <Grid item  md={4} xs={12} >

                    <TextFieldBox name="gender" label="Applicant Date of Birth" />

                    </Grid>


                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="password" label="New Password" value={form.password} onChange={this.onInputChange} error={this.hasError('password')}/>

                    </Grid>


                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="confirm_password" label="Confirm Password" value={form.confirm_password} onChange={this.onInputChange} error={this.hasError('confirm_password')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <FileUploadBox label="Upload Applicant Photo" />

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="mobileNo" label="Mobile Number" value={form.mobileNo} onChange={this.onInputChange} error={this.hasError('mobileNo')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="fName" label="Father Name" value={form.fName} onChange={this.onInputChange} error={this.hasError('fName')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="husbandName" label="Spouse Name" value={form.husbandName} onChange={this.onInputChange} error={this.hasError('husbandName')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="nomineename" label="Nominee Name" value={form.nomineename} onChange={this.onInputChange} error={this.hasError('nomineename')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="nomineeMiddleName" label="Nominee Middle Name" value={form.nomineeMiddleName} onChange={this.onInputChange} error={this.hasError('nomineeMiddleName')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="nomineeLastName" label="Nominee Last Name" value={form.nomineeLastName} onChange={this.onInputChange} error={this.hasError('nomineeLastName')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="nomineeDob" label="Nominee Date of Birth" value={form.nomineeDob} onChange={this.onInputChange} error={this.hasError('nomineeDob')}/>

                    </Grid>


                    <Grid item md={4}  xs={12} >

                    <SelectBox items={MaritalStatus} name="relationshipWithNominee" label="Relationship with Nominee"
                      error={this.hasError('relationshipWithNominee')}
                    />
                    </Grid>

                    <AddressComponent />

                    <Grid item md={12}  xs={12} >

                      <FormControlLabel
                        control={<Checkbox checked={true} onChange={(event) => console.log('event',event)} name="isSameAsDSAddress" />}
                        label="Same As DS Address"
                      />

                    </Grid>

                    <AddressComponent />

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="alternateMobNo" label="Alternate Mobile Number" value={form.alternateMobNo} onChange={this.onInputChange} error={this.hasError('alternateMobNo')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="email" label="E-mail Address" value={form.email} onChange={this.onInputChange} error={this.hasError('email')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <SelectBox items={IdProofs} value={form.selectedIdProof} label="Select ID Proof" name="selectedIdProof" onChange={this.onInputChange} error={this.hasError('selectedIdProof')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <SelectBox items={addressProofs} value={form.selectedAddressProof} label="Select Address Proof" name="selectedAddressProof" onChange={this.onInputChange} error={this.hasError('selectedAddressProof')}/>

                    </Grid>


                    <Grid item md={6}  xs={12} >

                    <FileUploadBox label="Upload Original ID Proof" /> 

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <FileUploadBox label="Upload Original Address Proof" />

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="ifscCode" label="IFSC Code" value={form.ifscCode} onChange={this.onInputChange} error={this.hasError('ifscCode')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="panNumber" label="PAN Number" value={form.panNumber} onChange={this.onInputChange} error={this.hasError('panNumber')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="bankName" label="Bank Name" value={form.bankName} onChange={this.onInputChange} error={this.hasError('bankName')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="bankAccountNumber" label="Bank Account Number" value={form.bankAccountNumber} onChange={this.onInputChange} error={this.hasError('bankAccountNumber')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="branchState" label="Branch State" value={form.branchState} onChange={this.onInputChange} error={this.hasError('branchState')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <TextFieldBox name="branchName" label="Branch Name" value={form.branchName} onChange={this.onInputChange} error={this.hasError('branchName')}/>

                    </Grid>

                    <Grid item md={4}  xs={12} >

                    <FileUploadBox label="Upload Original Bank Account Proof" />

                    </Grid>

                    <Grid item md={4}  xs={12} >
                      
                    <FileUploadBox label="Upload Original Pan Card Proof" />

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <SelectBox items={hintQuestions} value={form.hintQuestion} label="Select Hint Question" name="hintQuestion" onChange={this.onInputChange} error={this.hasError('hintQuestion')}/>

                    </Grid>

                    <Grid item md={6}  xs={12} >

                    <TextFieldBox name="hintAnswer" label="Hint Question Answer" value={form.hintAnswer} onChange={this.onInputChange} error={this.hasError('hintAnswer')}/>
                    
                    </Grid>

                    <Grid item xs={12} md={6}>
                      <p>
                      I have Read and Agreed to the Following 
                      <a href="javascript:void(0)" onClick={this.onTermsDialogOpen}>  Terms and Conditions</a>
                      </p>
                     
                    </Grid>
                    
                  </Grid>
               
               </form>
                </CardContent>
                
                <DialogBox open={this.state.termsDialog} title="TERMS AND CONDITIONS FOR DIRECT SELLER" 
                  onClose={this.onTermsDialogClose} onCancel={this.onTermsDialogClose} onOK={this.onTermsDialogClose}
                  >
                    {
                       <div>
                         
                       <p>
                           These terms and conditions are construed in accordance with of model guidelines
                           on direct selling issued by the Govt. of India, Ministry of Consumer Affairs, Food
                           &amp; Public Distribution, Department of Consumer Affairs vide F.No. 21/18/2014-IT
                           (Vol-II) dated 9<sup>th</sup> Sept., 2016 and supersedes any prior terms and conditions,
                           discussions or agreements between Company and direct seller.
                       </p>
                       <p>
                           The applicant intending to become direct seller shall go through these terms and
                           conditions and if he/she agrees and accept these, he/she shall append his signature
                           in the column provided hereunder as token of his/her acceptance. Choosing the sponsoring
                           and consent to join the group is exclusive decision of applicant. There is no role
                           or any suggestion of the company in taking such decision by the applicant. Further
                           there is no any charge for becoming a direct seller of the company. The company
                           exclusively uses its website to display the details of the products, marketing method/plan,
                           sales incentives and business monitoring etc.
                       </p>
                       <p>
                           <strong>DEFINITIONS</strong>
                       </p>
                       <p>
                           As used herein, the following terms shall have the meanings set forth below:
                       </p>
                       <p>
                           <strong>A. “<u>Net Work of Direct Selling</u>”</strong> shall means any system of
                           distribution or marketing adopted by direct selling entity to undertake direct selling
                           business and shall include the multi level marketing method of distribution of goods
                           and services.
                       </p>
                       <p>
                           <strong>B. “<u>Direct selling entity</u></strong>” means an entity which sells or
                           offers to sell goods or services through a direct seller. The company M/s Fashion
                           Suitings Private Limited is the direct selling entity. In case “RCM” word is used
                           at company’s website, in any publication, literature, marketing plan etc. etc. of
                           the company then meaning of “RCM” word shall be interpreted as M/s Fashion Suitings
                           Private Limited.
                       </p>
                       <p>
                           <strong>C. “<u>Direct seller</u></strong>” means a person (Individual Indian Citizen
                           only) appointed or authorized, directly or indirectly, by a direct selling entity
                           to undertake direct selling business on principal to principal basis.
                       </p>
                       <p>
                           <strong>D. “<u>Direct selling</u></strong>” means marketing, distribution and sale
                           of goods or providing of services as a part of network of direct selling.
                       </p>
                       <p>
                           <strong>E “<u>Unique ID/Track ID</u>”</strong> Means unique identification number
                           issued by the company to the direct seller as token of acceptance of his/her application
                           for direct selling of the goods/service of the company. No communication will be
                           entertained without unique ID and password. Direct Seller shall preserve the Unique
                           ID and Password properly as it is “must” for logging on to website.
                       </p>
                       <p>
                           <strong>F. “<u>Cooling-Off Period</u>”</strong> The duration of time counted from
                           the date when the direct seller and the direct selling entity enter into an agreement
                           and ending with the date on which the contract is to be performed and within which
                           direct seller may repudiate the agreement without being subject to penalty for breach
                           of contract.
                       </p>
                       <p>
                           <strong>G. ”<u>Website</u>”</strong> means the official website of the company i.e.
                           <a href="http://www.rcmbusiness.com/">www.rcmbusiness.com</a> or any other website,
                           which the company may notify time to time.
                       </p>
                       <p>
                           <strong>THE APPOINTMENT AND UNDERSTANDING</strong>
                       </p>
                       <p>
                           <strong>A. </strong>The Company upon scrutiny and verification of the Application
                           may register the Applicant as “Direct Seller” for selling the goods/ products of
                           the Company. The Company shall be at liberty to accept or reject the application
                           at its discretion without assigning any reason whatsoever. Allotment of password
                           and Unique ID by the company shall be construed as the registration of direct seller.
                       </p>
                       <p>
                           The applicant hereby covenants as under:
                       </p>
                       <p>
                           i. That he has clearly understood the marketing methods/plan, the incentive plan,
                           its limitations and terms &amp; conditions. He/she agrees that he/she is not relying
                           upon any misrepresentation/s or fraudulent inducement or assurance or commitment
                           that is not set out in the terms and conditions/marketing plan/incentive plan or
                           any other officially printed or published materials of the Company.
                       </p>
                       <p>
                           ii. Relation between the Company and the Direct Seller shall be governed, in addition
                           to these terms &amp; conditions, by the rules and procedure mentioned in the marketing
                           plan, available on website or provided by the company in any manner. The Direct
                           Seller further confirms that he/she has read and understood the terms &amp; conditions
                           carefully and agrees to be bound by them.
                       </p>
                       <p>
                           iii. Direct Seller is an independent contractor, and nothing contained in these
                           terms &amp; conditions shall be construed to
                       </p>
                       <p>
                           (a) Give any party the power to direct and control the day-to-day activities of
                           the other.
                       </p>
                       <p>
                           (b) Constitute the parties as partners, joint ventures, co-owners or otherwise,
                           or
                       </p>
                       <p>
                           (c) Allow Direct Seller to create or assume any obligation on behalf of Company
                           for any purpose whatsoever.
                       </p>
                       <p>
                           iv. Direct Seller is not an employee of Company and shall not be entitled to any
                           employee’s benefits. Direct Seller shall be responsible for paying all taxes whether
                           direct or indirect including but not limited to Income Tax, VAT, Service tax and
                           other taxes chargeable to Direct Seller on amounts earned hereunder. All Legal,
                           Statutory, financial and other obligations associated with Direct Seller’s business
                           shall be the sole responsibility of Direct Seller.
                       </p>
                       <p>
                           v. It is made and understood in very clear terms that Direct Seller is not an Agent,
                           Employee nor an authorized representative of the Company or its service providers.
                           He is not authorized to receive/accept any amount/payment for and behalf of the
                           Company and any payment received by him/her from any party shall not be deemed to
                           be received by the Company.
                       </p>
                       <p>
                           vi. Direct Seller, hereby declare that all the information furnished by him/her
                           are true and correct. Company shall be at liberty to take any action against the
                           Direct Seller in case it is discovered at any stage that the Direct Seller has furnished
                           any wrong/false/misleading information to the Company or other direct sellers.
                       </p>
                       <p>
                           vii. If any relative as defined under the provisions of Income Tax Act, 1961 or
                           defined under the provisions of Companies Act, 2013 of existing direct seller desire
                           to become direct seller then he/she shall disclose the relationship with existing
                           direct seller to the company. It is company’s sole discretion to accept or reject
                           the application of such relative.
                       </p>
                       <p>
                           <strong>B. </strong>The Direct Seller shall enjoy the following privileges:-
                       </p>
                       <p>
                           i. Incentive for effecting sale of goods /products of the Company as per marketing
                           plan.
                       </p>
                       <p>
                           ii. No territorial restriction to sale the goods/products in India.
                       </p>
                       <p>
                           iii. Search and inspect his/her account on website of the Company through the Unique
                           ID and password awarded by the Company.
                       </p>
                       <p>
                           iv. Incentive of the Direct Seller shall be in proportion to the volume of performance
                           by the Direct Seller either by his personal efforts or through team as stipulated
                           in the marketing plan of the Company.
                       </p>
                       <p>
                           v. The direct seller shall be entitled to a cooling off period of 45 days from the
                           date of acceptance of this terms &amp; conditions without any punishable clause.
                       </p>
                       <p>
                           vi. The Direct seller shall have the option to return the currently marketable goods
                           purchased by him/her within 30 days from the date of the purchase. Such return shall
                           be governed by the return policy published on the website of the company.
                       </p>
                       <p>
                           <strong>OTHER GENERAL TERMS AND CONDITIONS</strong>
                       </p>
                       <p>
                           A. <strong>General Duties</strong>
                       </p>
                       <p>
                           i. Direct Seller shall use his/her best efforts to promote the sale of goods and
                           services and maximize them. Direct Seller shall also provide reasonable assistance
                           to Company in promotional activities. Direct Seller will assist the company by taking
                           part in all promotional events,<strong> </strong>use the marketing inputs judiciously
                           for maximizing orders for the company. Direct seller shall offer accurate and complete
                           explanations and demonstrations of goods and services, price, payment terms, return
                           policies etc. to a prospective consumer. He/she shall also take care for all obligations;
                           provisions, terms and conditions etc. of the model guidelines on direct selling
                           issued by the Govt. of India, Ministry of Consumer Affairs, Food &amp; Public Distribution
                           Department of Consumer Affairs vide F.No. 21/18/2014-IT (Vol-II) dated 9<sup>th</sup>
                           Sept., 2016.
                       </p>
                       <p>
                           ii. The Company reserves its right to withheld/block/suspend the Direct Seller in
                           the event the Direct Seller fails to provide any details as desired by the Company
                           from time to time including but not limited to his/her Pan Card details.
                       </p>
                       <p>
                           iii. In case the Direct Seller losses his contractual capacity due to any reason
                           or in case of death of the Direct Seller, either his nominee or one of the legal
                           heir with the written consent of all the legal heirs may join the Company as Direct
                           Seller in place of the deceased provided he applies in prescribed form and undertakes
                           to abide all rules and regulations, terms and conditions etc. in the same manner
                           as that of original Direct Seller. In case of failure to arrival at such consent
                           within six months from the date of death of the Seller or losing his/her contractual
                           capacity, the Company shall be at liberty to terminate the Unique ID. For this period
                           the Company will keep his/her Unique ID in abeyance.
                       </p>
                       <p>
                           v. Direct Seller shall be sole responsible for all the arrangements, expenses, permission
                           from local authorities, complying with rules of Central Government, State Government,
                           local body or any other Government body for the meetings and seminars or any other
                           event conducted by the Direct Seller.
                       </p>
                       <p>
                           vi. Direct Seller is prohibited from listing, marketing, advertising, promoting,
                           discussing, or selling any product, or the business opportunity on any website/online
                           portal/mobile application/online forum/or any other manner that offers like auction
                           as a mode of selling.
                       </p>
                       <p>
                           vii. The Direct Seller hereby undertakes not to compel or induce or mislead any
                           person with any false statement /promise to purchase products from the Company or
                           to become Direct Seller of the Company.
                       </p>
                       <p>
                           <strong>B. </strong><strong>Modification of The Terms and Conditions</strong>
                       </p>
                       <p>
                           Notwithstanding anything stated or provided herein, Company reserves the complete
                           and unfettered rights and discretion to modify, amend, alter, or very the terms
                           and conditions, products, marketing plan, business and any other policies at anytime
                           without any prior notice. Modification shall be published through the official website
                           of the Company or any other mode as company may deem fit and proper and such modification/amendment
                           shall be applicable and binding upon the Direct Seller from the date of such modification/notification.
                           If the Direct Seller does not agree to such amendment, he/she may terminate his/her
                           direct sellership within 45 days of such publication by giving a written notice
                           to the Company to such effect. Without any objection to such modifications/alterations,
                           if Direct Seller continues his/her activities, it shall be deemed that he/she has
                           accepted all modifications and amendments in the terms &amp; conditions.
                       </p>
                       <p>
                           <strong>C. </strong><strong>Sole Compensation (Sales Incentive)</strong>
                       </p>
                       <p>
                           The Company shall pay to the Direct Seller sales incentive as prescribed in the
                           marketing plan. The marketing plan shall be available at website of the company.
                           The sales incentive will be subjected to the relevant taxes as applicable. The Company
                           reserves its right to revise the rate of sales incentive from time to time. The
                           Company does not guarantee/assure any particular or fixed facilitation fees or fixed
                           income to the Direct Seller.
                       </p>
                       <p>
                           D. <strong>Indemnification by Direct Seller</strong>
                       </p>
                       <p>
                           The Direct Seller hereby indemnify the company, its employees, directors, agents,
                           and each of their Affiliates (the “<strong>Indemnified Persons</strong>”) against,
                           and agree to hold them harmless from, any and all damages (including any claim,
                           charge, action, depletion or diminution in value of the assets of the Company, loss,
                           liability and expense (including, without limitation, reasonable expenses of investigation
                           and reasonable attorneys’ fees and expenses in connection with any action, suit
                           or proceeding) (hereinafter referred to as “<strong>Loss</strong>”) incurred or
                           suffered by the Indemnified Persons and arising out of or relating to any misrepresentation,
                           negligence, malfeasant acts or breach of Warranty, or any breach of any covenant
                           or agreement made or to be performed by the direct seller pursuant to these terms
                           &amp; conditions.
                       </p>
                       <p>
                           E. <strong>Additional Responsibilities of Direct Seller</strong>
                       </p>
                       <p>
                           <strong>i. </strong><strong>Expense of Doing Business</strong>
                       </p>
                       <p>
                           Direct Seller shall bear the cost and expense of conducting its business in accordance
                           with these terms and conditions. The company will not entertain any re-imbursement
                           on any expense made by the Direct Seller other than sales incentive earned by the
                           Direct Seller as per the Marketing Plan.
                       </p>
                       <p>
                           <strong>ii. </strong><strong>Use of Marketing Material with Prior Permission.</strong>
                       </p>
                       <p>
                           Use of company logo, product logo, any advertising / promotion / marketing activity
                           conceived originally by the Direct Seller shall be first approved in writing by
                           the company before being used / implemented.
                       </p>
                       <p>
                           <strong>iii. </strong><strong>Customer Complaints</strong>
                       </p>
                       <p>
                           Direct Seller shall notify the Company of any Customer's complaints regarding either
                           the Products or the services by the direct sellers and immediately forward to Company
                           the information regarding those complaints.
                       </p>
                       <p>
                           iv. <strong>Non-Compete</strong>
                       </p>
                       <p>
                           (a) During the term of association.
                       </p>
                       <p>
                           During the term of association as direct seller with Fashion Suiting Private Limited,
                           Direct Seller shall and / or his/her relative as defined under these terms &amp;
                           conditions elsewhere not represent, promote or otherwise try to do direct selling
                           activities that, in Company's judgment, compete with its direct selling activities.
                       </p>
                       <p>
                           (b) After the termination
                       </p>
                       <p>
                           For a period of [12 months] after the Direct Seller is no longer in arrangement
                           with the Company, the Direct Seller or through his relative will not, directly or
                           indirectly, either as proprietor, stockholder, partner, officer, employee or otherwise,
                           distribute, sell, offer to sell, or solicit any orders for the purchase or distribution
                           of any products or services which are similar to those distributed, sold or provided
                           by the Company.
                       </p>
                       <p>
                           <strong>TERMINATIONS AND CESSATION </strong>
                       </p>
                       <p>
                           The Company shall be at complete liberty to terminate the direct sellership in occurrence
                           of any of the following event:-
                       </p>
                       <p>
                           i. Where a direct seller is found to have made no purchases by himself/herself of
                           goods and services for a period of 3 months since the date of joining the direct
                           sellership or where there is no purchases by himself/herself of goods or services
                           for a continuous period of 6 months since the date of the last purchases made.
                       </p>
                       <p>
                           ii. Where a direct seller failed to comply with any terms and conditions.
                       </p>
                       <p>
                           iii. Where information given by direct seller found wrong/false/misleading.
                       </p>
                       <p>
                           iv. Where direct seller migrate to the other country.
                       </p>
                       <p>
                           v. Where direct seller is convicted of an offence punishable imprisonment of whatever
                           term.
                       </p>
                       <p>
                           vi. Where direct seller resign voluntarily.
                       </p>
                       <p>
                           vii. Where company deem it necessary to terminate the direct seller in the interest
                           of company’s business or in the interest of others direct sellers connected in his/her
                           group/teams:-
                       </p>
                       <p>
                           Return of materials. All of Company's trademarks, trade names, data, photographs,
                           literature, and sales aids, all kind of customer related database and any other
                           information generated shall always remain the property of Company. Within five (5)
                           days after the termination of direct sellership, Direct Seller shall return all
                           such items to company. Direct Seller shall not make or retain any copies of any
                           confidential items or information that may have been entrusted to it. Effective
                           upon the termination of direct sellership, Direct Seller shall cease to use all
                           trademarks, marks and trade name of Company.
                       </p>
                       <p>
                           The company is free to review the performance of direct seller at timely intervals.
                           Any Direct Seller not performing to the full satisfaction of the company in terms
                           of securing new orders, in compliance of company's policies and terms and conditions
                           is liable to be terminated.
                       </p>
                       <p>
                           <strong>CONFIDENTIALITY</strong>
                       </p>
                       <p>
                           Direct Seller acknowledges that by reason of its relationship to Company hereunder,
                           it will have access to certain information and materials concerning Company's business
                           plans, customers, technology, and products/services that is confidential and of
                           substantial value to Company, which value would be impaired if such information
                           were disclosed to third parties. Direct Seller agrees that it shall not use in any
                           way for its own account or the account of any third party, nor disclose to any third
                           party, any such confidential information revealed to it by the Company.
                       </p>
                       <p>
                           Company shall advise Direct Seller whether or not it considers any particular information
                           or materials to be confidential. Direct Seller shall not publish any description
                           of the Products/Services beyond the description published by Company and without
                           the prior written consent of the Company. In the event of termination, there shall
                           be no use or disclosure by Direct Seller of any confidential information of the
                           Company.
                       </p>
                       <p>
                           <strong>RECOURSE AND JURISDICTION</strong>
                       </p>
                       <p>
                           The terms and conditions stipulated in the forgoing paragraphs shall be governed
                           in accordance with the law in force in India. Subject the Arbitration Clause of
                           these terms &amp; conditions, all Disputes, either civil or criminal in nature,
                           shall be subject to the exclusive jurisdiction of the courts in Bhilwara, Rajasthan
                           only and nowhere else.
                       </p>
                       <p>
                           <strong>
                           DISPUTE RESOLUTION AND ARBITRATION</strong>
                       </p>
                       <p>
                           <a name="_DV_M250"></a>If any dispute arises between the Parties in connection with
                           or relating to these terms &amp; conditions, including the validity, interpretation,
                           implementation, termination, or alleged material breach of any provision thereof,
                           the Parties hereto shall endeavor to settle such dispute amicably. In the event
                           any dispute is not amicably settled within a period of 30<a name="_DV_C418"> (thirty)</a><a name="_DV_M251"></a> days after any Party has given notice to the other Parties
                           of the existence of such dispute and requiring an amicable settlement thereof, the
                           same shall be, at the request Party, settled by arbitration.
                       </p>
                       <p>
                           <a name="_DV_M252"></a>Any arbitration in these terms &amp; conditions shall be
                           conducted by a sole Arbitrator appointed mutually by all disputing Parties, or in
                           case of disagreement as to the appointment of the sole Arbitrator, by a panel of
                           three (3) Arbitrators, of which the Company shall appoint one (1) Arbitrator, the
                           direct Seller shall appoint the second Arbitrator and the third Arbitrator shall
                           be appointed by the two appointed Arbitrators. The Arbitration proceedings shall
                           be governed by the Arbitration and Conciliation Act, 1996.<a name="_DV_C419"> Each Party
                               shall bear its own cost of arbitration.</a>
                       </p>
                       <p>
                           <a name="_DV_M253"></a>The Arbitration proceedings shall be held in Bhilwara, Rajasthan,
                           India.<a name="_DV_M254"></a> The Arbitration proceeding shall be governed by the<a name="_DV_C421">Laws</a><a name="_DV_M255"></a> of India. <a name="_DV_M256">
                           </a>The proceedings of arbitration shall be in English languag<a name="_DV_M257"></a>e.
                           The Arbitrator’s award shall be substantiated in writing. The Arbitrator <a name="_DV_C423">
                               may</a><a name="_DV_M258"></a> also <a name="_DV_C425">award</a><a name="_DV_M259"></a>
                           costs<a name="_DV_M260"></a>.
                       </p>
                       <p>
                           <a name="_DV_M261"></a>Any decision of the arbitrators will be final, binding and
                           incontestable. The Parties agree that no Party shall have any right to commence
                           or maintain any suit or legal proceedings (other than for interim or conservatory
                           measures) until the Dispute has been determined in accordance with the arbitration
                           procedure provided herein and then only for enforcement of the award rendered in
                           the arbitration. Judgment upon the arbitration award may be rendered in any court
                           of competent jurisdiction or application may be made to such court for a judicial
                           acceptance of the award and an order of enforcement, as the case may be. The Parties
                           hereby waive any application or appeal to any court of competent jurisdiction to
                           the fullest extent permitted by Law in connection with any question of Law arising
                           during the course of arbitration or any award made.
                       </p>
                       <p>
                           <strong>FORCE MAJEURE</strong>
                       </p>
                       <p>
                           The Company shall not be liable for any failure to perform its obligations where
                           such failure has resulted due to Acts of Nature (including fire, flood, earthquake,
                           storm, hurricane or other natural disaster), war, invasion, act of foreign enemies,
                           hostilities (whether war is declared or not), civil war, rebellion, revolution,
                           insurrection, military or usurped power or confiscation, terrorist activities, nationalization,
                           acquisition of the company’s asset by the government to any other government/semi
                           government agency, civil/financial emergency by the government, any other government
                           sanction, blockage, embargo, labour dispute, strike, lockout or interruption or
                           failure of electricity, Raw Material Supply Constraint, or any type of redirection
                           by Government (Central and / or State), Local Authority or any other government
                           department.<strong></strong>
                       </p>
                       <p>
                           <strong>SOLEMNLY AFFIRM AND DECLARE AS FOLLOWS:</strong>
                       </p>
                       <p>
                           i. That I have read and understood the terms and conditions for appointment of Direct
                           Seller of the Company.
                       </p>
                       <p>
                           ii. I have also gone through the Company's official website, printed materials,
                           brochures and convinced about the business and I have applied to appoint me as a
                           Direct Seller on my own volition.
                       </p>
                       <p>
                           iii. I declare that I have not been given any assurance or promise or inducement
                           by the Company or its Directors in regard to any fixed income incentive, prize or
                           benefit on account of the products purchased by me.
                       </p>
                       <p>
                           iv. I have clearly understood that eligibility of income exclusively depends on
                           my performance in business volume as per the business plan. I further agree that
                           company reserves the right to change the Business Plan at any point of time without
                           any prior notice.
                       </p>
                       <p>
                           v. I undertake not to misguide or induce dishonestly anybody to join the Company.
                       </p>
                       <p>
                           vi. I hereby agree and adhere to the terms and conditions as stipulated along with
                           the application form and as mentioned above to agree to purchase the product as
                           Consumer/to do the Direct Seller activities.
                       </p>
                       <p>
                           vii. I hereby agree to submit all disputes to arbitration as provided in the terms
                           and conditions of the Company.
                       </p>
                   </div>
                    }
                </DialogBox>

                <Divider />
                <CardActions>
        
                <SpinnerButton title="Next" handleButtonClick={this.onSubmit} loading={this.state.loading}  />
        
                </CardActions>
              </form>
            </Card>
            </Grid>

            </Grid>
            </div>
        )
    }
}

KYCApplication.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(KYCApplication);