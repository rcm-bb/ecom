import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import SpinnerButton from './../../components/SpinnerButton';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField
} from '@material-ui/core';
import {isEmpty,set,unset} from 'lodash'
import validate from 'validate.js';
import client from './../../utils/client';
import CommonSnackbar from './../../components/Snackbar';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  DatePicker
} from '@material-ui/pickers';
import moment from 'moment'
const schema = {
    applicantDob: {
      presence: { allowEmpty: false, message: '^Applicant Date of Birth is required' }
    },
    registeredMobNo: {
      presence: { allowEmpty: false, message: '^Registered Mobile Number is required' }
    }
};
  

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    }
});


class ForgotRefNumber extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{
                applicantDob:moment()
            },
            loading:false,
            errors:{}
        }
        this._snackbar = null;
    }

    handleChange = event => {
        let {form,errors} = this.state;
        let name = event.target.name;
        unset(errors,name)
        this.setState({
            form:{
                ...form,
                [name] : event.target.value
            },
            errors
        });
    };

    handleChangeDate = date => {
        let {form,errors} = this.state;
        unset(errors,'applicantDob')
        this.setState({
            form:{
                ...form,
                applicantDob: date
            },
            errors
        });
    };

    onSubmit = async () => {
        const {applicantDob, registeredMobNo} = this.state.form;
        let errors = validate(this.state.form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({loading:true,errors})
        const res = await client.post(`pu/forgotRefId/${applicantDob}/${registeredMobNo}`);
        if(res.status == true){
            this._snackbar.openSnack('data fetch successfully')
        }else{
            this._snackbar.openSnack(res.data,'error')
        }
        this.setState({loading:false})
    }

    hasError = field =>  this.state.errors[field] ? true : false;


    render(){
        const { classes } = this.props;
        const { form,errors } = this.state;
        
        return (
            <div className={classes.root}>
                <CommonSnackbar ref={ ref => this._snackbar = ref }/>
            <Grid
              container
              spacing={4}
            >
        
            <Grid item  md={12} xs={12} >
            <Card>
              <form
                autoComplete="off"
                noValidate
              >
                <CardHeader title="FORGET REFERENCE NUMBER" />
                <Divider />
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                  >
                    <Grid
                      item
                      md={6}
                      xs={12}
                    >

                        <DatePicker
                            disableToolbar
                            inputVariant="outlined"
                            format="dd/MM/yyyy"
                            margin="normal"
                            name="applicantDob"
                            fullWidth
                            id="date-picker-inline"
                            label="Select Applicant Date of Birth"
                            value={form.applicantDob}
                            onChange={this.handleChangeDate}
                            error={this.hasError('applicantDob')}
                            helperText={
                                this.hasError('applicantDob') ? errors.applicantDob[0] : null
                            }
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            variant="outlined"
                        />

                      {/* <TextField
                        fullWidth
                        label="Select Applicant Date of Birth"
                        name="applicantDob"
                        onChange={this.handleChange}
                        required
                        value={form.applicantDob}
                        variant="outlined"
                        error={this.hasError('applicantDob')}
                        helperText={
                            this.hasError('applicantDob') ? errors.applicantDob[0] : null
                        }
                      /> */}
                    </Grid>
                    <Grid
                      item
                      md={6}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Enter Registered Mobile Number"
                        name="registeredMobNo"
                        onChange={this.handleChange}
                        required
                        value={form.registeredMobNo}
                        variant="outlined"
                        error={this.hasError('registeredMobNo')}
                        helperText={
                            this.hasError('registeredMobNo') ? errors.registeredMobNo[0] : null
                        }
                      />
                    </Grid>
        
                  </Grid>
                </CardContent>
                </MuiPickersUtilsProvider>
                <Divider />
                <CardActions>
        
                <SpinnerButton title="Submit" handleButtonClick={this.onSubmit} loading={this.state.loading}  />
        
                </CardActions>
              </form>
            </Card>
            </Grid>

            </Grid>
            </div>
        )
    }
}

ForgotRefNumber.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(ForgotRefNumber);