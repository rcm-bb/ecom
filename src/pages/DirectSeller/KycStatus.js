import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import SpinnerButton from './../../components/SpinnerButton';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField
} from '@material-ui/core';
import {isEmpty,set,unset} from 'lodash'
import validate from 'validate.js';
import client from './../../utils/client';
import CommonSnackbar from './../../components/Snackbar';

const schema = {
    refId: {
      presence: { allowEmpty: false, message: '^Reference ID is required' }
    },
    refPass: {
      presence: { allowEmpty: false, message: '^Password is required' }
    }
};
  

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    }
});


class AccountDetails extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:false,
            errors:{},
            touched:{}
        }
        this._snackbar = null;
    }

    handleChange = event => {
        let {form,errors} = this.state;
        let name = event.target.name;
        unset(errors,name)
        this.setState({
            form:{
                ...form,
                [name] : event.target.value
            },
            errors
        });
    };

    onSubmit = async () => {
        const {refId, refPass} = this.state.form;
        let errors = validate(this.state.form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({loading:true,errors})
        const res = await client.post(`pu/getKYCStatus/${refId}/${refPass}`);
        if(res.status == true){
            this._snackbar.openSnack('data fetch successfully')
        }else{
            this._snackbar.openSnack(res.message,'error')
        }
        this.setState({loading:false})
    }

    hasError = field =>  this.state.errors[field] ? true : false;


    render(){
        const { classes } = this.props;
        const { form,errors } = this.state;
        
        return (
            <div className={classes.root}>
                <CommonSnackbar ref={ ref => this._snackbar = ref }/>
            <Grid
              container
              spacing={4}
            >
        
            <Grid item  md={6} xs={12} >
            <Card>
              <form
                autoComplete="off"
                noValidate
              >
                <CardHeader title="Application KYC Status" />
                <Divider />
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                  >
                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Enter Reference ID Here"
                        name="refId"
                        onChange={this.handleChange}
                        required
                        value={form.refId}
                        variant="outlined"
                        error={this.hasError('refId')}
                        helperText={
                            this.hasError('refId') ? errors.refId[0] : null
                        }
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Enter Password Here"
                        name="refPass"
                        onChange={this.handleChange}
                        required
                        value={form.refPass}
                        variant="outlined"
                        error={this.hasError('refPass')}
                        helperText={
                            this.hasError('refPass') ? errors.refPass[0] : null
                        }
                      />
                    </Grid>
        
                  </Grid>
                </CardContent>
                <Divider />
                <CardActions>
        
                <SpinnerButton title="Submit" handleButtonClick={this.onSubmit} loading={this.state.loading}  />
        
                </CardActions>
              </form>
            </Card>
            </Grid>
        
            <Grid item  md={6} xs={12} >
            <Card >
        
                <CardHeader
                  title="KYC Status"
                />
                <Divider />
                <CardContent className={classes.cardContainer} >
                  <Grid
                    container
                    spacing={3}
                  >
              
                  </Grid>
                </CardContent>
           
            </Card>
            </Grid>
        
            </Grid>
            </div>
        )
    }
}

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(AccountDetails);