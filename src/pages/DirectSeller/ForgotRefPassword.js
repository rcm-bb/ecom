import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import SpinnerButton from './../../components/SpinnerButton';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField,
  InputLabel,
  FormControl,
  Select,
  MenuItem,
  FormHelperText
} from '@material-ui/core';
import {isEmpty,unset} from 'lodash'
import validate from 'validate.js';
import client from './../../utils/client';
import CommonSnackbar from './../../components/Snackbar';
import { hintQuestions } from '../../config';

const schema = {
    hintAnswer: {
      presence: { allowEmpty: false, message: '^Hint Answer is required' }
    },
    refId: {
      presence: { allowEmpty: false, message: '^Reference Numberis is required' }
    },
    hintQuestion: {
        presence: { allowEmpty: false, message: '^Hint Question is required' }
    }
};
  

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    },
    formControl: {
        width: '100%',
    },
});


class ForgotRefPassword extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:false,
            errors:{}
        }
        this._snackbar = null;
    }

    handleChange = event => {
        console.log('event',event)
        let {form,errors} = this.state;
        let name = event.target.name;
        unset(errors,name)
        this.setState({
            form:{
                ...form,
                [name] : event.target.value
            },
            errors
        });
    };

    onSubmit = async () => {
        const {refId, hintAnswer,hintQuestion} = this.state.form;
        let errors = validate(this.state.form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({loading:true,errors})
        const res = await client.post(`pu/forgotRefPassword/${refId}/${hintQuestion}/${hintAnswer}`);
        console.log('res',res)
        if(res.status == true){
            this._snackbar.openSnack('data fetch successfully')
        }else{
            this._snackbar.openSnack(res.message,'error')
        }
        this.setState({loading:false})
    }

    hasError = field =>  this.state.errors[field] ? true : false;


    render(){
        const { classes } = this.props;
        const { form,errors } = this.state;
        
        return (
            <div className={classes.root}>
                <CommonSnackbar ref={ ref => this._snackbar = ref }/>
            <Grid
              container
              spacing={4}
            >
        
            <Grid item  md={12} xs={12} >
            <Card>
              <form
                autoComplete="off"
                noValidate
              >
                <CardHeader title="FORGET REFERENCE PASSWORD" />
                <Divider />
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                  >
                    <Grid
                      item
                      md={6}
                      xs={12}
                    >

                      <TextField
                        fullWidth
                        label="Enter Reference Number"
                        name="refId"
                        onChange={this.handleChange}
                        required
                        value={form.refId}
                        variant="outlined"
                        error={this.hasError('refId')}
                        helperText={
                            this.hasError('refId') ? errors.refId[0] : null
                        }
                      />

                    </Grid>

                    <Grid
                      item
                      md={6}
                      xs={12}
                    >

                    <FormControl variant="outlined" className={classes.formControl}   error={this.hasError('hintQuestion')} >
                        <InputLabel id="demo-simple-select-outlined-label">Hint Question</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={form.hintQuestion}
                        onChange={this.handleChange}
                        label="Hint Question"
                        name="hintQuestion"
                        >
                        {
                            hintQuestions.map((item,i) => <MenuItem key={i} value={item.value}>{item.title}</MenuItem>)
                        }
                        </Select>
                    {this.hasError('hintQuestion') && <FormHelperText>{this.hasError('hintQuestion') ? errors.hintQuestion[0] : null}</FormHelperText> }
                    </FormControl>

                    </Grid>


                    <Grid
                      item
                      md={6}
                      xs={12}
                    >
                      <TextField
                        fullWidth
                        label="Enter Hint Question Answer"
                        name="hintAnswer"
                        onChange={this.handleChange}
                        required
                        value={form.hintAnswer}
                        variant="outlined"
                        error={this.hasError('hintAnswer')}
                        helperText={
                            this.hasError('hintAnswer') ? errors.hintAnswer[0] : null
                        }
                      />
                    </Grid>
        
                  </Grid>
                </CardContent>
                
                <Divider />
                <CardActions>
        
                <SpinnerButton title="Next" handleButtonClick={this.onSubmit} loading={this.state.loading}  />
        
                </CardActions>
              </form>
            </Card>
            </Grid>

            </Grid>
            </div>
        )
    }
}

ForgotRefPassword.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(ForgotRefPassword);