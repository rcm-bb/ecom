import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Box,
  Divider,
  Grid,
  Tab,Tabs,Typography
} from '@material-ui/core';
import {isEmpty} from 'lodash';
import client from './../../utils/client';

function TabPanel(props) {
    const { children, value,item, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box m={1}>
            {children}
          </Box>
        )}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


const TabItemContainer = ({item,...props}) => {
    const [tab,setTab] = useState(0);

    const handleTabChange = (event,tab) => {
        setTab(tab)
    }

    return (
        <>
        <Tabs
            value={tab}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleTabChange}
            aria-label="disabled tabs example"
        >
            <Tab label="English" />
            <Tab label="Hindi" />
        </Tabs>

            <TabPanel value={tab} index={0}> 
            <Typography component="div" dangerouslySetInnerHTML={{__html:item.englishNotice}}></Typography>
            </TabPanel>

            <TabPanel value={tab} index={1}> 
            <Typography component="div" dangerouslySetInnerHTML={{__html:item.hindiNotice}}></Typography>
            </TabPanel>
            
        </>
    )
}



const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{

    }
});


class NoticeBoard extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:true,
            data:[]
        }
    }

    async componentDidMount(){
        const {objectMap} = await client.post('pu/getActiveNotice');
        let data = !isEmpty(objectMap) && !isEmpty(objectMap.noticeList) ? objectMap.noticeList:[];
        this.setState({data,loading:false})
    }

    render(){
        const { classes } = this.props;
        const {data} = this.state;
        
        return (
            <div className={classes.root}>
               
            <Grid
              container
              spacing={4}
            >
             { !isEmpty(data) && data.map((item,index) => (   
                    <Grid item  md={12} xs={12} key={index}>
                    <Card >
                
                        <CardHeader
                        subheader={new Date(item.created).toDateString()}
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >

                            <TabItemContainer item={item}/>
                            
                        </CardContent>
                
                    </Card>
                    </Grid>
                ))
             }
        
            </Grid>
            </div>
        )
    }
}

NoticeBoard.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(NoticeBoard);