import React,{useCallback,useState,useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import client from './../utils/client'
import CommonSnackbar from '../components/Snackbar';
import {storeToken} from './../utils/auth';
import { useHistory } from "react-router-dom";
import validate from 'validate.js';

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 12
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 20
    }
  }
};

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  wrapper: {
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

export default function SignIn() {
  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const hasError = field => formState.touched[field] && formState.errors[field] ? true : false;
  const [loading,setLoading] = useState(false)
  let _snackbar = null;  
  const history = useHistory();
  async function onLogin(_snackbar){ 
      try{
        let res = await client.post('/api/auth/signin',{
            usernameOrEmail:formState.values.email,
            password:formState.values.password
        })
        storeToken(res)
        setLoading(false)
        _snackbar.openSnack('Login successful')
        setTimeout(() => history.push('/'),1000)
      }catch(err){
        console.log('err',err) 
        setLoading(false)
        _snackbar.openSnack('Invalid Email & password','error')
      }
  }

  const onSubmit = useCallback(() => {
    setLoading(true)
    onLogin(_snackbar);
  });
  
  return (
    <Container component="main" maxWidth="xs">
        <CommonSnackbar ref={ ref => _snackbar = ref }/>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            error={hasError('email')}
            helperText={
              hasError('email') ? formState.errors.email[0] : null
            }
            onChange={handleChange}
            value={formState.values.email || ''}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            error={hasError('password')}
            helperText={
              hasError('password') ? formState.errors.password[0] : null
            }
            onChange={handleChange}
            value={formState.values.password || ''}
          />

          <div className={classes.wrapper}>
            <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                onClick={onSubmit}
                className={classes.submit}
                // disabled={!formState.isValid || loading}
            >
            Sign In
            </Button>
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>

          <Grid container>
            <Grid item xs>
              <Link to="forgot" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link to="/signup" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>

    </Container>
  );
}