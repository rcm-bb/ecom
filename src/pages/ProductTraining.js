import React from 'react'
import {Grid} from '@material-ui/core'
import DataTable from '../components/Common/DataTable'
import DatePickerBox from '../components/Form/DatePickerBox'
import DialogBox from '../components/Common/DialogBox'
import client from '../utils/client';
import {isEmpty} from 'lodash'
import SelectBox from '../components/Form/SelectBox';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';

const defaultFilter = {
    visitingLeader:'',
    stateId:0,
    fromDate: new Date()
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box >
            {children}
          </Box>
        )}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
  
class ProductTraining extends React.Component {
    constructor(){
        super();
        this.state = {
            states:[],
            leaders:[],
            tab:0,
            title:'Product Training Programs',
            fetchURL:'pu/getPTPBySeacrhDto',
            filters:defaultFilter
        }
        this._leaderWiseTable = null;
        this._dateWiseTable = null;
        this._stateWiseTable = null;
    } 

    async componentDidMount(){
        let {objectMap} = await client.post('pu/getState');
        let leaderRes = await client.post('pu/getVisitingLeaderList');
        this.setState({states: objectMap.stateListDto, leaders: leaderRes.objectMap.visitingLeaderList })
    }

    onChange = (name,event) => {
        this.setState(prevState => ({filters:{...prevState.filters,[name]:event.target.value}}),() => {
            if(name == 'stateId') this._stateWiseTable.onFilterChanged();
            else if(name == 'visitingLeader') this._leaderWiseTable.onFilterChanged();
        })
    }
    
    onDateChange = value => this.setState(prevState => ({filters:{...prevState.filters, fromDate: value }}),() =>  this._dateWiseTable.onFilterChanged())

    handleTabChange = (event, tab) => this.setState({tab})

    render(){
        const {states,leaders,tab,fetchURL,title,filters} = this.state;
        return (
            <div style={{
                padding:20
            }}>

                <Paper>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleTabChange}
                    aria-label="disabled tabs example"
                >
                    <Tab label="Date wise" />
                    <Tab label="State wise" />
                    <Tab label="Leader wise" />
                </Tabs>
                </Paper>

                <SwipeableViews index={tab} >
                    <TabPanel value={tab} index={0}> 

                    <DataTable ref={ref => this._dateWiseTable = ref}  title={title} fetchURL={fetchURL} filters={{...defaultFilter,fromDate: filters.fromDate ? filters.fromDate.toISOString().split('T')[0]:''}}  rightHeaderChildren={(
                               <Grid container justify="flex-end" style={{marginTop:6}}> 
                                    <Grid  item xs={12} md={6}>
                                        <DatePickerBox label="From Date" value={filters.fromDate} name="fromDate" onChange={this.onDateChange} />
                                    </Grid>
                               </Grid>
                    )} />

                    </TabPanel>
     
                    <TabPanel value={tab} index={1}>
                        
                        <DataTable ref={ref => this._stateWiseTable = ref}  title={title} fetchURL={fetchURL} filters={{...defaultFilter,stateId:filters.stateId}} rightHeaderChildren={(
                               <Grid container spacing={2} justify="flex-end" style={{marginTop:6}}> 
                                    <Grid  item xs={12} md={6}>
                                    <SelectBox items={states} label="Select State" name="stateId" value={filters.stateId} onChange={this.onChange} itemValue="stateId" itemLabel="name" />
                                    </Grid>
                               </Grid>
                    )}/>
    
                    </TabPanel>

                    <TabPanel value={tab} index={2}>
                        
                        <DataTable ref={ref => this._leaderWiseTable = ref}  title={title} fetchURL={fetchURL} filters={{...defaultFilter,visitingLeader:filters.visitingLeader}} rightHeaderChildren={(
                               <Grid container spacing={2}  justify="flex-end" style={{marginTop:6}}> 
                                    <Grid  item xs={12} md={6}>
                                    <SelectBox items={leaders} label="Select Leader" name="visitingLeader" value={filters.visitingLeader} onChange={this.onChange} />
                                    </Grid>
                               </Grid>
                    )}/>
    
                    </TabPanel>

                </SwipeableViews>
            </div>
        )
    }
}

export default ProductTraining;