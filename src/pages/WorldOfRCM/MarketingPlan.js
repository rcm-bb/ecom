import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {AccordionFull} from './../../components/Accordion'
import SalesIn1 from "./../../assets/image/1.png";
import SalesIn2 from "./../../assets/image/2.png";
import SalesIn3 from "./../../assets/image/3.png";
import SalesIn4 from "./../../assets/image/bonus-slab-graph.jpg";
import SalesIn5 from "./../../assets/image/royalty.png";
import SalesIn6 from "./../../assets/image/tech-bonus.jpg";

const AntTabs = withStyles({
    root: {
      borderBottom: '1px solid #e8e8e8',
    },
    indicator: {
      backgroundColor: '#1890ff',
    },
  })(Tabs);
  
  const AntTab = withStyles((theme) => ({
    root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:hover': {
        color: '#40a9ff',
        opacity: 1,
      },
      '&$selected': {
        color: '#1890ff',
        fontWeight: theme.typography.fontWeightMedium,
      },
      '&:focus': {
        color: '#40a9ff',
      },
    },
    selected: {},
  }))((props) => <Tab disableRipple {...props} />);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function NavTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [epanel,setEpanel] = React.useState('epanel1')
  const [hpanel,setHpanel] = React.useState('hpanel1')

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && epanel == panel) expanded = false;
    setEpanel(expanded)
  };

  const handleHTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && hpanel == panel) expanded = false;
    setHpanel(expanded)
  };

  return (
    <div className={classes.root}>

        <AntTabs value={value} onChange={handleChange} aria-label="ant example">
          <AntTab label="English" />
          <AntTab label="मार्केटिंग प्लान" />
        </AntTabs>
        
      <TabPanel value={value} index={0}>
       
            <AccordionFull name="epanel1" value={epanel} title="A. BUSINESS VOLUME (B.V.) :" onChange={handleTabChange} expandIcon={true}  >
                <Typography>
                    It is the value of a product on which the Sales Incentive
                    is calculated. It can be seen on company’s website. The
                    B.V. of product may be equivalent to selling price or
                    different as may be declared by the company from time to
                    time. Business Volume can also be changed from time to
                    time by the company.
                </Typography>
            </AccordionFull>

            <AccordionFull name="epanel2" value={epanel} title="B. MAIN LEG (GROUP) AND OTHER LEG / LEGS (GROUP)" onChange={handleTabChange} expandIcon={true}  >
                <Typography>
                      The leg (Group) with highest Business Volume is called
                      main leg (Group) of Direct Seller. All the legs (Group)
                      other than main leg (Group) are called other leg/legs
                      (Group). Main leg (Group) may be different in different
                      months.
                </Typography>
            </AccordionFull>

            <AccordionFull name="epanel3" value={epanel} title="C. CALCULATION OF SALES INCENTIVE ON DIFFERENCE BASIS" onChange={handleTabChange} expandIcon={true}  >
                <Typography>
                      Net Sales Incentive of a Direct Seller is calculated by
                      deducting Sales Incentive of downline group from Sales
                      Incentive of total group. This can be termed as
                      calculation on difference basis. The Sales Incentive vtes
                      to the Direct Seller who becomes disqualified for any
                      reason for Sales Incentive, shall not be excluded for
                      calculate of Sales Incentive payable to other Direct
                      Sellers on difference basis.
                </Typography>
            </AccordionFull>

            <AccordionFull name="epanel4" value={epanel} title="D. PROPOSER" onChange={handleTabChange} expandIcon={true}  >
                <Typography>
                      Direct Seller who promotes the new person to become Direct
                      Seller in his/her group termed as a Proposer.
                </Typography>
            </AccordionFull>

            <AccordionFull name="epanel5" value={epanel} title="E. SPONSOR" onChange={handleTabChange} expandIcon={true}  >
                <Typography>
                      Just immediate upline of new applicant termed as a
                      sponsor.
                </Typography>
            </AccordionFull>

            <hr />
            <h5 class="mp-content-heading">HOW TO BECOME DIRECT SELLER :-</h5>
            <section class="mp-content-section">
                <p>
                  The person interested in joining is required to apply online
                  on the application form available on the company's website.
                  The details of who has proposed the applicant with the
                  necessary details are to be filled in the column of the
                  proposer and the detail of sponsor in the sponsor column. In
                  addition, the original scan copy of the applicant's photo,
                  identity card, address proof and bank account details has to
                  be uploaded.
                </p>
                <p>
                  The terms and conditions given with the application are to be
                  read and accepted. After that an OTP comes in the mobile
                  number given in the application. After the OTP is entered, the
                  application is received by the company. List of the documents
                  to be upload along with application form for becoming a Direct
                  Seller.
                </p>
                <ul class="lowerlatin">
                  <li>A colorful photo passport size</li>
                  <li>
                    One of the following documents for certification of Photo ID
                    <ul class="listnumber">
                      <li>Passport (Valid)</li>
                      <li>PAN Card</li>
                      <li>Voter’s Identity Card</li>
                      <li>Driving License (Valid)</li>
                      <li>
                        Written confirmation from the banks certifying identity
                        proof
                      </li>
                      <li>
                        Domicile certificate with communication address and
                        photograph
                      </li>
                      <li>Central/State Government certified ID proof</li>
                      <li>
                        Certification from any of the Authorities mentioned
                        below:
                        <br />
                        Panchayat Pradhan
                        <br />
                        Councilor
                        <br />
                        Sarpanch of Gram Panchayat
                      </li>
                    </ul>
                  </li>
                  <li>
                    Self attested Photocopy of any of following documents for
                    address proof:-
                    <ul class="listnumber">
                      <li>Telephone bill not older than 3 months</li>
                      <li>
                        Bank account statement not older than 3 months(Attested
                        by Bank)
                      </li>
                      <li>Voter’s Identity Card</li>
                      <li>Electricity bill not older than 3 months</li>
                      <li>Ration card</li>
                      <li>Passport (Valid)</li>
                      <li>Driving License (Valid)</li>
                      <li>Voter’s Identity Card</li>
                      <li>
                        Written confirmation from the banks (Attested by Bank)
                      </li>
                      <li>
                        Lease agreement along with rent receipt not older than 3
                        months
                      </li>
                      <li>
                        Current employer’s certificate mentioning residence
                      </li>
                      <li>
                        Domicile certificate with communication address and
                        photograph
                      </li>
                      <li>
                        Central/State Government certified Address proof. Please
                        note that expired passport cannot be used as address
                        proof, it can be accepted as ID proof.
                      </li>
                    </ul>
                  </li>
                  <li>
                    Cancelled original signed cheque which has the name printed
                    of the account holder or original Bank statement issued and
                    attested by bank for the proof of Bank Account Number.
                    <br></br>There is no charge/fee for registration as a Direct
                    Seller. After scrutiny of the application form and documents
                    and then the acceptance of applicant as Direct Seller, a
                    Unique/Track ID Number will be given on website. After
                    receiving ID Number, the Direct Seller has to obtain the ID
                    card by login their personal information on Website. One
                    Direct Seller can sponsor as many person as he/she desire.
                    Sponsor who is not Direct Seller can sponsor single new
                    person.
                  </li>
                </ul>
              </section>
              <h5 class="mp-content-heading">
                CALCULATION OF SALE INCENTIVE :-
              </h5>
              <section class="mp-content-section">
                <p>
                  Sales Incentive is calculated on a fixed percentage of
                  business volume of the purchase made by direct sellers and
                  their groups on differential basis as follows:
                </p>
                <div class="row">
                  <div class="col-sm-2">
                    <img src={SalesIn1} alt="" />
                  </div>
                  <div class="col-sm-2">
                    <img src={SalesIn2} alt="" />
                  </div>
                  <div class="col-sm-2">
                    <img src={SalesIn3} alt="" />
                  </div>
                </div>
              </section>
              <h5 class="mp-content-heading">PERFORMANCE BONUS :-</h5>

              <div class="col-sm-12">
                <img src={SalesIn4} alt="" />
              </div>
              <div class="table-wrap clearfix row">
                <div class="col-sm-6 wow fadeInLeft">
                  <table class="table table-bordred">
                    <thead>
                      <tr>
                        <th colspan="3">Example 1 :-</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th colspan="3">When one has business like</th>
                      </tr>
                      <tr>
                        <td colspan="2">Purchase in main leg (group)</td>
                        <td class="tb-value">80,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">Purchase in other leg /legs (group)</td>
                        <td class="tb-value">33,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">Self purchase</td>
                        <td class="tb-value">2,000 B.V</td>
                      </tr>
                      <tr>
                        <td colspan="2">Total Business Volume</td>
                        <td class="tb-value">1,15,000 B.V.</td>
                      </tr>
                      <tr>
                        <th colspan="3">Calculation of Performance Bonus :</th>
                      </tr>
                      <tr>
                        <td>Bonus of total group</td>
                        <td class="tb-value">1,15,000 x 24%</td>
                        <td class="tb-value">27,600/-</td>
                      </tr>
                      <tr>
                        <td>Less Bonus of main leg (group)</td>
                        <td class="tb-value">80,000 x 21.5%</td>
                        <td class="tb-value">17,200/-</td>
                      </tr>
                      <tr>
                        <td>Less Bonus of other leg/legs (group)</td>
                        <td class="tb-value">33,000 x 16.5%</td>
                        <td class="tb-value">5,445/-</td>
                      </tr>
                      <tr>
                        <th colspan="2">Net performance bonus</th>
                        <td class="tb-value">4,955/-</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-6 wow fadeInRight">
                  <table class="table table-bordred">
                    <thead>
                      <tr>
                        <th colspan="3">Example 2 :-</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th colspan="3">When one has business like</th>
                      </tr>
                      <tr>
                        <td colspan="2">Purchase in main leg (group)</td>
                        <td class="tb-value">1,18,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">Purchase in other leg /legs (group)</td>
                        <td class="tb-value">52,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">Self purchase</td>
                        <td class="tb-value">2,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">Total Business Volume</td>
                        <td class="tb-value">1,72,000 B.V.</td>
                      </tr>
                      <tr>
                        <th colspan="3">Calculation of Performance Bonus :</th>
                      </tr>
                      <tr>
                        <td>Bonus of total group</td>
                        <td class="tb-value">1,72,000 x 26.5%</td>
                        <td class="tb-value">45,580/-</td>
                      </tr>
                      <tr>
                        <td>Less Bonus of the main leg (group)</td>
                        <td class="tb-value">1,18,000 x 24%</td>
                        <td class="tb-value">28,320/-</td>
                      </tr>
                      <tr>
                        <td>Less Bonus of other leg/legs (group)</td>
                        <td class="tb-value">52,000 x 19%</td>
                        <td class="tb-value">9,880/-</td>
                      </tr>
                      <tr>
                        <th colspan="2">Net performance bonus</th>
                        <td class="tb-value">7,380/-</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-12 mp-content-div">
                  <label>Above examples are given for understanding.</label>
                  <p>
                    <b>Note :</b> Performance Bonus will be released to only
                    those Direct Sellers whose minimum purchase of RCM Products
                    is 100 Rs. on accrual basis in the relevant month.
                    Performance Bonus will be calculated on monthly Business
                    Volume on difference basis system (Deduction of performance
                    bonus of downline from performance bonus of total group).
                  </p>
                  <h5 class="mp-content-heading">ROYALTY :-</h5>
                </div>
                <div class="col-sm-8">
                  <img src={SalesIn5} alt="" />
                </div>
                <div class="col-sm-12 mp-content-div">
                  <b>Note :</b>
                  <ul class="listnumber">
                    <li>
                      Royalty will be released to only those Direct Sellers
                      whose minimum purchase of RCM Products is 1500 B.V. on
                      accrual basis in the relevant month.
                    </li>
                    <li>
                      Royalty will be calculated on difference basis by
                      deducting Royalty of downline from total Royalty.
                    </li>
                    <li>Royalty will be calculated on monthly basis.</li>
                    <li>
                      When B.V. of any other leg (group) except main leg (group)
                      is 3,50,000 or more, it is called the second leg (group)
                      and if B.V. of other leg/legs (other than main and second
                      leg) is 1,15,000 or more, then he will get Royalty on the
                      second leg (group) also as per the slab applicable.
                    </li>
                  </ul>
                  <h5 class="mp-content-heading">TECHNICAL BONUS :-</h5>
                  <img src={SalesIn6} alt="" />

                  <b>Note :</b>
                  <ul class="listnumber">
                    <li>
                      Technical Bonus will be released to only those Direct
                      Sellers whose minimum purchase of RCM Products is
                      equivalent to 1500 B.V. on accrual basis in the relevant
                      month.
                    </li>
                    <li>
                      Technical Bonus will be calculated on difference basis by
                      deducting Technical Bonus of downline from total Technical
                      Bonus of the main leg (group).
                    </li>
                    <li>
                      Technical Bonus will be calculated on monthly basis.
                    </li>
                    <li>
                      Direct Seller, who fulfills 8 % Royalty for consecutive 3
                      months, shall be entitled for Technical Bonus.
                    </li>
                    <li>
                      When B.V. of any other leg (group) except main leg (group)
                      is 5,00,000 or more, it is called the second leg and if
                      B.V. of other leg/legs (other than main and second leg) is
                      5,00,000 or more, then he/she will get Technical Bonus on
                      the second leg (group) also as per the slab applicable.
                    </li>
                  </ul>

                  <b>Terms & Conditions:-</b>
                  <ul class="listnumber">
                    <li>
                      Except for abnormal reasons calculation of Sales Incentive
                      shall be completed within 40 days from the last day of the
                      month, for which Sales Incentive is to be calculated.
                    </li>
                    <li>
                      The amount of Sales Incentive will be remitted within in
                      90 days from the date of calculation of the Sales
                      Incentive. Sales Incentive below Rs. 500 shall be remitted
                      once within one year from the date of the first accrual of
                      Sales Incentive.
                    </li>
                    <li>
                      Payment of Sales Incentive shall be made by anyone mode of
                      Banking System (NEFT/RTGS/INTER BANKING TRANSFER). For
                      this, it is mandatory to give correct bank account number
                      and IFSC detail by Direct Seller.
                    </li>
                    <li>
                      If Direct Seller does not receive payment due to
                      Non-compliance of rules of Direct Selling by Direct Seller
                      or by any other reason created by Direct Seller, then
                      complete responsibility for delay/ Non-payment will be of
                      Direct Seller
                    </li>
                    <li>
                      If any defect is found in any of the RCM Products
                      purchased, then the same can be returned/ exchanged within
                      30 days from the date of purchase. The returned product
                      must be supported with Bill of purchase and such product
                      should not be damaged from any angle. The purchaser should
                      ensure that condition of the product should be similar to
                      the condition which was prevailed at the time of
                      purchases.
                    </li>
                  </ul>
                </div>
              </div>
           

      </TabPanel>
      <TabPanel value={value} index={1}>
       
       
            <AccordionFull name="hpanel1" value={hpanel} title="A. बिजनस वोल्यूम (B.V.) :" onChange={handleHTabChange} expandIcon={true}  >
                <Typography>
                      हर उत्पाद की वह वेल्यू जिस पर विक्रय प्रोत्साहन राशि की
                      गणना होती है। यह उत्पादों के विक्रय मूल्य के बराबर या
                      विक्रय मूल्य से भिन्न भी हो सकता हैए जिसकी सूचना समय.समय
                      पर कम्पनी द्वारा वेबसाइट पर दी जाती है। बिजनस वोल्यूम में
                      समय.समय पर कम्पनी द्वारा परिवर्तन भी किया जा सकता है।
                </Typography>
            </AccordionFull>

            <AccordionFull name="hpanel2" value={hpanel} title="B. मेन लेग (समूह) व अन्य लेग (समूह) :-" onChange={handleHTabChange} expandIcon={true}  >
                <Typography>
                      डायरेक्ट सेलर का जिस लेग (समूह) का बिजनस वोल्यूम सबसे
                      ज्यादा होता है, उसे मेन लेग (समूह) कहा जाता है। उस लेग
                      (समूह) के अतिरिक्त लेगों (समूह) को मिलाकर अन्य लेग (समूह)
                      कहा जाता है। अलग-अलग माह में मेन लेग (समूह) अलग-अलग हो
                      सकती है।
                </Typography>
            </AccordionFull>

            <AccordionFull name="hpanel3" value={hpanel} title="C. विक्रय प्रोत्साहन राशि की डिफरेन्स आधार पर गणना :-" onChange={handleHTabChange} expandIcon={true}  >
                <Typography>
                      डायरेक्ट सेलर के पूरे समूह की विक्रय प्रोत्साहन राशि में
                      से डाउनलार्इन समूह की विक्रय प्रोत्साहन राशि को घटाकर नेट
                      विक्रय प्रोत्साहन राशि निकाली जाती है, इसे डिफरेन्स आधार
                      पर गणना कहते हैं। जिन डायरेक्ट सेलर्स की किसी भी कारणवश
                      विक्रय प्रोत्साहन राशि प्राप्त करने की पात्रता समाप्त हो
                      जाती है, उनकी विक्रय प्रोत्साहन राशि को डिफरेन्स आधार पर
                      गणना करते समय गणना से पृथक नहीं किया जायेगा।
                </Typography>
            </AccordionFull>

            <AccordionFull name="hpanel4" value={hpanel} title="D. प्रपोजर :-" onChange={handleHTabChange} expandIcon={true}  >
                <Typography>
                      जो डायरेक्ट सेलर नए व्यक्तियों को अपने समूह में डायरेक्ट
                      सेलर बनने के लिए प्रपोज करता है उसको प्रपोजर कहा जायेगा |
                </Typography>
            </AccordionFull>

            <AccordionFull name="hpanel5" value={hpanel} title="E. स्पॉन्सर :-" onChange={handleHTabChange} expandIcon={true}  >
                <Typography>
                      नए आवेदक के सबसे प्रथम अपलाइन को स्पॉन्सर कहा जायेगा |
                </Typography>
            </AccordionFull>

            <hr />
              <h5 class="mp-content-heading">डायरेक्ट सेलर बनने का तरीका :-</h5>
              <section class="mp-content-section">
                <p>
                  जॉइनिंग के लिए इच्छुक व्यक्ति को कम्पनी की वेबसाइट पर उपलब्ध
                  आवेदन फार्म पर आॅनलाईन आवेदन करना होता है। आवश्यक विवरण के
                  साथ, जिसने आवेदक को प्रपोज किया है उसका विवरण प्रपोजर के कॉलम
                  में व स्पोन्सर का विवरण स्पोन्सर के कॉलम में भरना होता है।
                  इसके साथ ही आवेदक का फोटो, पहचान पत्र, पते का प्रमाण पत्र व
                  बैंक खाते का प्रमाण पत्र की ओरिजनल स्केन कॉपी भी अपलोड करना
                  होता है। आवेदन के साथ दिये गये नियम व शर्तों को पढ़कर स्वीकार
                  करना होता है। उसके पश्चात आवेदन में दिये गये मोबाइल नम्बर पर
                  एक OTP आता है। वह OTP फीड करने के पश्चात आवेदन कम्पनी के पास
                  पहुँच जाता है। डायरेक्ट सेलर द्वारा आवेदन पत्र के साथ अपलोड
                  किये जाने वाले आवश्यक दस्तावेजों की सूचीः-
                </p>
                <ul class="lowerlatin">
                  <li>एक रंगीन पासपोर्ट सार्इज फोटो</li>
                  <li>
                    फोटो पहचान पत्र के लिए निम्न में से कोई एक दस्तावेज
                    <ul class="listnumber">
                      <li>पासपोर्ट (प्रामाणिक)</li>
                      <li>पेन कार्ड</li>
                      <li>मतदाता पहचान पत्र</li>
                      <li>ड्राइविंग लाइसेन्स (प्रामाणिक)</li>
                      <li>बैंक द्वारा जारी प्रमाणित फोटो पहचान पत्र</li>
                      <li>
                        मूल निवासी प्रमाण पत्र मय संचार पते एवं फोटो के साथ
                      </li>
                      <li>
                        केन्द्र/राज्य सरकार द्वारा प्रमाणित फोटो पहचान पत्र
                      </li>
                      <li>
                        निम्न में से किसी एक अधिकारी द्वारा प्रमाणित पत्र (फोटो
                        सहित)
                        <br />
                        पंचायत प्रधान
                        <br />
                        पार्षद
                        <br />
                        ग्राम पंचायत का सरपंच
                      </li>
                    </ul>
                  </li>
                  <li>
                    पता प्रमाण पत्र के लिए निम्न में से कोई एक दस्तावेज :-
                    <ul class="listnumber">
                      <li>टेलीफोन बिल जो 3 महिने से ज्यादा पुराना नहीं हो</li>
                      <li>
                        बैंक अकाउन्ट स्टेटमेन्ट (बैंक द्वारा प्रमाणित) 3 महिने
                        से ज्यादा पुराना नहीं हो
                      </li>
                      <li>बिजली का बिल जो 3 महिने से ज्यादा पुराना नहीं हो</li>
                      <li>राशन कार्ड</li>
                      <li>पासपोर्ट (प्रामाणिक)</li>
                      <li>ड्रार्इविंग लाइसेन्स (प्रामाणिक)</li>
                      <li>मतदाता पहचान पत्र</li>
                      <li>बैंक द्वारा जारी प्रमाणित फोटो पहचान पत्र</li>
                      <li>
                        किरायानामा, किराये की रसीद के साथ जो 3 महिने से पुरानी
                        नहीं हो
                      </li>
                      <li>
                        वर्तमान नियुक्ता प्रमाण पत्र जिसमें निवास पता अंकित हो
                      </li>
                      <li>
                        मूल निवासी प्रमाण पत्र मय संचार पते एवं फोटो के साथ
                      </li>
                      <li>
                        केन्द्र/राज्य सरकार द्वारा प्रमाणित पता प्रमाण पत्र
                        <br />
                        अवधि उपरांत पासपोर्ट पता प्रमाण पत्र के रूप में मान्य
                        नहीं होगा, पहचान पत्र के रूप में स्वीकार किया जा सकता
                        है।
                      </li>
                    </ul>
                  </li>
                  <li>
                    बैंक खाता संख्या के प्रमाण के लिए निरस्त किया हुआ हस्ताक्षर
                    सुदा एक आरिजनल चैक जिसमें कि खाताधारक का नाम प्रिन्ट हो या
                    बैंक द्वारा जारी व प्रमाणित किया हुआ आरिजनल बैंक स्टेटमेन्ट।
                  </li>
                </ul>
                <p>
                  डायरेक्ट सेलर के रूप में रजिस्ट्रेशन करवाने का कोई चार्ज फीस
                  नहीं है, आवेदन पत्र ऑनलाइ फीड करने के बाद आवेदक को एक
                  रेफ्रेन्स नम्बर प्रदान किया जाता है व कम्पनी द्वारा आवेदन जाँच
                  होकर स्वीकार किये जाने पर डायरेक्ट सेलर यूनिक आईडी ट्रेक आईडी
                  नंबर आवण्टित किया जाता है व इस आईडी नम्बर को वेबसाइट पर
                  दर्शाया जाता है। आईडी नम्बर प्राप्त करने के पश्चात डायरेक्ट
                  सेलर को अपना आईडी कार्ड वेबसाइट पर लॉग-इन करके अपनी पर्सनल
                  इनफार्मेशन से प्राप्त करना होता है। यदि आवेदन में कोई कमी पाये
                  जाने या अन्य किसी कारण से आवेदन स्वीकार नहीं होता है तो वह
                  आवेदक डायरेक्ट सेलर की श्रेणी में नहीं आता है।
                  <br />
                  एक डायरेक्ट सेलर अपनी स्वेच्छा से कितने भी लोगों को स्पॉन्सर
                  कर सकता/सकती है। जो स्पॉन्सर डायरेक्ट सेलर नहीं है वह एक नए
                  व्यक्ति को स्पॉन्सर कर सकता/सकती है।
                </p>
              </section>
              <h5 class="mp-content-heading">
                विक्रय प्रोत्साहन राशि की गणना :-
              </h5>
              <section class="mp-content-section">
                <p>
                  विक्रय प्रोत्साहन राशि की गणना डायरेक्ट सेलर व उसके समूह
                  द्वारा खरीद की गयी बिजनस वोल्यूम पर निश्चित प्रतिशत के आधार पर
                  की जाती है जिसका विवरण नीचे दिया गया है :-
                </p>
                <div class="row">
                  <div class="col-sm-2">
                    <img src={SalesIn1} alt="" />
                  </div>
                  <div class="col-sm-2">
                    <img src={SalesIn2} alt="" />
                  </div>
                  <div class="col-sm-2">
                    <img src={SalesIn3} alt="" />
                  </div>
                </div>
              </section>
              <h5 class="mp-content-heading">परफोरमेन्स बोनस :-</h5>

              <div class="col-sm-12">
                <img src={SalesIn4} alt="" />
              </div>
              <div class="table-wrap clearfix row">
                <div class="col-sm-6 wow fadeInLeft">
                  <table class="table table-bordred">
                    <thead>
                      <tr>
                        <th colspan="3">उदाहरण 1 :-</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th colspan="3">बिजनस की स्थिति</th>
                      </tr>
                      <tr>
                        <td colspan="2">लेग (समूह)में खरीद</td>
                        <td class="tb-value">80,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">लेग (समूह)में खरीद</td>
                        <td class="tb-value">33,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">स्वयं की खरीद</td>
                        <td class="tb-value">2,000 B.V</td>
                      </tr>
                      <tr>
                        <td colspan="2">कुल बिजनस वोल्यूम</td>
                        <td class="tb-value">1,15,000 B.V.</td>
                      </tr>
                      <tr>
                        <th colspan="3">परफोरमेन्स बोनस की गणना :-</th>
                      </tr>
                      <tr>
                        <td>कुल समूह का बोनस</td>
                        <td class="tb-value">1,15,000 x 24%</td>
                        <td class="tb-value">27,600/-</td>
                      </tr>
                      <tr>
                        <td>घटाएं A लेग (समूह) का बोनस</td>
                        <td class="tb-value">80,000 x 21.5%</td>
                        <td class="tb-value">17,200/-</td>
                      </tr>
                      <tr>
                        <td>घटाएं B लेग (समूह) का बोनस</td>
                        <td class="tb-value">33,000 x 16.5%</td>
                        <td class="tb-value">5,445/-</td>
                      </tr>
                      <tr>
                        <th colspan="2">नेट परफोरमेन्स बोनस</th>
                        <td class="tb-value">4,955/-</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-6 wow fadeInRight">
                  <table class="table table-bordred">
                    <thead>
                      <tr>
                        <th colspan="3">उदाहरण 2 :-</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th colspan="3">बिजनस की स्थिति</th>
                      </tr>
                      <tr>
                        <td colspan="2">लेग (समूह)में खरीद</td>
                        <td class="tb-value">1,18,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">लेग (समूह)में खरीद</td>
                        <td class="tb-value">52,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">स्वयं की खरीद</td>
                        <td class="tb-value">2,000 B.V.</td>
                      </tr>
                      <tr>
                        <td colspan="2">कुल बिजनस वोल्यूम</td>
                        <td class="tb-value">1,72,000 B.V.</td>
                      </tr>
                      <tr>
                        <th colspan="3">परफोरमेन्स बोनस की गणना :-</th>
                      </tr>
                      <tr>
                        <td>कुल समूह का बोनस</td>
                        <td class="tb-value">1,72,000 x 26.5%</td>
                        <td class="tb-value">45,580/-</td>
                      </tr>
                      <tr>
                        <td>घटाएं A लेग (समूह) का बोनस</td>
                        <td class="tb-value">1,18,000 x 24%</td>
                        <td class="tb-value">28,320/-</td>
                      </tr>
                      <tr>
                        <td>घटाएं B लेग (समूह) का बोनस</td>
                        <td class="tb-value">52,000 x 19%</td>
                        <td class="tb-value">9,880/-</td>
                      </tr>
                      <tr>
                        <th colspan="2">नेट परफोरमेन्स बोनस</th>
                        <td class="tb-value">7,380/-</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-12 mp-content-div">
                  <label>ये उदाहरण गणना को समझने के लिए दिये गये हैं।</label>
                  <p>
                    <b>नोट :</b> परफोरमेन्स बोनस उन्हीं डायरेक्ट सेलर्स को दिया
                    जायेगा जो सम्बन्धित माह में स्वयं भी कम से कम 100 रूपए की
                    आर.सी.एम. उत्पादों की खरीद करते हैं अथवा यह खरीद वे
                    सम्बन्धित माह की समाप्ति के 30 दिनों के भीतर कर लेते हैं।
                    बोनस की गणना मासिक आधार पर व डिफरेन्स आधार पर समूह के कुल
                    परफोरमेन्स बोनस में से डाउनलार्इन का परफोरमेन्स बोनस घटाकर
                    की जायेगी।
                  </p>
                  <h5 class="mp-content-heading">रोयल्टी :-</h5>
                </div>
                <div class="col-sm-8">
                  <img src={SalesIn5} alt="" />
                </div>
                <div class="col-sm-12 mp-content-div">
                  <b>नोट :</b>
                  <ul class="listnumber">
                    <li>
                      रोयल्टी उन्हीं डायरेक्ट सेलर्स को दी जायेगी जो सम्बन्धित
                      माह में स्वयं भी कम से कम 1500 बी.वी. के आर.सी.एम.
                      उत्पादों की खरीद करते हैं ।
                    </li>
                    <li>
                      रोयल्टी की गणना डिफरेन्स आधार पर कुल रोयल्टी में से
                      डाउनलार्इन की रोयल्टी घटाकर की जायेगी।
                    </li>
                    <li>रोयल्टी की गणना मासिक आधार पर की जायेगी।</li>
                    <li>
                      यदि किसी डायरेक्ट सेलर की मेन लेग (समूह) के अलावा अन्य
                      किसी लेग (समूह) का बी.वी. 3,50,000 या उससे ज्यादा है तो
                      उसे सैकण्ड लेग (समूह) कहा जायेगा व यदि मेन लेग (समूह) व
                      सैकण्ड लेग (समूह) के अलावा अन्य लेग/लेगों (समूह) का बी.वी.
                      1,15,000 या उससे ज्यादा होता है तो सैकण्ड लेग (समूह) पर भी
                      उपरोक्त स्लेब के अनुसार रोयल्टी मिलेगी।
                    </li>
                  </ul>
                  <h5 class="mp-content-heading">टेक्नीकल बोनस :-</h5>
                  <img src={SalesIn6} alt="" />

                  <b>नोट :</b>
                  <ul class="listnumber">
                    <li>
                      टेक्नीकल बोनस उन्हीं डायरेक्ट सेलर्स को दिया जायेगा जो
                      सम्बन्धित माह में स्वयं भी कम से कम 1500 बी.वी. के
                      आर.सी.एम. उत्पादों की खरीद करते हैं ।
                    </li>
                    <li>
                      टेक्नीकल बोनस की गणना डिफरेन्स आधार पर कुल टेक्नीकल बोनस
                      में से डाउनलार्इन का टेक्नीकल बोनस घटाकर की जायेगी।
                    </li>
                    <li>टेक्नीकल बोनस की गणना मासिक आधार पर की जायेगी।</li>
                    <li>
                      टेक्नीकल बोनस की पात्रता के लिए किन्हीं लगातार 3 माह में
                      8% रोयल्टी होनी चाहिए।
                    </li>
                    <li>
                      यदि किसी डायरेक्ट सेलर की मेन लेग (समूह) के अलावा अन्य
                      किसी लेग (समूह) का बी.वी. 5,00,000 या उससे ज्यादा है तो
                      उसे सैकण्ड लेग (समूह) कहा जायेगा व यदि मेन लेग (समूह) व
                      सैकण्ड लेग (समूह) के अलावा अन्य लेग/लेगों (समूह) का बी.वी.
                      5,00,000 या उससे ज्यादा होता है तो सैकण्ड लेग (समूह) पर भी
                      उपरोक्त स्लेब के अनुसार टेक्नीकल बोनस मिलेगा।
                    </li>
                  </ul>

                  <b>शर्तें :-</b>
                  <ul class="listnumber">
                    <li>
                      असामान्य कारणों को छोड़कर विक्रय प्रोत्साहन की देय राशि की
                      गणना सम्बन्धित माह की अंतिम तिथि के बाद 40 दिनों के भीतर
                      की जायेगी।
                    </li>
                    <li>
                      विक्रय प्रोत्साहन राशि का भुगतान गणना की समाप्ति के पश्चात
                      90 दिनों के भीतर किया जायेगा। यदि किसी डायरेक्ट सेलर की
                      कुल देय विक्रय प्रोत्साहन राशि 500रु. से कम रहती है तो
                      उसका भुगतान विक्रय प्रोत्साहन राशि के प्रथम उपार्जन तिथि
                      से एक वर्ष के भीतर सम्पूर्ण उपार्जित राशि का एक मुश्त किया
                      जायेगा।
                    </li>
                    <li>
                      विक्रय प्रोत्साहन राशि का भुगतान किसी एक बैंक प्रणाली
                      (NEFT/RTGS/ INTER BANKING TRANSFER) से किया जाता है, इसके
                      लिए डायरेक्ट सेलर का स्वयं का सही बैंक खाता संख्या व
                      सम्बनिधत बैंक आर्इएफएस कोड दर्ज होना आवश्यक है।
                    </li>
                    <li>
                      डायरेक्ट सेलर द्वारा डायरेक्ट सेलिंग की शर्तों का पालन
                      नहीं करने या डायरेक्ट सेलर द्वारा उत्पन्न किन्हीं कारणों
                      से विक्रय प्रोत्साहन राशि का भुगतान यदि बाधित होता है तो
                      उसकी समस्त जिम्मेदारी डायरेक्ट सेलर की होगी।
                    </li>
                    <li>
                      आरसीएम से खरीदे गये उत्पाद में यदि कोर्इ दोष पाया जाता है
                      तो ऐसे उत्पाद को खरीद दिनांक से 30 दिन के अन्दर वापस किया
                      जा सकता है। उसके बदले अन्य उत्पाद या राशि वापस ली जा सकती
                      है। उत्पाद वापसी के समय खरीद किया हुआ बिल प्रस्तुत करना
                      आवश्यक है। उत्पाद किसी भी तरह से क्षतिग्रस्त नहीं होना
                      चाहिए व उसकी गुणवत्ता खरीद किये गये समय की गुणवत्ता से
                      किसी भी प्रकार से भिन्न नहीं होनी चाहिए।
                    </li>
                  </ul>
                </div>
              </div>
           
           

      </TabPanel>

    </div>
  );
}
