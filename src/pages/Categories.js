import React from 'react'
import { Typography, Grid, Container, Hidden, Button,Menu,MenuItem } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Skeleton } from '@material-ui/lab'
import client from './../utils/client'
import BackToTop from './../components/BackToTop'
import { Hbox } from './../components/Box';
import Breadcrumbs from './../components/Breadcrumbs';
import Fill from './../components/Fill'
import Filter from './../components/FilterModule/Filter'
import ResponsiveTiles from './../components/ResponsiveTiles'
import ProductItem from './../components/product/productItem'
import ProductOptionSelector from './../components/option/ProductOptionSelector'
import CommonSnackbar from '../components/Snackbar';
import {isEmpty,findIndex, indexOf} from 'lodash';

const styles = theme => ({
    sideBar: {
      margin: theme.spacing(0, 4, 0, 0),
      width: 275,
    },
    sortButton: {
      [theme.breakpoints.down('xs')]: {
        flex: 1,
      },
    },
    total: {
      marginTop: theme.spacing(1),
    },
    button: {
      width: '100%'
    }
});

function SimpleMenu({items = []}) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div style={{
      display: 'flex',
    justifyContent: 'flex-end'
    }}>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        Sort
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >

        {
          items && items.map((item,key) => <MenuItem onClick={handleClose} key={key}>{item.name}</MenuItem>)
        }

      </Menu>
    </div>
  );
}


class Subcategory extends React.Component {
    constructor() {
      super();
      this.state = {
        loading: true,
        pageData: {
          attributes:[],
          description: "This is the description for subcategory 3.",
          breadcrumbs: [{ text: "Home", href: "/" }, { text: "Subcategory 3" }]
        },
        sortOptions: [
          {
            code: "price_asc",
            name: "Price - Low to High"
          },
          {
            code: "price_desc",
            name: "Price - High to Low"
          },
          {
            code: "pop",
            name: "Most Popular"
          },
          {
            code: "rating",
            name: "Highest Rated"
          }
        ],
        page: 0,
        recordCount: 0,
        fetching: false,
        hasMore: false,
        currentProductId: null,
        filters:[]
      }
      this._snackbar = null  
    }
  
    async componentWillMount() {
      this.fetchProducts();
    }
  
    componentWillReceiveProps(nextProps) {
      if (this.props.match.params && nextProps.match.params && this.props.match.params.id != nextProps.match.params.id) {
        this.setState({ loading: true, recordCount: 0, pageData: [], page: 0, hasMore: false }, () => this.fetchProducts());
      }
    }

    async fetchProducts(fetchMore = false, filtering = false) {
      let limit = 10;
      let { page = 1,attributes } = this.state;
      const { id } = this.props.match.params
      let prodsData = {
        ...this.state.pageData
      }
      page = page + 1;
      let name = 'SAREE';
      let catId = id;
      let level = 1
      if (fetchMore) {
        this.setState({ fetching: true, loading: false });
      }
      const { objectMap } = await client.get(`pu/getProductsREInit?catId=${catId}&limit=${limit}&page=${page}&lev=${level}&title=${name}`);
      let prods = objectMap.pcWrapper.products;
      let hasMore = prods.length < limit ? false : true;
      let newAttributes = !isEmpty(objectMap.attributes) ? objectMap.attributes:[]
      if(filtering) newAttributes = attributes;
      if (page > 1) {
        this.setState({
          pageData: {
            ...prodsData,
            products: prodsData.products.concat(prods)
          },
          loading: false,
          fetching: false,
          page,
          recordCount: objectMap.recordCount,
          hasMore,
          attributes:newAttributes,
          currentProductId: id
        });
  
      } else {
        this.setState({
          pageData: {
            title: name,
            name: name,
            products: prods,
            ...prodsData
          },
          page,
          loading: false,
          fetching: false,
          recordCount: objectMap.recordCount,
          hasMore,
          currentProductId: id,
          attributes:newAttributes
        })
      }
    }

    onChangeFilter = (attribute,value) => {
      let {filters} = this.state
      let index = findIndex(filters, o => o.attributeValueId == value.attributeValueId);
      if(index == -1){
        filters = filters.concat(value)
      }else{
        filters.splice(index,1)
      }
      this.setState({filters,loading: true, recordCount: 0, pageData: [], page: 0, hasMore: false }, () => this.fetchProducts(false,true))
    }
  
    render() {
      const { classes, theme } = this.props;
      const { loading, pageData,recordCount,hasMore,filters,attributes,sortOptions } = this.state;
      return (
        <>
        <Breadcrumbs items={pageData.breadcrumbs} />
        <CommonSnackbar ref={ ref => this._snackbar = ref }/>
        <Container maxWidth="lg" >
        <BackToTop/>
        <Hbox align="flex-start" style={{ display:'flex',alignItems:'baseline' }}>

            <Hidden implementation="css" xsDown>
              <div className={classes.sideBar}>
                <Hidden xsDown>
                  
                  <Filter 
                    classes={{ root: classes.sideBar }} 
                    facetGroups={attributes} 
                    filters={filters}
                    onChangeFilter={this.onChangeFilter}
                  />
                  
                </Hidden>
              </div>
            </Hidden>

            <Grid container style={{ position: 'relative' }}>

                <Grid item xs={12}>
                    {!loading ? (
                    <Typography component="h1" variant="h6" gutterBottom>
                        {pageData.name}
                    </Typography>
                    ) : (
                    <Skeleton height={32} style={{ marginBottom: theme.spacing(1) }} />
                    )}
                </Grid>

                

              <Grid item xs={12} style={{ display: 'flex',alignItems: 'center'}}>    

              <Grid item xs={6} >
                {loading ? (
                  <Skeleton
                    width={90}
                    height={14}
                    style={{ marginBottom: 4 }}
                    className={classes.total}
                  />
                ) : (
                  <Typography variant="caption" className={classes.total}>
                    <span>
                      {recordCount} total {recordCount === 1 ? 'item' : 'items'}
                    </span>
                  </Typography>
                )}
              </Grid>

                  <Grid item xs={6} >
                  {
                        !loading ? ( <SimpleMenu items={sortOptions} />
                        ):null
                  }
                  </Grid>
                 
              </Grid>  


              <Grid item xs={12}>
                    {
                        !loading ? (
                            <ResponsiveTiles autoScrollToNewTiles>
                                
                                {!isEmpty(pageData.products) && pageData.products.map((product, i) => (
                                <ProductItem key={product.id} product={product} index={i} colorSelector={true} />
                                ))}

                            </ResponsiveTiles>
                        )
                        :
                        (
                            <ResponsiveTiles>
                                {(() => {
                                const tiles = []
                                for (let i = 0; i < 10; i++) {
                                    tiles.push(
                                    <div
                                        key={i}
                                        style={{ marginTop: theme.spacing(2), marginBottom: theme.spacing(2) }}
                                    >
                                        <Fill height="100%" style={{ marginBottom: theme.spacing(1) }}>
                                        <Skeleton variant="rect" />
                                        </Fill>
                                        <Skeleton height={26} />
                                        <ProductOptionSelector
                                        skeleton={4}
                                        variant="swatch"
                                        size="small"
                                        optionProps={{
                                            size: 'small',
                                            showLabel: false,
                                        }}
                                        />
                                        <Skeleton height={18} />
                                        <Skeleton height={24} style={{ marginTop: '5px' }} />
                                    </div>
                                    )
                                }
                                return tiles
                                })()}
                            </ResponsiveTiles>
                        )
                    
                    }
              </Grid>
              <Grid item xs={12}>
                {!loading && hasMore && <Button variant="contained" color="primary" onClick={this.fetchProducts.bind(this)} style={{width:'100%'}}>
                        Load More
                </Button>}
              </Grid>
                
            </Grid>

        </Hbox>
        </Container>
        </>
      )
    }
}

export default withStyles(styles, { withTheme: true })(Subcategory)