import React from 'react'
import {Grid} from '@material-ui/core'
import DataTable from '../components/Common/DataTable';
import client from '../utils/client';
import SelectBox from '../components/Form/SelectBox';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';

const pucHeadCells = [
    { value: 'name',label: 'PUC Name' },
    { value: 'address', label: 'Address' },
    { value: 'pincode', label: 'Pincode' },
    { value: 'mobileNo', label: 'Contact No.' }
]
const depotHeadCells = [
    { value: 'comapnyName',label: 'Company Name' },
    { value: 'address', label: 'Company Address' },
    { value: 'city', label: 'City' },
    { value: 'mobileNo', label: 'District' },
    { value: 'pincode', label: 'Pincode' },
    { value: 'contactNo', label: 'Contact No.' },
]

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box >
            {children}
          </Box>
        )}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
  
class DeliveryCenter extends React.Component {
    constructor(){
        super();
        this.state = {
            states:[],
            districts:[],
            tab:0,
            title:'Delivery Center',
            form:{}
        }
        this._pucWiseTable = null;
        this._depotWiseTable = null;
    } 

    async componentDidMount(){
        let {objectMap} = await client.post('pu/getState');
        this.setState({states: objectMap.stateListDto })
    }

    onChange = (name,event) => {
        this.setState(prevState => ({form:{...prevState.form,[name]:event.target.value}}),() => {
            if(name == 'stateId') this.fetchDistricts();
            else if(name == 'districtId') this._pucWiseTable.onFilterChanged();
            else if(name == 'depotId') this._depotWiseTable.onFilterChanged();
        })
    }

    async fetchDistricts(){
        let {objectMap} = await client.post(`pu/getDistrictsByStateId/${this.state.form.stateId}`);
        this.setState({districts:objectMap.districtDto})
    }

    handleTabChange = (event, tab) => this.setState({tab})

    render(){
        const {states,districts,tab,form,title,filters} = this.state;
        return (
            <div style={{
                padding:20
            }}>

                <Paper>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleTabChange}
                    aria-label="disabled tabs example"
                >
                    <Tab label="Puc" />
                    <Tab label="Depot" />
                </Tabs>
                </Paper>

                <SwipeableViews index={tab} >
     
                    <TabPanel value={tab} index={0}>
                        
                        <DataTable ref={ref => this._pucWiseTable = ref} headCells={pucHeadCells}  title={title} fetchURL={ !form.districtId ? null:`pu/getPUCByDistrictID/${form.districtId}`} resField="pucDetailsDto" rightHeaderChildren={(
                               <Grid container spacing={2}  justify="flex-end" style={{marginTop:6}}> 
                                    <Grid  item xs={12} md={6}>
                                    <SelectBox items={states} label="Select State" name="stateId" value={form.stateId} onChange={this.onChange} itemValue="stateId" itemLabel="name" />
                                    </Grid>

                                    <Grid  item xs={12} md={6}>
                                    <SelectBox items={districts} label="Select District" name="districtId" value={form.districtId} onChange={this.onChange} itemValue="districtId" itemLabel="name" />
                                    </Grid>

                               </Grid>
                    )}/>
    
                    </TabPanel>

                    <TabPanel value={tab} index={1}>
                        
                        <DataTable ref={ref => this._depotWiseTable = ref} headCells={depotHeadCells} title={title} inData={true} fetchURL={ !form.depotId ? null:`pu/getDepoCenterByState/${form.depotId}`} rightHeaderChildren={(
                               <Grid container spacing={2} justify="flex-end" style={{marginTop:6}}> 
                                    <Grid  item xs={12} md={6}>
                                    <SelectBox items={states} label="Select State" name="depotId" value={form.depotId} onChange={this.onChange} itemValue="stateId" itemLabel="name" />
                                    </Grid>
                               </Grid>
                    )}/>
    
                    </TabPanel>
                </SwipeableViews>
            </div>
        )
    }
}

export default DeliveryCenter;