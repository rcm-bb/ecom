import React from 'react'
import {Grid} from '@material-ui/core'
  
class MediaGallary extends React.Component {
    constructor(){
        super();
        this.state = {
            links:[
                'https://www.youtube.com/embed/LtKPlCo1myI',
                'https://www.youtube.com/embed/jGwk_8mVdsg',
                'https://www.youtube.com/embed/dv6FhiB9bz0',
                'https://www.youtube.com/embed/htHloAJUUGE',
                'https://www.youtube.com/embed/cHPj3vwJzEM',
                'https://www.youtube.com/embed/0UL6CX2wz3w',
                'https://www.youtube.com/embed/SuAthDY25Mw',
                'https://www.youtube.com/embed/9U3LrxUrPJ8',
                'https://www.youtube.com/embed/luOyXeCL25k',
                'https://www.youtube.com/embed/GDTsJ8XQkTw',
                'https://www.youtube.com/embed/3d9wgZyZtEk',
                'https://www.youtube.com/embed/ClozcOcgGAA'
            ],
        }
    } 


    render(){
        console.log('links',this.state.links)
        return (
                <Grid container spacing={2} style={{width:'100%',marginTop:15}}>
                    {
                        this.state.links.map((link,i) => {
                            return (
                                <Grid item xs={6} sm={6} md={4} key={i} >
                                    <iframe  title={`iframe-${i+1}`}  src={link} width="100%" height="338" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen />
                                </Grid>
                            )
                        })
                    }
                </Grid>
        )
    }
}

export default MediaGallary;