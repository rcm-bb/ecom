import React from 'react';
import { withStyles } from '@material-ui/styles';
import DataTable from '../../components/Common/DataTable'
import DatePickerBox from '../../components/Form/DatePickerBox'
import DialogBox from '../../components/Common/DialogBox'
import client from '../../utils/client';
import {isEmpty,get} from 'lodash'
import SelectBox from '../../components/Form/SelectBox';
import PropTypes from 'prop-types';
import {
    Card,
    CardHeader,
    CardContent,
    Box,
    Divider,
    Grid,
    Tab,Tabs,Typography,
    Paper,
    List,ListItem,ListItemIcon,ListItemText,
    CircularProgress
  } from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';

const headCells = [
    {label:'Direct Seller ID',value:'sellerId'},
    {label:'Name',value:'name'},
    {label:'Sponsor',value:'sponser'},
    {label:'Business Volume',value:'bv'},
];


const styles = theme =>({
    root: {
        flex:1
    },
    TypographyPaddingY:{
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    }
});

const defaultFilter = {
    visitingLeader:'',
    stateId:0,
    fromDate: new Date()
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {children}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

class YourBusiness extends React.Component {
    constructor(){
        super();
        this.state = {
            fetchURL:'getMlmResp/GET_NODE_DATA/210982/210982',
            tab:0
        }
        this._dataTable = null;
    } 

    handleTabChange = (event, tab) => this.setState({tab})

    render(){
        const {tab,data,loading} = this.state;
        const { classes } = this.props;
        return (
            <div className={classes.root}>

                <Paper>
                <Typography variant="h6" gutterBottom>
                    Your Business
                </Typography>
                <Divider/>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleTabChange}
                    aria-label="disabled tabs example"
                >
                    
                    <Tab label="Aug-2020" />
                    <Tab label="Jul-2020" />
                    <Tab label="Jun-2020" />

                </Tabs>
                </Paper>

                <SwipeableViews index={tab} >

                    <TabPanel value={tab} index={0}>
                   
                    <DataTable showTitleBar={false} ref={ref => this._dataTable = ref} headCells={headCells} fetchURL={`pu/${this.state.fetchURL}`}  />
                    
                    </TabPanel>

                    <TabPanel value={tab} index={1}>

                    <DataTable showTitleBar={false}  ref={ref => this._dataTable = ref} headCells={headCells} fetchURL={`pu/${this.state.fetchURL}`}  />
                    
                    </TabPanel>

                    <TabPanel value={tab} index={2}>

                    <DataTable showTitleBar={false}  ref={ref => this._dataTable = ref} headCells={headCells} fetchURL={`pu/${this.state.fetchURL}`}  />
    
                    </TabPanel>

                </SwipeableViews>
            </div>
        )
    }
}

YourBusiness.propTypes = {
    className: PropTypes.string
  };
  
  export default withStyles(styles)(YourBusiness);