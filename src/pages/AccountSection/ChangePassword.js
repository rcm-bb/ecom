import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import SpinnerButton from '../../components/SpinnerButton';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  TextField
} from '@material-ui/core';
import {isEmpty,unset} from 'lodash'
import validate from 'validate.js';
import client from '../../utils/client';
import CommonSnackbar from '../../components/Snackbar';

const schema = {
   oldPassword: {
      presence: { allowEmpty: false, message: '^password is required' },
      length: {
        minimum: 6,
        message: "password must be at least 6 characters"
      }
    },
    password: {
      presence: { allowEmpty: false, message: '^new password is required' },
      length: {
        minimum: 6,
        message: "new password must be at least 6 characters"
      }
    },
    confirmPassword: {
        presence: { allowEmpty: false, message: '^confirm password is required' },
        length: {
          minimum: 6,
          message: "confirm password must be at least 6 characters"
        },
        equality: "password"
    }
};
  

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    },
    formControl: {
        width: '100%',
    },
});


class ChangePassword extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:false,
            errors:{}
        }
        this._snackbar = null;
    }

    handleChange = event => {
        console.log('event',event)
        let {form,errors} = this.state;
        let name = event.target.name;
        unset(errors,name)
        this.setState({
            form:{
                ...form,
                [name] : event.target.value
            },
            errors
        });
    };

    onSubmit = async () => {
        const {form} = this.state;
        console.log('form',form)
        let errors = validate(form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({loading:true,errors})
        const res = await client.post('updatePassword',form);
        console.log('res',res)
        if(res.hasError){
          this._snackbar.openSnack(res.errors[0].errorMessage,'error');
          this.setState({loading:false})
        }else{
          this._snackbar.openSnack('password has been changed successfully.');
          this.setState({loading:false,form:{}})
        }
       
    }

    hasError = field =>  this.state.errors[field] ? true : false;


    render(){
        const { classes } = this.props;
        const { form,errors } = this.state;
        
        return (
            <div className={classes.root}>
                <CommonSnackbar ref={ ref => this._snackbar = ref }/>
            <Grid
              container
              spacing={4}
            >
        
            <Grid item  md={12} xs={12} >
            <Card>
              <form
                autoComplete="off"
                noValidate
              >
                <CardHeader title="Change Password" />
                <Divider />
                <CardContent>
                  <Grid
                    container
                    spacing={3}
                  >


                    <Grid item md={6} xs={12} >
                        
                        <TextField
                          fullWidth
                          label="Current password"
                          name="oldPassword"
                          onChange={this.handleChange}
                          required
                          value={form.oldPassword || ''}
                          variant="outlined"
                          type="password"
                          error={this.hasError('oldPassword')}
                          helperText={
                              this.hasError('oldPassword') ? errors.oldPassword[0] : null
                          }
                        />
  
                      </Grid>

                      <Grid item md={6} xs={12} >
                        
                        <TextField
                          fullWidth
                          label="New Password"
                          name="password"
                          type="password"
                          onChange={this.handleChange}
                          required
                          value={form.password || ''}
                          variant="outlined"
                          error={this.hasError('password')}
                          helperText={
                              this.hasError('password') ? errors.password[0] : null
                          }
                        />
  
                      </Grid>

                      <Grid item md={6} xs={12} >

                        <TextField
                            fullWidth
                            label="Confirm password"
                            name="confirmPassword"
                            type="password"
                            onChange={this.handleChange}
                            required
                            value={form.confirmPassword || ''}
                            variant="outlined"
                            error={this.hasError('confirmPassword')}
                            helperText={
                                this.hasError('confirmPassword') ? errors.confirmPassword[0] : null
                            }
                        />

                      </Grid>
        
                  </Grid>
                </CardContent>
                
                <Divider />
                <CardActions>
        
                <SpinnerButton title="Save" handleButtonClick={this.onSubmit} loading={this.state.loading}  />
        
                </CardActions>
              </form>
            </Card>
            </Grid>

            </Grid>
            </div>
        )
    }
}

ChangePassword.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(ChangePassword);