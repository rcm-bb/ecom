import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Box,
  Divider,
  Grid,
  Tab,Tabs,Typography,
  List,ListItem,ListItemIcon,ListItemText
} from '@material-ui/core';
import {isEmpty} from 'lodash';
// import client from '../../utils/client';
import GetAppIcon from '@material-ui/icons/GetApp';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';

const styles = theme => ({
  root: {
    padding: theme.spacing(4)
  },
  cardContainer:{
    
  },
  cardMarginBottom: {
    marginBottom: theme.spacing(2),
  },
});




class AccountMain extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:true,
            data:[],
            selectedIndex:0
        }
    }

    async componentDidMount(){
        // const {objectMap} = await client.post('pu/getActiveNotice');
        // let data = !isEmpty(objectMap) && !isEmpty(objectMap.noticeList) ? objectMap.noticeList:[];
        // this.setState({data,loading:false})
    }

    render(){
        const { classes } = this.props;
        const {data} = this.state;
        const handleListItemClick = (event, index) => {
          this.setState({selectedIndex:index});
        };
        
        return (
            <div className={classes.root}>
               
            <Grid
              container
              spacing={4}
            >
             
                    <Grid  md={12} xs={12}>
                    <Card className={classes.cardMarginBottom}>
                
                        <CardHeader
                          subheader="SMS Services"
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >
                          <Typography variant="subtitle1" gutterBottom>
                            For Current Month Business :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>CUR<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Previous Month Business :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>BUS<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Self Repurchase :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>SELF<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Register Mobile Number Change :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>MOB<space><DSID><space><PASSWORD><space><MOBILE> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Email Address Change :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>EMAIL<space><DSID><space><PASSWORD><space><EMAILID> Send to 9910418866'}
                          </Typography>
                        </CardContent>
                
                    </Card>
                    <Card >
                
                        <CardHeader
                          subheader="Download Forms for PUC"
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >
                        <List component="nav">
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 0}
                              onClick={(event) => handleListItemClick(event, 0)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Reactivation & Mobile No. Change"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 1}
                              onClick={(event) => handleListItemClick(event, 1)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Security Refund (Hindi)"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 2}
                              onClick={(event) => handleListItemClick(event, 2)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Security Refund (English)"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 3}
                              onClick={(event) => handleListItemClick(event, 3)}
                            >
                              <ListItemIcon>
                                <FormatListBulletedIcon />
                              </ListItemIcon>
                              <ListItemText primary={"List of PUC to Purchase Paints"} />
                            </ListItem>
                          </List>
                        </CardContent>
                    </Card>
                    </Grid>
            </Grid>
            </div>
        )
    }
}

AccountMain.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(AccountMain);