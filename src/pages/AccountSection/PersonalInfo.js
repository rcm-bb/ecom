import React from 'react';
import { withStyles } from '@material-ui/styles';
import DataTable from '../../components/Common/DataTable'
import DatePickerBox from '../../components/Form/DatePickerBox'
import DialogBox from '../../components/Common/DialogBox'
import client from '../../utils/client';
import {isEmpty,get} from 'lodash'
import SelectBox from '../../components/Form/SelectBox';
import PropTypes from 'prop-types';
import {
    Card,
    CardHeader,
    CardContent,
    Box,
    Divider,
    Grid,
    Tab,Tabs,Typography,
    Paper,
    List,ListItem,ListItemIcon,ListItemText,
    CircularProgress
  } from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';

const styles = theme =>({
    root: {
        flex:1
    },
    TypographyPaddingY:{
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    },
    gridListRoot:{
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 60
    },
    loaderRoot:{
        minHeight: 300,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const defaultFilter = {
    visitingLeader:'',
    stateId:0,
    fromDate: new Date()
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box >
            {children}
          </Box>
        )}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const GridListItem = ({title,value,classes}) => (
    <Grid item md={12} xs={12}>
    <Divider/>
    <div className={classes.gridListRoot}>
     <Grid item xs={4} md={4} className={classes.TypographyPaddingY}>
        <Typography variant="subtitle2" >
        {title}
        </Typography>
    </Grid>
    <Grid item xs={8} md={8} >
        <Typography variant="subtitle2" >
        {value}
        </Typography>
    </Grid>
    </div>
    </Grid>
);

function LIComponent({classes,loading,items = {},lables = []}){

    const getItemValue = (item) => {
        let value = get(items,item.value)
        if(item.type == 'dob'){
            value = new Date(value)
            .toLocaleDateString("en-GB", {
              day: "numeric",
              month: "short",
              year: "numeric",
            })
            .split(" ")
            .join("-")
        }
        value = !isEmpty(value) ? value:'-'
        return value;
    }
    if(loading) return (
        <div className={classes.loaderRoot}>
            <CircularProgress/>
        </div>
    )
    return (
     <Grid container >
         {
             !isEmpty(lables) && lables.map((item,index) => <GridListItem key={index} title={item.title} value={getItemValue(item)} classes={classes} />)
         }
     </Grid>
    )
} 

const ListItemContainer = withStyles(styles)(LIComponent);

const sponseValues = [
    {title:'Sponsor Number',value:'sponsorId'},
    {title:'Sponsor Name',value:'sponsorName'},
    {title:'Proposer Number',value:'proposerId'},
    {title:'Proposer Name',value:'proposerName'}
];

const profileValues = [
    {title:'Join Date',value:'approvalDate'},
    {title:'Applicant Name',value:'name'},
    {title:"Father's Name",value:'fathersName'},
    {title:'Husband Name',value:'husbandName'},
    {title:'Nominee Name',value:'nomineename'},
    {title:'Applicant Date of Birth',value:'applicantDob',type:'dob'},
    {title:'Nominee Date of Birth',value:'nomineeDOB',type:'dob'},
    {title:'Aadhaar Card Number',value:'aadharCardNo'},
    {title:'Kyc Status',value:'kycStatus'},
];

const bankDetailValues = [
    {title:'Bank Name',value:'bankName'},
    {title:'Branch Name',value:'branchName'},
    {title:'Ifsc Code',value:'ifscCode'},
    {title:'Bank Account Number',value:'bankAccountNumber'}
];

const communicationValues = [
    {title:'House No./Location',value:'houseNo'},
    {title:'City',value:'city'},
    {title:'Post',value:'post'},
    {title:'Tehsil',value:'tehsil'},
    {title:'Distirct',value:'district'},
    {title:'State',value:'state.name'},
    {title:'Pincode',value:'pincode'},
    {title:'Phone Number',value:'registeredMobNo'},
    {title:'Mobile Number',value:'registeredMobNo'},
    {title:'Email Address',value:'email'}
]


class PersonalInfo extends React.Component {
    constructor(){
        super();
        this.state = {
            states:[],
            leaders:[],
            tab:0,
            title:'',
            fetchURL:'',
            filters:defaultFilter,
            profile:[],
            loading:true
        }
        this._leaderWiseTable = null;
        this._dateWiseTable = null;
        this._stateWiseTable = null;
    } 

    async componentDidMount(){
        let {objectMap} = await client.post('pu/getDSNominee');
        let {objectMap: backObjectMap} = await client.post('pu/getDSBankDetails');
        let {objectMap: communicationObjectMap} = await client.post('pu/getDSCommunication');
        console.log('objectMap',objectMap,backObjectMap,backObjectMap)
        let nomineeDto = isEmpty(objectMap) && isEmpty(objectMap.nomineeDto) ? []:objectMap.nomineeDto;
        let bankDto = isEmpty(backObjectMap) && isEmpty(backObjectMap.bankDto) ? []:backObjectMap.bankDto;
        let communicationDto = isEmpty(communicationObjectMap) && isEmpty(communicationObjectMap.communicationDto) ? []:communicationObjectMap.communicationDto;
        this.setState({profile:{
            ...nomineeDto,
            ...bankDto,
            ...communicationDto
        },loading:false})
    }

    handleTabChange = (event, tab) => this.setState({tab})

    render(){
        const {tab,profile,loading} = this.state;
        const { classes } = this.props;
        return (
            <div className={classes.root}>

                <Paper>
                <Typography variant="h6" gutterBottom>
                    Personal Information
                </Typography>
                <Divider/>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleTabChange}
                    aria-label="disabled tabs example"
                >
                    
                    <Tab label="Sponsor/Proposer" />
                    <Tab label="Profile" />
                    <Tab label="Communciation" />
                    <Tab label="Bank" />

                </Tabs>
                </Paper>

                <SwipeableViews index={tab} >

                    <TabPanel value={tab} index={0}>
                       
                     <ListItemContainer lables={sponseValues} items={profile} loading={loading} />
                    
                    </TabPanel>

                    <TabPanel value={tab} index={1}>

                     <ListItemContainer lables={profileValues} items={profile} loading={loading} />
                    
                    </TabPanel>

                    <TabPanel value={tab} index={2}>

                     <ListItemContainer lables={communicationValues} items={profile} loading={loading} />
    
                    </TabPanel>

                    <TabPanel value={tab} index={3}>

                     <ListItemContainer lables={bankDetailValues} items={profile} loading={loading} />
    
                    </TabPanel>

                </SwipeableViews>
            </div>
        )
    }
}

PersonalInfo.propTypes = {
    className: PropTypes.string
  };
  
  export default withStyles(styles)(PersonalInfo);