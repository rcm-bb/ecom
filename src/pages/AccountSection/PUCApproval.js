import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import DataTable from '../../components/Common/DataTable';
import clinet from './../../utils/client';
import SelectBox from '../../components/Form/SelectBox';
import { Grid } from '@material-ui/core';

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    },
    formControl: {
        width: '100%',
    },
});

const headCells = [
    {label:'Application Date',value:'created',type:'date'},
    {label:'Application Type',value:'pucType',type:'puc'},
    {label:'App No.',value:'pucDetailsId'},
    {label:'Applicant Name',value:'name'},
    {label:'Keysoul Store Name',value:'org.name'},
    {label:'Keysoul Store Address',value:'address'},
    {label:'City',value:'villageOrCity'},
    {label:'District',value:'districtId.name'},
    {label:'State',value:'state.name'},
    {label:'Mobile',value:'mobileNo'},
    {label:'Approve',value:'',button:'CheckBoxIcon',actionType:'approve'},
    {label:'Reject',value:'',button:'CancelIcon',actionType:'reject'}
];

const selectItems = [
    {title:'Royalty Approval',value:'royalty'},
    {title:'Technical Approval',value:'technical'}
]


class PUCApproval extends React.Component {

    constructor(){
        super();
        this.state = {
            data:[],
            loading:false,
            fetchURL:'getPUCTechnicalVerificationList',
            filters:{
                pucmode:'technical'
            }
        }
        this._snackbar = null;
        this._dataTable = null;
    }

    onChange = (name,event) => {
        let {fetchURL} = this.state;
        let value = event.target.value;
        if(name == 'pucmode'){
            if(value == 'technical') fetchURL = 'getPUCTechnicalVerificationList';
            else fetchURL = 'getPUCRoyaltyVerificationList'; 
        } 
        this.setState(prevState => ({filters:{...prevState.filters,[name]: value},fetchURL}),() => {
            if(name == 'pucmode') this._dataTable.fetch();
        })
    }
    

    onActionClick = async (row,head) => {
        console.log('onActionClick',row,head)
        let authorizeId = row.pucAuthorize.pucAuthorizeId;
        let type = head.actionType == 'approve' ? 'Y':'N';
        let actionType = this.state.fetchURL == 'getPUCTechnicalVerificationList' ? 'isTechnicalApproved':'isRoyaltyApproved'
        let res =  await clinet.post(`pu/setPUCVerification/${authorizeId}/${type}/${actionType}`);
        console.log('ress',res);
        this._dataTable.fetch();
    }

    render(){
        const { classes } = this.props;
        const {fetchURL} = this.state
        return (
            <div className={classes.root}>
                <DataTable ref={ref => this._dataTable = ref}  title={'Keysoul Store Application Royalty Achiever Verification'} fetchURL={`pu/${fetchURL}`} 
                    resField={fetchURL == 'getPUCRoyaltyVerificationList' ? 'verificationRoyaltyListOfPUC':'verificationListOfTechnicalPUC'} headCells={headCells} onActionClick={this.onActionClick} 
                    rightHeaderChildren={(
                        <Grid container spacing={2} justify="flex-end" style={{marginTop:6}}> 
                             <Grid  item xs={12} md={6}>
                             <SelectBox items={selectItems} label="PUC Approval Type" name="pucmode" value={this.state.filters.pucmode} onChange={this.onChange} />
                             </Grid>
                        </Grid>
             )}
              />
            </div>
        )
    }
}

PUCApproval.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(PUCApproval);