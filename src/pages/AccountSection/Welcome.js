import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Box,
  Divider,
  Grid,
  Tab,Tabs,Typography,
  Paper,
  List,ListItem,ListItemIcon,ListItemText
} from '@material-ui/core';
import {isEmpty} from 'lodash';
import client from './../../utils/client';
import GetAppIcon from '@material-ui/icons/GetApp';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';

const styles = theme => ({
    root: {
        flex:1,
        flexGrow: 1,
        flexWrap: 'wrap',
        '& > *': {
          margin: theme.spacing(1),
          width: theme.spacing(16),
          height: theme.spacing(16),
        },
      },
      paper:{
          width:'100%',
          height:'auto',
          flex:1,
          padding:theme.spacing(1)
      },
      cardMarginY: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
      },
});


class Welcome extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:true,
            data:[],
            selectedIndex:0
        }
    }

    async componentDidMount(){
        // const {objectMap} = await client.post('pu/getActiveNotice');
        // let data = !isEmpty(objectMap) && !isEmpty(objectMap.noticeList) ? objectMap.noticeList:[];
        // this.setState({data,loading:false})
    }

    render(){
        const { classes } = this.props;
        const {data} = this.state;
        const handleListItemClick = (event, index) => {
            this.setState({selectedIndex:index});
          };
        console.log("Classes", classes);

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={3}>
                    <Typography variant="h6" gutterBottom>
                        Welcome
                    </Typography>
                    <Divider/>
                    <Typography py={2} variant="body2" gutterBottom>
                        [Name]
                    </Typography>

                    <Card variant="outlined" className={classes.cardMarginY}>
                
                        <CardHeader
                          subheader={'Self Repurchase of Month - {month_name}'}
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >
                            <Grid container>
                            <Grid item xs={12} md={6}>
                                <Typography variant="subtitle1" gutterBottom>
                                    Amount
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    [100]
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Typography variant="subtitle1" gutterBottom>
                                    Business Volume
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    
                                </Typography>
                            </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                    <Card variant="outlined" className={classes.cardMarginY}>
                
                        <CardHeader
                          subheader="SMS Services"
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >
                          <Typography variant="subtitle1" gutterBottom>
                            For Current Month Business :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>CUR<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Previous Month Business :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>BUS<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Self Repurchase :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>SELF<space><DSID><space><PASSWORD> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Register Mobile Number Change :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>MOB<space><DSID><space><PASSWORD><space><MOBILE> Send to 9910418866'}
                          </Typography>
                          <Divider />
                          <Typography variant="subtitle1" gutterBottom>
                            For Email Address Change :
                          </Typography>
                          <Typography variant="body2" gutterBottom>
                            {'RCM<space>EMAIL<space><DSID><space><PASSWORD><space><EMAILID> Send to 9910418866'}
                          </Typography>
                        </CardContent>
                
                    </Card>
                    <Card variant="outlined" >
                
                        <CardHeader
                          subheader="Download Forms for PUC"
                        />
                        <Divider />
                        <CardContent className={classes.cardContainer} >
                        <List component="nav">
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 0}
                              onClick={(event) => handleListItemClick(event, 0)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Reactivation & Mobile No. Change"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 1}
                              onClick={(event) => handleListItemClick(event, 1)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Security Refund (Hindi)"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 2}
                              onClick={(event) => handleListItemClick(event, 2)}
                            >
                              <ListItemIcon>
                                <GetAppIcon />
                              </ListItemIcon>
                              <ListItemText primary={"PUC Security Refund (English)"} />
                            </ListItem>
                            <Divider />
                            <ListItem
                              button
                              selected={this.state.selectedIndex === 3}
                              onClick={(event) => handleListItemClick(event, 3)}
                            >
                              <ListItemIcon>
                                <FormatListBulletedIcon />
                              </ListItemIcon>
                              <ListItemText primary={"List of PUC to Purchase Paints"} />
                            </ListItem>
                          </List>
                        </CardContent>
                    </Card>
                    
                </Paper>
            </div>
        )
    }
}

Welcome.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(Welcome);