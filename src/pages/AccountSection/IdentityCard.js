import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Divider,Card,CardContent,CardHeader,Button} from '@material-ui/core'
import client from './../../utils/client';
import {isEmpty} from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
   flex:1
  },
  contentRoot:{
    margin:'5%'
  },
  paper: {
    border:'1px solid black'
  },
  logo:{
    width:150
  },
  gridItem:{
    padding:'10px 15px !important'
  },
  typoText:{
    fontWeight:'bold'
  },
  gridItemRight:{
    padding:'10px 15px !important',
    textAlign:'right'
  },
}));


export default function AutoGridNoWrap() {
  const classes = useStyles();
  const [data,setData] = React.useState({});
  
  const fetch = async () => {
      const {objectMap} = await client.post('pr/getProfileDetailsForDirectSeller');
      console.log('objectMap',objectMap.personalinfo)
      const data = !isEmpty(objectMap) && !isEmpty(objectMap.personalinfo) ? objectMap.personalinfo:{};
      if(!isEmpty(data)){
          let {houseNo,bPartner,registeredMobNo,email} = data;
          let name,sellerId;
          if(!isEmpty(bPartner)){
            name = bPartner.name;
            sellerId = bPartner.sellerID;
          }
        setData({houseNo,registeredMobNo,email,name,sellerId});
      }else{
        setData(data);  
      }
  }

  React.useEffect(() => {
    fetch();
  },[]);

  return (
    <div className={classes.root}>
      <div>

      <CardHeader
        action={
          <Button variant="outlined">Print</Button>
        }
        title="Identity Card"
      />
    <Divider />
    <CardContent className={classes.contentRoot}>
      <Paper className={classes.paper}>
        <Grid container  >
          <Grid item xs={6} md={6} classes={{root:classes.gridItem}}>
            <img className={classes.logo} src="http://localhost:3001/static/media/logo.0961210e.png" alt="logo" />
          </Grid>
          <Grid item xs={6} md={6} justify="flex-end" classes={{root:classes.gridItemRight}}>
            <Typography variant="h6" >Fashion Suitings Pvt.Ltd.</Typography>           
            <Typography variant="caption" component="div" >Spl-7, RIICO Growth Center</Typography>           
            <Typography variant="caption" component="div" >HameerGarh, Bhilwara, Rajasthan</Typography>           
           </Grid>
          
           <Grid item xs={12} md={12}>
           <Divider />
           </Grid>
           
           <Grid item xs={8} md={8} classes={{root:classes.gridItem}}>
            <Typography variant="overline" component="div" className={classes.typoText}>Direct Seller ID: <Typography variant="caption">{data.sellerId}</Typography> </Typography>      
           
            <Typography variant="body2" component="div">{data.name}</Typography>      
           
    <Typography variant="overline" component="div" className={classes.typoText}>Address: <Typography variant="caption"> {data.houseNo} </Typography> </Typography>      

            <Typography variant="overline" component="div" className={classes.typoText}>Contact: <Typography variant="caption"> {data.registeredMobNo}</Typography> </Typography>      

            <Typography variant="overline" component="div" className={classes.typoText}>Email ID: <Typography variant="caption"> {data.email} </Typography> </Typography>      

           </Grid>

           <Grid item xs={8} md={8} classes={{root:classes.gridItem}}>
            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAyADIAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAdAEsDASIAAhEBAxEB/8QAGwAAAgMBAQEAAAAAAAAAAAAAAAgFBgkHBAr/xAAsEAAABwABAwMDAwUAAAAAAAABAgMEBQYHEQAIEhMUIQkWMRUXUSIyQVRh/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APv46OuCaX3JY7k8uzrVrtTl5cJIE/Z0WkVW36bobojny9soSh5tX7RcPZiUfh2MOBBACiBx5ERiIvXNXuItFaV292yFi3gCITu0WOu5024+QKJK5XB0a9lMJQHyJYK1WxARD+r5ATBB92s/p9QpmZ2/MZ5vDqQHcf2+t9Cj3zUzlCz5NdNTr+baJDlEAEUzs4W8ms7RQRJ4K1shSiJjF4a7rNLvFpXc3Zu2jcpOw6ll1Ohq3m9iu41nP8zl56edOaG0G5NOL1eLgMeBgNXQKIftuUTB8iYfEAFj5DA1rvDslZ/uG7h5Ns5YguV5AXeFzMzlu6AHJVBHK6hRTAIkN8GEfMCCPl8CIdAz3R0nyXYz2yi4QlbJSbHocqzblKlLa9rGw686AG/4ETapf7f4/wA8iXjj55/A9MjTICn1irwsFQouFh6e0jyjAxtbaNW0ESOcCLooxRGfDQqLkzj1CiQwlU8jKF+DCcQuHR0dHQHR0dHQLDbu1+h2W7TGlQ1l1fLr1ZVGbizy2V6dcKdGW13FRUZExsrbac1kXdDsMwyiIWOh0J+YqK0sSHISIBcEvFMlxHLbokfll3B7CgTkR9FzFYnJNgH1+RDyd4+Z7z+eRB5yBPwIiUOe29HQIbueB9x1ooGsxcD3ezjKGsmb3WvhBWjE8sn27QZWtyDH3Td9W4+nywiT1BECGOoIiIiQPIC9XbNKj3HqZtnro+70pR0vUao4dGkMJH3AlcQ0eYzYxWeosikMBhEPLjyEQARHjjhrX7FCSZO49yKnoSDVy0XFM/gqVB0iKSxUlOBEgiUR4Hg3A8fAgHHXgrsIyrMFE1yLFx+m1yLjoKP924UdujMY1i1atfdOlhMs4XKkQAMqYweQiI+IcgABgdsfY79RK5dzll27RtUz/cs0VurSo57klGltUzEWmc3zH7jj0z9yQ0vqI0TOaJTrJev3f0j7CZXzbdUGG4+8BiKdQ826YHtixDvh7QoBGrMJ627bjub2S2AOY26WyB3c7tTZWYkWFZDt+sbJhTjVElciAa6PZa3tliEljuFhmc4oY5rQ6xWzS2vMjHklGEhGquXzVF40dMDrxrxeOfokdNygLhm/aGSdsnbfyMLVw1VSMiYQOACcpRKt9b7UKHVZdKda6L3Oy7tusqoVraO7PuQs0MosqAD6q0FMac7h1AS5H0W/sgZk/wBYegWOOafUv+0q3CSzikyFqrdqr+0S9zQladX/ANwawsdlZZvs5fRLOBmWNblmks+sWcJ7YxZEiQoNdotmOM5fJq3AjWNfuX1KajM0OxVysZDKz05EaCe0UyOfW+0YpRqhnVIgb0aZlpkYKhXJ1qWn2+Ie5ZSEzS4VioN7ka0mTugVBRGY0ZXymuOo/wDS3EzpB2no+PknrWntX/H8/rDO2t5jn/vv+oAuB0JRuo1Vk9aXQUIugsm43/eHALpGNwYqwraQcT8h/kR6DqFfnWVlhIaxRapV46ch42Yj10TAsg4ZyjQrxscolD8emoUeQEvICUBEBHgJ7pS1uy7DFK7G1lgrtldiYqOjoiERqfdH3N1VWCYRjJu0jEYhxA68wWaBHINkStBA5xS8A/uARAWagINvX4OIgm72ZkEIaNZRSD+dmpOcm3qUe3TaJu5iak3LiSl5RwRIFX8nIOF3r90dV06WVXVUOYP/2Q==" alt="auth-sign" />
            <Typography variant="overline" component="div">Authorized Signatory</Typography>                 
           </Grid>
           
          
        </Grid>
      </Paper>
      </CardContent>
      </div>
 
    </div>
  );
}
