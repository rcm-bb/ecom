import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Box,
  Divider,
  Grid,
  Tab,Tabs,Typography,
  List,ListItem,ListItemIcon,ListItemText
} from '@material-ui/core';
import {isEmpty} from 'lodash';
// import client from '../../utils/client';
import GetAppIcon from '@material-ui/icons/GetApp';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Welcome from './Welcome';
import PersonalInfo from './PersonalInfo';
import ChangePassword from './ChangePassword';
import AccountProductTraining from './ProductTraining';
import PUCApproval from './PUCApproval';
import IdentityCard from './IdentityCard';
import YourBusiness from './YourBusiness';
import PUCApplication from './PUCApplication';

const styles = theme => ({
  root: {
    padding: theme.spacing(4)
  },
  tabpanel:{
    width:'100%',
    flex:1,
    overflow:'auto'
  },
  cardMarginBottom: {
    marginBottom: theme.spacing(2),
  },
  tabRoot:{
    fontSize:'0.80rem',
    textAlign:'left'
  },
  tabWrapper:{
    alignItems:'flex-start'
  }
});

function TabPanel(props) {
  const { children, value, index,classes, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
      className={classes.tabpanel}
    >
      {value === index && (
        <Box px={2}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


class userDashboardContainer extends React.Component {

    constructor(){
        super();
        this.state = {
            form:{},
            loading:true,
            data:[],
            selectedIndex:0,
            tabIndex:0
        }
    }

    
    async componentDidMount(){
        // const {objectMap} = await client.post('pu/getActiveNotice');
        // let data = !isEmpty(objectMap) && !isEmpty(objectMap.noticeList) ? objectMap.noticeList:[];
        // this.setState({data,loading:false})
    }

    handleListItemClick = (event, index) => {
      this.setState({selectedIndex:index});
    };

    handleChange = (event, tabIndex) => {
      this.setState({tabIndex});
    };

    render(){
        const { classes } = this.props;
        const {tabIndex} = this.state;

        
        return (
            <div className={classes.root}>
               
            <Grid
              container
              spacing={4}
            >
             <Tabs
        orientation="vertical"
        variant="scrollable"
        value={tabIndex}
        onChange={this.handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label="Home" {...a11yProps(0)} className={classes.tabRoot} classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Personal Info" {...a11yProps(1)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Your Business" {...a11yProps(2)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Your Group" {...a11yProps(3)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Identity Card" {...a11yProps(4)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Change Password" {...a11yProps(5)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Product Training Programs" {...a11yProps(6)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Direct Seller Info" {...a11yProps(7)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Downline KYC Status" {...a11yProps(8)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Technical/Royality Treeview" {...a11yProps(9)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="PUC Approval" {...a11yProps(10)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
        <Tab label="Wonder World (PUC)/Keysoul Outlet Application" {...a11yProps(10)} className={classes.tabRoot}  classes={{wrapper:classes.tabWrapper}} />
      </Tabs>
      <TabPanel classes={classes} value={tabIndex} index={0}>
        <AccountProductTraining />
      </TabPanel>
      {/* <TabPanel classes={classes} value={tabIndex} index={1}>
        <PersonalInfo/>
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={2}>
        <YourBusiness />
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={3}>
        Item Four
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={4}>
        <IdentityCard/>
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={5}>
        <ChangePassword />
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={6}>
        <AccountProductTraining />
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={7}>
        Item Seven
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={8}>
        Item Eight
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={9}>
        Item Nine
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={10}>
        <PUCApproval/>
      </TabPanel>
      <TabPanel classes={classes} value={tabIndex} index={11}>
        <PUCApplication />
      </TabPanel> */}
            </Grid>
            </div>
        )
    }
}

userDashboardContainer.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(userDashboardContainer);