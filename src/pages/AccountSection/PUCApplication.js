import React from 'react';
import { withStyles } from '@material-ui/styles';
import DatePickerBox from '../../components/Form/DatePickerBox'
import DialogBox from '../../components/Common/DialogBox'
import client from '../../utils/client';
import {isEmpty,unset,set,isObject} from 'lodash'
import SelectBox from '../../components/Form/SelectBox';
import PropTypes from 'prop-types';
import {
    Card,
    CardHeader,
    CardContent,
    Box,
    Divider,
    Grid,
    Tab,Tabs,Typography,
    Paper,
    List,ListItem,ListItemIcon,ListItemText,
    CircularProgress,Button,Checkbox,FormControlLabel
  } from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import ScrollableTabs from '../../components/Common/ScrollableTab';
import validate from 'validate.js';
import TextFieldBox from './../../components/Form/TextFieldBox';
import GoogleMap from '../../components/Common/GoogleMap';
import FileUploadBox from './../../components/Form/FileUploadBox';

const schema = {
    gender:{
      presence:{ allowEmpty:false, message:'is required' }
    }
}
  
const Genders = [
    {title:'Male',value:'M'},
    {title:'Female',value:'F'},
    {title:'Transgender',value:'T'}
];

const tabs = [ 'Personal Information','Puc Information','Location','Upload Document','Authorize']

const styles = theme =>({
    root: {
        flex:1
    },
    TypographyPaddingY:{
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    }
});

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
        style={{margin:'20px 10px 10px 10px',overflow:'hidden'}}
      >
        {children}
      </div>
    );
  }
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

class PUCApplication extends React.Component {
    constructor(){
        super();
        this.state = {
            tab:0,
            form:{},
            loading:false,
            errors:{},
            termsDialog:false
        }
    } 

    onSubmit = async () => {
        const {form} = this.state;
        let errors = validate(form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({errors})
    }

    handleTabChange = (tab) => this.setState({tab})

    onTermsDialogClose = () => this.setState({termsDialog:false})

    onTermsDialogOpen = () => this.setState({termsDialog:true})

    onInputChange = (name,event) => {
        const {form,errors} = this.state;
        unset(errors,name);
        this.setState({
          form:{
            ...form,
            [name]: !isEmpty(event) && !isEmpty(event.target) ? event.target.value:event
          },
          errors
        })
    }

    hasError = field =>  this.state.errors[field];


    render(){
        const {tab,form,loading} = this.state;
        const { classes } = this.props;
        return (
            <div className={classes.root}>

   
                <ScrollableTabs tabs={tabs} onTabChanged={this.handleTabChange}/>
   
        

                    <TabPanel value={tab} index={0}>
                        
                        <CardHeader title="Personal Details" />
                        <Grid container spacing={2} >
                        
                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Applicant First Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Applicant Middle Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Applicant Last Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Father/Husband First Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Father/Husband Last Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <DatePickerBox name="middleName" label="Date Of Birth" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Room No/Building Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Area/Locality" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="State" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="District" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="City" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Pincode" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Occupation" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Computer Knowkedge" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Qualification" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Name Title" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Gender" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Marital Status" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>


                        </Grid>

                        <CardHeader title="DS Details" />
                        <Grid container spacing={2} >
                    
                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Family DS No" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Family DS Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Family DS Phone No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>
                        
                        </Grid>
                    </TabPanel>

                    <TabPanel value={tab} index={1}>
              
                    <CardHeader title="PUC Details" />
                        <Grid container spacing={2} >
                        
                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="PUC" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="PAN No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="PUC Room Number/Building" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="PUC Area/Locality" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Village/City" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>


                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="State" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="District" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Pincode" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Phone No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Mobile No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Email Id" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        </Grid>

                        <CardHeader title="Other Details" />
                        
                        <Grid container spacing={2} >
                        
                        <Grid item  md={4} xs={4} >
                    
                        <SelectBox items={Genders} name="middleName" label="Premises Type" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Total Shop Area (sq.ft)" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Constructed Shop Area (sq.ft)" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Area Population" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Flex Size (width × height)" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>
                        
                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Nearest Depot" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <SelectBox items={Genders} name="middleName" label="Min. Sale Target (per month)" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="First Order Amount" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>
                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Product use from" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>
                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Operator Details" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        </Grid>

                        <CardHeader title="Bank Details" />
                        
                        <Grid container spacing={2} >

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="IFSC Code" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Bank Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="State" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Branch Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Account No" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>



                        </Grid>
                  
                    </TabPanel>

                    <TabPanel value={tab} index={2}>

                    <Grid container spacing={2} >

                    <Grid item  md={12} xs={12} >

                    <Grid container spacing={2}>

                    <Grid item  md={6} xs={6} >
                        <Typography variant="caption" >Click on any point or drag the marker to get coordinates.</Typography>
                    </Grid>

                    <Grid item  md={3} xs={6} >
                    
                    <TextFieldBox name="latitude" label="Latitude" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                    </Grid>

                    <Grid item  md={3} xs={6} >
                    
                    <TextFieldBox name="longitude" label="Longitude" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                    </Grid>
                    </Grid>

                    </Grid>

                    <Grid item  md={12} xs={12} >
                    <GoogleMap />
                    </Grid>

                    </Grid>
                    
                    </TabPanel>

                    <TabPanel value={tab} index={3}>

                    <Grid container spacing={2} >
                    
                    <Grid item  md={4} xs={6} >
                        <FileUploadBox label="Profile Photo" />
                    </Grid>

                    <Grid item  md={4} xs={6} >
                        <FileUploadBox label="Signature Image" />
                    </Grid>

                    <Grid item  md={4} xs={6} >
                        <FileUploadBox label="ID Proof" />
                    </Grid>

                    <Grid item  md={4} xs={6} >
                        <FileUploadBox label="Address Proof" />
                    </Grid>

                    <Grid item  md={4} xs={6} >
                        <FileUploadBox label="Bank Proof" />
                    </Grid>
                    
                    </Grid>
                  
                    </TabPanel>

                    <TabPanel value={tab} index={4}>

                        <CardHeader title="Authorize By" />

                        <Grid container spacing={2} >
                        
                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Royalty Achiever No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Royalty Achiever Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Royalty Achiever Phone No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={12} xs={12} >
                            <Button color="primary" variant="outlined">GO</Button>
                        </Grid>


                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Technical Achiever No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                    
                        <TextFieldBox name="middleName" label="Technical Achiever Name" value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={4} xs={4} >
                        
                        <TextFieldBox name="middleName" label="Technical Achiever Phone No." value={form.middleName} onChange={this.onInputChange} error={this.hasError('middleName')}/>

                        </Grid>

                        <Grid item  md={12} xs={12} >
                            <Button color="primary" variant="outlined">GO</Button>
                        </Grid>

                        <Grid item  md={12} xs={12} >
                        <FormControlLabel
                            control={
                            <Checkbox
                                checked={true}
                                onChange={() => null}
                                name="terms"
                                color="primary"
                            />
                            }
                            label="I have Read and Agreed to the Following Terms and Conditions"
                        />
                        </Grid> 

                        <DialogBox open={this.state.termsDialog} title="TERMS AND CONDITIONS" 
                            onClose={this.onTermsDialogClose} onCancel={this.onTermsDialogClose} onOK={this.onTermsDialogClose}
                            >

                          <div>
                            <div class="text-justify">
                              {/* <div class="text-center" style="font-weight:700;font-size:12px;text-decoration:underline">DECLARATION (BY APPLICANT)</div> */}
                              <div class="text-center">
                                DECLARATION (BY APPLICANT)
                              </div>
                              <ul class="ul-disc">
                                <li>
                                  Hereby declare that the statements made by me
                                  in this “PUC Application cum Allotment Form”
                                  are true to the best of my knowledge and
                                  belief and complete in all respects.
                                </li>
                                <li>
                                  I understand that any information furnished in
                                  the application, if found incorrect or false
                                  will render me liable for any penal action or
                                  other consequences as may be prescribed in law
                                  or otherwise warranted.
                                </li>
                                <li>
                                  I agree that this “PUC Application cum
                                  Allotment Form” and declarations shall be the
                                  basis of the contract between me and Fashion
                                  Suitings Private Limited.
                                </li>
                                <li>
                                  I also declare that any changes in the
                                  information given above after the submission
                                  of this would be conveyed to Fashion Suitings
                                  Private Limited, immediately.
                                </li>
                                <li>
                                  I have read and clearly understood the terms
                                  &amp; conditions of this “PUC Application cum
                                  Allotment Form”, printed overleaf and I
                                  undertake to be abide by and follow them
                                  strictly.
                                </li>
                              </ul>
                            </div>
                            <div class="text-justify">
                              {/* <div class="text-center" style="font-weight:700;font-size:12px;text-decoration:underline;padding:15px 0 5px 0">TERMS &amp; CONDITIONS</div> */}
                              <div class="text-center">
                                TERMS &amp; CONDITIONS
                              </div>
                              <p class="termcondition">
                                The standard terms &amp; conditions forming part
                                of “PUC Application cum Allotment Form for
                                Regular and Online Business” are stipulated as
                                under.
                              </p>
                              <p class="termcondition">
                                WHEREAS in such terms &amp; conditions, the term
                                “Company” denotes “Fashion Suitings Private
                                Limited”, a company incorporated under the
                                Companies Act, 1956 with its CIN
                                U18108RJ1988PTC004383 and Registered Office at
                                RCM World, SPL – 6, RIICO Growth Centre, Post –
                                Swaroopganj , Via – Hamirgarh, Bhilwara -
                                311025, Rajasthan, India.
                              </p>
                              <p class="termcondition">
                                The term “PUC” denotes “Pick up Center” and the
                                term “PUC Holder” denotes successful applicant
                                of PUC.
                              </p>
                              <p class="termcondition">
                                WHEREAS the Company is engaged in marketing and
                                distribution of various types of goods and
                                products through “Direct Selling” system under
                                the brand name of “RCM”. The company has
                                undertaken a plan to expand its business
                                throughout the country. The Company has
                                considered it necessary to allot “Pick up
                                Centers” throughout the country to strengthen
                                its sale and distribution network. These Pick up
                                Centers will be operated and managed by the PUC
                                Holders at their cost and expenses. The Company
                                will supply its products for sale &amp;
                                distribution to such Pick up Centers. The
                                Company also owns a domain (website)
                                www.rcmbusiness.com for facilitating online
                                sales and purchase of products offered by the of
                                the company. The direct sellers, registered
                                Buyer and guest Buyer can avail the online
                                ordering facility through this portal and PUC
                                registered under this shall operate as
                                distribution point for such purchases.
                              </p>
                              <p class="termcondition">
                                The Terms &amp; conditions for operation and
                                management of aforesaid PUC by the PUC Holder
                                are stipulated as under: -
                              </p>
                              <ul class="ul-disc">
                                <li>
                                  The PUC Holder shall work consciously in a
                                  professional manner for the sale and
                                  distribution of Company’s products.
                                  <ul class="ul-circle">
                                    <li>
                                      PUC Holder shall totally concentrate over
                                      sale and distribution of Company’s
                                      products. He shall not involve in any
                                      other business/employment of similar
                                      nature either directly or indirectly.
                                    </li>
                                    <li>
                                      PUC Holder shall neither permit nor
                                      involve directly or indirectly in
                                      operation/management of any other
                                      business/activity of similar nature at the
                                      place/territory allotted to PUC.
                                    </li>
                                    <li>
                                      PUC Holder shall not change the name,
                                      address and other arrangements of allotted
                                      PUC as stated in its agreement without
                                      written permission of the Company.
                                    </li>
                                    <li>
                                      PUC Holder shall be responsible to arrange
                                      covered, specified and segregated
                                      shop/showroom at the place of PUC. In case
                                      of rented premises, he shall also be
                                      responsible to pay rent and other charges
                                      including the local taxes etc out of his
                                      pocket. He shall also be responsible to
                                      arrange necessary furniture &amp;
                                      fixtures, electric installation, computer
                                      &amp; software, printers, telephone and
                                      internet connections, manpower,
                                      electricity &amp;water Connections,
                                      interior and exterior decoration, displays
                                      etc. as per the guidelines of the Company
                                      for the PUC at his own costs.
                                    </li>
                                    <li>
                                      PUC Holder shall undertake to keep open
                                      the PUC, from 10.00 a.m. to 8.00 p.m. on
                                      all working days and shall provide
                                      adequate time and response to all Direct
                                      Sellers of the Company.
                                    </li>
                                    <li>
                                      PUC Holder shall provide computerized
                                      invoices to all purchasers through its
                                      official login ID allotted by the Company.
                                    </li>
                                    <li>
                                      PUC Holder shall be responsible to make
                                      all sales strictly in accordance with the
                                      Company’s guidelines. He shall also be
                                      responsible to provide complete and
                                      detailed information of the Company’s
                                      sales promotional schemes and its benefits
                                      to all purchasers adequately but he shall
                                      not provide special discounts to anybody,
                                      without written permission of the Company.
                                    </li>
                                    <li>
                                      PUC Holder shall appoint qualified and
                                      competent staff and who are employed
                                      solely by PUC Holder and not by the
                                      Company. PUC Holder is solely responsible
                                      for hiring, training and discharging
                                      employees and setting their wages and
                                      terms of employment. PUC Holder shall
                                      comply with all applicable laws and
                                      regulations, including, but not limited
                                      to, workers' compensation laws. PUC Holder
                                      shall require employees to wear such
                                      uniforms or attire as the Company
                                      prescribes periodically, and otherwise
                                      comply with the ongoing system standards.
                                    </li>
                                    <li>
                                      PUC Holder and/or his employees shall
                                      attend the company’s training intended for
                                      obtaining necessary information about
                                      products, if he receives a written
                                      invitation.
                                    </li>
                                    <li>
                                      PUC Holder shall store the product at
                                      temperatures to maintain product
                                      shelf-life. The package/pallet integrity
                                      should be maintained throughout the
                                      storage period to maintain the condition
                                      of the finished product.
                                    </li>
                                    <li>
                                      PUC holder shall be liable to act in way
                                      that none of its activity is harmful for
                                      the company’s interest.
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                  PUC Holder shall not assign, mortgage,
                                  hypothecate, sublet or otherwise part with
                                  possession or create any right in third party
                                  rights in the allotted PUC without written
                                  consent of the Company.
                                </li>
                                <li>
                                  PUC Holder shall observe and comply with all
                                  laws, rules &amp; regulations etc. for the
                                  time being in force. In case of any default,
                                  he shall be personally liable.
                                </li>
                                <li>
                                  PUC Holder shall maintain and keep in
                                  possession adequate stock of Company’s
                                  products for sale and distribution. In case of
                                  paucity of adequate stock, Company reserves
                                  the right to terminate his PUC with immediate
                                  effect without any notice.
                                </li>
                                <li>
                                  The Company also reserves the right to allot
                                  the PUC to anybody else at the same place as
                                  per its free will. The Company has absolute
                                  discretion to appoint additional PUC Holder/s
                                  at the same place without any notice to
                                  existing PUC Holder.
                                </li>
                                <li>
                                  In all cases Company will acknowledge only the
                                  payments, received either by direct
                                  deposit/RTGS/NEFT or online fund transfer to
                                  the Company’s Bank A/c.
                                </li>
                                <li>
                                  If the PUC Holder receives any product whose
                                  “Best Before Date/Expiry Date” as printed over
                                  it is very short and the sale and/ or
                                  distribution of such products is not
                                  practicable before the “Expiry Date”, he shall
                                  intimate the Company immediately in writing on
                                  receipt of such products and shall take all
                                  necessary steps as per directions of the
                                  Company. In case of default, the Company shall
                                  not be responsible for any loss or damage
                                  incurred. It will be the sole responsibility
                                  of the PUC Holder to deliver the products well
                                  before their expiry date so that Buyer can
                                  properly use the product. The PUC Holder shall
                                  not sale the products after the expiry
                                  date/period mentioned thereon. The company
                                  shall not be responsible for any damages for
                                  such sale and the PUC Holder shall alone be
                                  responsible for the consequences.
                                </li>
                                <li>
                                  PUC Holder shall prepare and maintain all
                                  requisite books of accounts e.g. journals,
                                  ledgers, registers, invoices, returns,
                                  challans etc. and statutory records. The
                                  Company reserves the right to carry audit of
                                  such records at any time without notice to PUC
                                  Holder. The PUC Holder shall be responsible to
                                  cooperate with them and provide them all
                                  facilities, documents and information required
                                  to carry such audit.
                                </li>
                                <li>
                                  PUC Holder shall prepare and forward to the
                                  company various reports and information that
                                  the company deems necessary.
                                </li>
                                <li>
                                  PUC Holder shall follow FIFO method for
                                  product rotation/inventory control and
                                  accounting.
                                </li>
                                <li>
                                  PUC Holder must not use the website or get
                                  involved in any activity that can causes or
                                  likely to cause the interruption, damage or
                                  impair to the website in any way.
                                </li>
                                <li>
                                  PUC holder shall be solely held responsible
                                  and liable for any misbehave, misconduct, or
                                  illegal activities committed by PUC holder or
                                  their representative, or by delivery person
                                  during the course of delivery of products.
                                </li>
                                <li>
                                  PUC Holder shall have no right to claim
                                  damages for delayed/defective supplies against
                                  the Company in any case.
                                </li>
                                <li>
                                  PUC Holder will have to make an interest free
                                  (refundable) Security Deposit of minimum Rs.
                                  10,000/- (Rs. Ten Thousand only) to the
                                  Company.
                                </li>
                                <li>
                                  PUC Holder shall also perform the following
                                  tasks related to online business of the
                                  company generated through www.rcmbusiness.com
                                  <ul class="ul-circle">
                                    <li>
                                      PUC Holder shall be solely responsible for
                                      the collection of amounts due from the
                                      Buyers who have made online order on
                                      www.rcmbusiness.com.
                                    </li>
                                    <li>
                                      PUC Holder will ensure timely delivery of
                                      the product to the user. He will make a
                                      maximum of two attempts to deliver order.
                                      In case the Buyer is not reachable or does
                                      not accept delivery of products in these
                                      attempts, then the PUC Holder reserves the
                                      right to cancel the order(s) at his
                                      discretion and intimate the company
                                      immediately.
                                    </li>
                                    <li>
                                      PUC Holder shall facilitate the return or
                                      exchange of the product booked online by
                                      buyers as per the return or exchange
                                      clause mentioned in “Terms of Use” for
                                      www.rcmbusiness.com
                                    </li>
                                    <li>
                                      PUC Holder shall submit his claim for the
                                      expenses incurred in relation to swapping
                                      machine within 30 days of receiving
                                      swapping machine monthly statement. No
                                      claim will be entertained after the said
                                      period.
                                    </li>
                                    <li>
                                      PUC Holder will deliver the product as per
                                      the ordered quality, quantity and
                                      packaging.PUC Holder remains responsible
                                      for overall quality, timely delivery and
                                      cost of delivery.
                                    </li>
                                    <li>
                                      PUC Holder shall issue the tax/retail
                                      invoice to the user with the total price
                                      for the Goods inclusive of all taxes.
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                  In case of any dispute with a buyer regarding
                                  quality of products etc., the PUC Holder shall
                                  intimate the facts to the Company immediately
                                  and the Company will suggest the appropriate
                                  action in the facts and circumstances of the
                                  case.
                                </li>
                                <li>
                                  Company shall not be liable to PUC Holder or
                                  any buyer for any lost profits or savings,
                                  consequential, incidental, special or punitive
                                  damages arising from use, misuse, or inability
                                  to use products.
                                </li>
                                <li>
                                  The Company shall terminate/revoke allotted
                                  PUC with immediate effect and without any
                                  notice to PUC Holder in following and other
                                  circumstances:-
                                  <ul class="ul-circle">
                                    <li>
                                      if PUC Holder is declared insolvent or
                                      there is likelihood of his becoming
                                      bankrupt, or
                                    </li>
                                    <li>He incurs heavy debts, or</li>
                                    <li>
                                      He breaches any of the terms &amp;
                                      conditions stipulated herein and fails to
                                      rectify the same or fails to fulfil his
                                      duties and obligations despite of
                                      directions of the Company within 30 days,
                                      or
                                    </li>
                                    <li>
                                      He is directly or indirectly involved in
                                      any activity dormant to Direct Sellers ID,
                                      or
                                    </li>
                                    <li>
                                      Any other condition as the company may
                                      deem fit.
                                    </li>
                                  </ul>
                                  Such termination/revocation shall be without
                                  prejudice to any right of remedy of each
                                  party. However, the Company reserves the right
                                  to confiscate all payables of the PUC Holder,
                                  may it be cash or kind. Upon
                                  termination/revocation of PUC for any reason:
                                  -
                                  <ul class="ul-circle">
                                    <li>
                                      The relationship between the parties shall
                                      immediately come to an end.
                                    </li>
                                    <li>
                                      Any obligation which shall expressly or by
                                      implication is intended to remain in force
                                      even after the termination/revocation
                                      shall be given effect in letters and
                                      spirit.
                                    </li>
                                  </ul>
                                </li>
                                <li>
                                  In case of expiry or earlier termination of
                                  PUC Holder’s agreement with the Company, PUC
                                  Holder shall also be responsible to fulfill
                                  all pending orders or enquiries, until the
                                  final settlement of accounts takes place and
                                  “NO DUES CERTIFICATE” is issued by the
                                  Company. Further, PUC Holder shall be
                                  responsible to provide books of accounts and
                                  other documents related to the Company.
                                </li>
                                <li>
                                  Any delay or failure in the performance by
                                  either Party hereunder shall be excused if and
                                  to the extent caused by the occurrence of a
                                  Force Majeure. Force Majeure shall mean a
                                  cause or event that is not reasonably
                                  foreseeable or otherwise caused by or under
                                  the control of the Party claiming Force
                                  Majeure, including acts of God, fires, floods,
                                  explosions, riots, wars, hurricane, sabotage
                                  terrorism, vandalism, accident, restraint of
                                  government, governmental acts, injunctions,
                                  labour strikes, adverse climatic conditions,
                                  power outage, failure of performance by a
                                  third party (not due to any act or omission by
                                  either Party) or any other cause beyond the
                                  reasonable anticipation and control of either
                                  Party to this Agreement despite such Party’s
                                  reasonable efforts to prevent, avoid, delay,
                                  or mitigate the effect of such acts, events or
                                  occurrences, and which events or the effects
                                  thereof are not attributable to a Party's
                                  failure to perform its obligations.
                                </li>
                                <li>
                                  PUC Holder will, at his costs, indemnify,
                                  defend and save the Company and its
                                  affiliates, successors and assigns, and the
                                  officers, directors, members, managers,
                                  shareholders, administrators etc. from and
                                  against any and all claims, demands, actions,
                                  suits, judgments and liabilities of any kind
                                  and character whatsoever (collectively,
                                  "Claims") for damages arising from personal
                                  injury or property damage caused by the PUC
                                  Holder or anyone for whose acts the PUC holder
                                  may be held responsible, then the PUC Holder
                                  shall indemnify the company and fully
                                  reimburse any loss, damage or expenses,
                                  including the attorney's fees provided the
                                  claims arise out of or in connection with
                                  breach of any representation or warranty by
                                  the PUC Holder or arise due to performing such
                                  acts or deed due to which any claim is brought
                                  against the company by any third party. PUC
                                  Holder agrees to fully cooperate with Company
                                  in the defence of such Claims.
                                </li>
                                <li>
                                  Nothing contained herein shall be deemed to
                                  make PUC Holder as a business partner or joint
                                  venture or agent or partner of Company for any
                                  purpose.
                                </li>
                                <li>
                                  All relevant information and documents related
                                  to the PUC Holder will be published by the
                                  Company on its official website i.e.
                                  www.rcmbusiness.com. PUC Holder shall update
                                  himself by visiting the Company’s website
                                  regularly and act accordingly.
                                </li>
                                <li>
                                  The failure to exercise any right by the
                                  Company provided herein shall not be deemed
                                  waiver of such rights.
                                </li>
                                <li>
                                  If any clause of the terms and conditions
                                  shall be deemed invalid, void or for any
                                  reason unenforceable, such clause shall be
                                  deemed severable and shall not affect the
                                  validity and enforceability of the remaining
                                  terms and conditions of “PUC Application cum
                                  Allotment Form for Regular/Online Business”.
                                </li>
                                <li>
                                  The terms &amp; conditions stipulated herein
                                  are subject to revision by Company from time
                                  to time. The Company reserves the right to
                                  append new term/s &amp; conditions or to
                                  modify/delete/amend the terms &amp;
                                  conditions, stipulated herein and no
                                  disagreement will be entertained by the
                                  Company.
                                </li>
                                <li>
                                  That the company reserves all the rights to
                                  add, delete, amend, and alter any of the terms
                                  and conditions of this application form
                                  without any prior notice, on retrospective
                                  basis also. All such future changes shall be
                                  binding to all existing PUC HOLDERS. Such
                                  changes shall be updated online by the
                                  company. PUC HOLDERS shall be liable to keep
                                  themselves update with the changes made by the
                                  company.
                                </li>
                                <li>
                                  In case any dispute arises among the parties
                                  hereto out of or in relation to or in
                                  connection with this agreement, of the breach,
                                  termination, effect, validity, interpretation
                                  or application of this agreement or as to
                                  their rights, duties or liabilities
                                  there-under, or as to any act, matter or thing
                                  arising out of, consequent to or in connection
                                  with this agreement, the same shall be
                                  referred to a Sole Arbitrator identified and
                                  nominated by the Company. The decision of the
                                  arbitrator shall be final and binding upon the
                                  parties. The venue of the arbitration
                                  proceedings shall be Bhilwara (Raj.) only. The
                                  arbitration proceedings shall be in English
                                  language and shall be governed by Arbitration
                                  and Conciliation Act, 1996.
                                </li>
                                <li>
                                  The terms &amp; conditions stipulated herein
                                  shall be governed by and construed in
                                  accordance with the laws of India and under
                                  exclusive jurisdiction of the Courts in
                                  Bhilwara.
                                </li>
                              </ul>
                              <p class="termcondition tc-agree">
                                I verify that I have read and understood the
                                terms &amp; conditions stipulated as above and
                                hereby undertake to be with them strictly.
                              </p>
                            </div>
                          </div>

                      </DialogBox>
                        </Grid>
                  
                    </TabPanel>

            </div>
        )
    }
}

PUCApplication.propTypes = {
    className: PropTypes.string
  };
  
  export default withStyles(styles)(PUCApplication);