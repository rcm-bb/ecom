import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Divider,
  Grid,
  Button
} from '@material-ui/core';
import DataTable from '../../components/Common/DataTable';
import DialogBox from './../../components/Common/DialogBox';
import client from './../../utils/client';
import SelectBox from '../../components/Form/SelectBox';
import TextFieldBox from './../../components/Form/TextFieldBox';
import {isEmpty,unset,set} from 'lodash'; 
import DatePickerBox from '../../components/Form/DatePickerBox';
import validate from 'validate.js';
import CommonSnackbar from '../../components/Snackbar';

const styles = theme => ({
    root: {
      padding: theme.spacing(4)
    },
    cardContainer:{
        minHeight:236
    },
    formControl: {
        width: '100%',
    },
    buttonDiv:{
        display:'flex',
        justifyContent:'flex-end',
        alignItems:'center'
    }
});

const schema = {
    stateId:{
        presence:{allowEmpty:false,message:'^please select state.'}
    },
    districtId:{
        presence:{allowEmpty:false,message:'^please select district.'}
    },
    date:{
        presence:{allowEmpty:false,message:'^seminar date is required.'}
    },
    time:{
        presence:{allowEmpty:false,message:'^seminar time is required.'}
    }
}

const headCells = [
    {label:'Date',value:'created',type:'date'},
    {label:'Time',value:'time'},
    {label:'Venue',value:'programVenue'},
    {label:'City',value:'programCity'},
    {label:'District',value:'programDistrict.name'},
    {label:'State',value:'programState.name'},
    {label:'Visiting Leader',value:'visitingLeader'},
    {label:'Mobile1',value:'contactNo1'},
    {label:'Mobile2',value:'contactNo2'},
    {label:'Action',value:''}
];

const Genders = [
    {title:'Male',value:'M'},
    {title:'Female',value:'F'},
    {title:'Transgender',value:'T'}
];


class AccountProductTraining extends React.Component {

    constructor(){
        super();
        this.state = {
            data:[],
            loading:false,
            saving:false,
            formDialog:false,
            form:{
                date: new Date()
            },
            errors:{},
            states:[],
            disctricts:[]
        }
        this._snackbar = null;
        this._refTable = null;
    }

    componentDidMount(){
        this.fetchState();
    }

    openFormDialog = () => this.setState({formDialog:true});

    closeFormDialog = () => this.setState({formDialog:false});

    onSave = async () => {
        let {form} = this.state;
        let errors = validate(form,schema);
        errors = isEmpty(errors) ? {}:errors;
        if(!isEmpty(errors)) return this.setState({errors});
        this.setState({errors,saving:true});
        const {contactNo1,contactNo2,date,time,programVenue,visitingLeader,stateId,districtId} = form;
        let res = await client.post('pu/saveProductTrainingProgram',{
            contactNo1,contactNo2,date,time,programVenue,visitingLeader,programDistrict:{districtId:stateId},programState:{stateId:districtId}
        });
        this._snackbar.openSnack('Seminar added successfully.');
        this.setState({form:{date:new Date()},saving:false,formDialog:false});
        this._refTable.fetch();
    }

    async fetchState(){
        let {objectMap} = await client.post('pu/getState');
        let states = !isEmpty(objectMap) & !isEmpty(objectMap.stateListDto) ? objectMap.stateListDto:[];
        this.setState({states});
    }

    async fetchDistrict(){
        const {form} = this.state;
        let {objectMap} = await client.post(`pu/getDistrictsByStateId/${form.stateId}`);
        let disctricts = !isEmpty(objectMap) & !isEmpty(objectMap.districtDto) ? objectMap.districtDto:[];
        this.setState({disctricts});
    }

    onInputChange = (name,event) => {
        const {form,errors,disctricts} = this.state;
        unset(errors,name);
        this.setState({
          form:{ ...form, [name]: !isEmpty(event) && !isEmpty(event.target) ? event.target.value:event },
          errors, disctricts: name == 'stateId' ? []:disctricts
        },() => {
            if(name == 'stateId') this.fetchDistrict()
        })
    }

    hasError = field =>  this.state.errors[field];

    render(){
        const { classes } = this.props;
        const { data,formDialog,form,errors,states,disctricts } = this.state;
        
        return (
            <div className={classes.root}>
                    <CommonSnackbar ref={ ref => this._snackbar = ref }/>
                    <DialogBox open={formDialog} title="New Seminar" okButtonText="Submit" onCancel={this.closeFormDialog} onClose={this.closeFormDialog} onOK={this.onSave}>
                      
                        <Grid container spacing={2} >
                            
                            <Grid item  md={6} xs={6} >
                        
                            <DatePickerBox required name="date" label="Seminar Date" value={form.date} onChange={this.onInputChange} error={this.hasError('date')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                        
                            <TextFieldBox required name="time" label="Seminar Time" value={form.time} onChange={this.onInputChange} error={this.hasError('time')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                        
                            <SelectBox required items={states} name="stateId" label="State" value={form.stateId} itemLabel="name" itemValue="stateId" onChange={this.onInputChange} error={this.hasError('stateId')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                        
                            <SelectBox required items={disctricts} name="districtId" label="District" value={form.districtId} itemLabel="name" itemValue="districtId" onChange={this.onInputChange} error={this.hasError('districtId')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                        
                            <TextFieldBox name="programCity" label="City" value={form.programCity} onChange={this.onInputChange} error={this.hasError('programCity')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                            
                            <TextFieldBox name="programVenue" label="Venue" value={form.programVenue} onChange={this.onInputChange} error={this.hasError('programVenue')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                        
                            <TextFieldBox name="contactNo1" label="Mobile Number 1" value={form.contactNo1} onChange={this.onInputChange} error={this.hasError('contactNo1')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                            
                            <TextFieldBox name="contactNo2" label="Mobile Number 2" value={form.contactNo2} onChange={this.onInputChange} error={this.hasError('contactNo2')}/>

                            </Grid>

                            <Grid item  md={6} xs={6} >
                            
                            <TextFieldBox name="visitingLeader" label="Visiting Leader" value={form.visitingLeader} onChange={this.onInputChange} error={this.hasError('visitingLeader')}/>

                            </Grid>

                            </Grid>
                   
                    </DialogBox>

                    <DataTable ref={ref => this._refTable = ref}  title={'Product Training Program'} fetchURL={'pu/getMyProgram'} resField="programList" headCells={headCells} rightHeaderChildren={(
                               <Grid container justify="flex-end"> 
                                    <Grid  item xs={12} md={6} className={classes.buttonDiv}>
                                       <Button variant="contained" color="primary" size="medium" onClick={this.openFormDialog}>Add Seminar</Button>
                                    </Grid>
                               </Grid>
                    )} />
 
            </div>
        )
    }
}

AccountProductTraining.propTypes = {
  className: PropTypes.string
};

export default withStyles(styles)(AccountProductTraining);