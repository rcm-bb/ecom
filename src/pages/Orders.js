import React from 'react';
import { Grid,Container,Typography,CircularProgress } from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles'
import OrderListCard from './../components/Order/OrderListCard';
import Row from '../components/Row'
import client from './../utils/client'
import {isEmpty} from 'lodash'
const styles = () => ({
    root:{
        display:'flex',
        flexDirection: 'column',
        // marginTop:50
    },
    loaderRoot:{
        minHeight:500,
        display:'flex',
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})

class Orders extends React.Component{
    constructor(){
        super();
        this.state = {
            orders:[],
            loading:true
        }
    }

    componentDidMount(){
        this.fetchOrders();
    }

    async fetchOrders(){
        let {objectMap} = await client.post('/pr/getCustomerOrderhistoryPagination',{
                pageNumber: 1,
                pageSize: 10,
            });
            console.log('orders',objectMap)
        if(!isEmpty(objectMap.orderList)){
            this.setState({orders:objectMap.orderList,loading:false});
        }    
    }

    navigateTo = (order) => {
        // let od = {
        //     orderId:order
        // }
        this.props.history.push(`orderDetail/${order.orderId}`,{order})
    }

    render(){
        const {orders,loading} = this.state;
        const {classes} = this.props;
        if(loading) return <div className={classes.loaderRoot} ><CircularProgress/></div>
        return (
            <Container className={classes.root} >

              <Row>
                <Typography variant="h6">
                  Orders
                </Typography>
              </Row>

                <Grid container spacing={4} >
                <Grid item xs={12} sm={8}>
                    {
                        orders.map((order,index) => {
                            return (
                                <OrderListCard order={order} key={index} onClick={this.navigateTo} />
                            )
                        })
                    }
                </Grid>
                
                </Grid>
            </Container>
        )
    }
}

export default withStyles(styles)(Orders);