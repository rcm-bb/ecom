import React from 'react';
import {withStyles} from '@material-ui/styles'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import DatePickerBox from './../components/Form/DatePickerBox';
import validate from 'validate.js';
import CommonSnackbar from './../components/Snackbar';
import client from './../utils/client';
import TextFieldBox from './../components/Form/TextFieldBox';
import DialogBox from './../components/Common/DialogBox';
import {isEmpty,unset,set} from 'lodash';

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class ForgotPassword extends React.Component {
  constructor(){
    super();
    this.state = {
      dialog:false,
      form:{},
      errors:{},
      otpDone:false,
      loading:false
    }
  }

  onSubmit = async () => {
    const {otpDone,form} = this.state;
    if(!otpDone){
      let errors = validate(form,{
        otp:{
          presence:{allowEmpty:false,message:'^otp is required'}
        }
      });
      errors = !isEmpty(errors) ? errors:[];
      if(!isEmpty(errors)) return this.setState({errors});
      this.setState({errors});
      let res = await client.post('pu/resetPassword',{form});
      console.log('onSubmit ress',res);
      this.setState({errors:{},otpDone:true})
    }else{
      let errors = validate(form,{
        password:{
          presence:{allowEmpty:false,message:'^password is required'}
        },
        confirm_password:{
          equality:'password'
        }
      });
      errors = !isEmpty(errors) ? errors:[];
      if(!isEmpty(errors)) return this.setState({errors});
      this.setState({errors});
      let res = await client.post('pu/resetPassword',{form});
      console.log('onSubmit ress',res);
      this.setState({errors:{},otpDone:true})
    }
  }

  onSave = async () =>{
    const {form} = this.state;
    console.log('form',form)
    let errors = validate(form,{
      usernameOrEmail:{
        presence:{allowEmpty:false,message:'^username is required'}
      }
    });
    errors = !isEmpty(errors) ? errors:[];
    if(!isEmpty(errors)) return this.setState({errors});
    this.setState({errors,loading:true})
    let res = await client.post('pu/forgotPassword',{form});
    console.log('ress',res);
    this.setState({dialog:true})
  }

  onInputChange = (name,event) => {
    const {form,errors} = this.state;
    unset(errors,name);
    this.setState({
      form:{ ...form, [name]: !isEmpty(event) && !isEmpty(event.target) ? event.target.value:event },
      errors
    })
  }

  hasError = field =>  this.state.errors[field];
  
  closeDialog = () => this.setState({dialog:false,otpDone:false});

  openDialog = () => this.setState({dialog:true});

  render(){
    const {classes} = this.props;
    const {form,errors,otpDone} = this.state;
    console.log('errors',errors)
    return (
      <Container component="main" maxWidth="xs">

      <DialogBox open={this.state.dialog} title="Forgot password" okButtonText={otpDone ? 'Submit':'Next'} onClose={this.closeDialog} onCancel={this.closeDialog} onOK={this.onSubmit}>
          <Grid container spacing={2} >
                        
            {!otpDone ? <Grid item  md={12} xs={12} >
        
            <TextFieldBox name="otp" label="OTP" value={form.otp} onChange={this.onInputChange} error={this.hasError('otp')}/>

            </Grid>:
              <>
              <Grid item  md={12} xs={12} >
              
              <TextFieldBox name="password" label="New Password" value={form.usernameOrEmail} onChange={this.onInputChange} error={this.hasError('usernameOrEmail')}/>

              </Grid>
              <Grid item  md={12} xs={12} >
        
              <TextFieldBox name="confirm_password" label="Repeat Password" value={form.usernameOrEmail} onChange={this.onInputChange} error={this.hasError('usernameOrEmail')}/>

              </Grid>
              </>
            }

          </Grid>

      </DialogBox>

      <div className={classes.paper}>
  
        <Typography component="h1" variant="h6">
          Please Enter Your Username
        </Typography>

        <TextFieldBox name="usernameOrEmail" label="Username" value={form.usernameOrEmail} autoFocus onChange={this.onInputChange} error={this.hasError('usernameOrEmail')}/>

          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={this.onSave}
          >
            Submit
          </Button>

     
      </div>

    </Container>
    )
  }
}
export default withStyles(styles)(ForgotPassword);