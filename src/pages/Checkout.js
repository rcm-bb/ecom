import React, { createRef } from 'react'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box';
import Row from '../components/Row'
import clsx from 'clsx'
import CartItem from '../components/Cart/CartItem'
import { withStyles } from '@material-ui/core/styles'
import { Grid, Hidden, Divider, Container, Button} from '@material-ui/core'
import Spacer from '../components/Spacer'
import {Link} from 'react-router-dom'
import { Hbox } from '../components/Box'
import client from './../utils/client'
import Skeleton from '@material-ui/lab/Skeleton';
import {isAuthenticated} from './../utils/auth';
import CommonSnackbar from '../components/Snackbar';
import {setCartCount} from './../store/actions';
import store from './../store'
import {isEmpty,sumBy} from 'lodash';
import cookie from 'react-cookies';
import AddressModal from '../components/Checkout/AddressModal';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import SpinnerButton from '../components/SpinnerButton';
import {Accordion,AccordionSummary,AccordionDetails} from './../components/Accordion'

const styles = theme => ({
  root: {
    paddingBottom: '64px',
  },
  checkoutPanel: {
    backgroundColor: theme.palette.grey['200'],
    borderRadius: theme.shape.borderRadius,
    padding: `${theme.spacing(2)}px`,
  },
  total: {
    fontWeight: 'bold',
  },
  checkoutButton: {
    width: '100%',
  },
  docked: {
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.typography.subtitle1.fontSize,
      padding: `${theme.spacing(2)}px`,
      position: 'fixed',
      left: 0,
      bottom: 0,
      width: '100%',
      zIndex: 10,
      borderRadius: '0',
      boxShadow: 'none',
    },
  },
  accordionRoot: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
  stepperRoot:{
    display:'flex',
    justifyContent:'flex-end'
  }
}) 

const NextStepper = ({rootClassName,title,onClick}) => ( 
  <div className={rootClassName}>
  <Button variant="contained" color="primary" onClick={onClick.bind(this)}>
    {title}
  </Button>
  </div>
)

NextStepper.defaultProps = {
  title:'Next',
  onClick: () => null,
}


const dummyItem =  {
  description: "Aliqua proident et duis ut amet eiusmod id ipsum. Excepteur est eiusmod reprehenderit est officia elit ipsum voluptate proident elit. Ipsum voluptate ut incididunt ut aliquip. Veniam cillum magna in dolore quis incididunt Lorem non elit. Irure nostrud anim cupidatat nisi cillum esse. In Lorem non elit nostrud quis aliqua ipsum irure. Nisi dolor dolore sunt qui tempor. Ipsum anim incididunt laboris proident quis excepteur eu deserunt irure sint est aliquip quis. Non exercitation in voluptate non eiusmod adipisicing amet excepteur magna pariatur cillum. Reprehenderit voluptate minim ipsum eiusmod labore cupidatat eu ex et.",
  id: 1,
  name: "Banarsi Saree",
  price: 10.99,
  priceText: "Rs. 10.99",
  quantity: 1,
  rating: 4.5,
  thumbnail:{
    alt: "Product 1",
    src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU"
  }
};


class Checkout extends React.Component{
    constructor(){
        super();
        this.state = {
            items:[],
            checkoutForm:{
              billingAddress:{},
              shippingAddress:{},
              paymentMode:"COD",
              deliveryCharge:0,
              totalAmmount:0,
              totalTax:0,
              totalTaxAmmount:0,
              taxableAmt:0,
              totBv:0,
              totalBv:0,
              grandTotal:0,
              partner: {bPartnerId: 9108939},
              sameAddress: false
            },
            loading:true,
            expanded:'panel2',
            openAddressModal:false,
            addressType:'billing',
            cartId:null,
            saving:false,
        }
        this._snackbar = null  
        this.isAuth = isAuthenticated();
        let cartId = cookie.load('clid')
        this.cartId = !isEmpty(cartId) ? cartId:0;
    }

    async componentDidMount(){
      this.fetchCart();
      let {objectMap} = await client.post('pu/getAddressList/9108939');
      let address = objectMap.addressDto[0];
      // console.log('address',address)
      let addressId = address.addressId;
      if(objectMap &&  objectMap.addressDto) this.setState({checkoutForm:{  ...this.state.checkoutForm, billing: address,shipping: address, billingAddress:{addressId},shippingAddress:{addressId} } })
    }

    handleTabChange = (panel) => {
      let expanded = panel ? panel : false;
      if(panel && this.state.expanded == panel) expanded = false;
      this.setState({expanded})
    };
    
    async fetchCart(){
      let url = this.isAuth ? 'pu/getCartLine':`pu/getCartLineForUnsignedUser/${this.cartId}`
      let res = await client.post(url);
      // console.log('res...',this.cartId ,res)
      const {objectMap} = res;
      let items = [];
      objectMap && objectMap.cartLineDto && objectMap.cartLineDto.map((item) => {
        let product = item.materialID;
        let price = product.mPrice ? product.mPrice:0;
        items.push({
          ...dummyItem,
          name:product.name,
          price:price,
          id:product.materialId,
          quantity:item.quantity,
          priceText:`Rs. ${price}`,
          CartLineId:item.cartLineId,
          totalAmmount: parseInt(item.quantity) * parseFloat(price)
        })
        return item;
      });
      this.cartId = res.message;
      let totalAmmount = sumBy(items,'totalAmmount');
      store.dispatch(setCartCount('equal',items.length))
      this.setState({items,loading:false,cartId:res.message,checkoutForm:{...this.state.checkoutForm,grandTotal:totalAmmount,totalAmmount}})
    }

    removeProduct = async (product) => {
      let url = this.isAuth ? `pu/removeMaterialFromCart/${product.CartLineId}`:`pu/removeMaterialFromCartOfUnsignedUser/${product.CartLineId}/${this.cartId}`
      try{
        await client.post(url);
        let {items} = this.state;
        items = items.filter((item) => item.id != product.id)
        this._snackbar.openSnack('Item has been removed');
        store.dispatch(setCartCount('decrease'))
        this.setState({items})
      }catch(err){
        console.log('err',err)
        this._snackbar.openSnack('Item not removed from your cart','error')
      }
    }

    updateCartQty = async (product,type) => {
     let qty = type == 'decrease' ? -1:1;
      try{
        await client.post(`pu/storeMaterialsToCartForNewUser/${product.id}/${qty}/${product.CartLineId}`);
        let {items} = this.state;
        items = items.map((item) => {
          if(type == 'decrease') item.quantity = item.quantity - 1;
          if(type == 'increase') item.quantity = item.quantity + 1;
          return item;
        })
        this._snackbar.openSnack('Cart quantity has been updated');
        this.setState({items})
      }catch(err){
        console.log('err',err)
        this._snackbar.openSnack('Item not removed from your cart','error')
      }
    }

    onChangeTab(expanded){
      this.setState({expanded});
    }

    openAddressModal = (addressType) => this.setState({openAddressModal:true,addressType})
    
    closeAddressModal = () =>  this.setState({openAddressModal:false,addressType:'billing'})
    
    onPlaceOrder = async () => {
      if(!this.isAuth) return this.props.history.push('/sign');
      let {checkoutForm,items} = this.state;
      let cartObj = {cartId:this.cartId};
      let form = {
        ...checkoutForm,
        orderType:'online',
        cartId:this.cartId,
        qty: items.length,
        totalQuantity: sumBy(items,'quantity'),
        sellerID:{bPartnerId:210982},
        order:cartObj,
        invoice:cartObj,
        depotCode:'',
        igst:'',
        invoicePayment:{totalCard:'0',cod:''}
      }
      // delete checkoutForm.shipping;
      // delete checkoutForm.billing;
      this.setState({saving:true})
      console.log('resss order created',checkoutForm);
      let res = await client.post('/pr/createNewOnlineOrder',form);
      console.log('order completed',res);
      this.setState({saving:false});
      // this.props.history.push('/orders');

    }

    onChangeForm(event,name){
      this.setState({checkoutForm:{ ...this.state.checkoutForm, [name]: event.target.value }})
    }

    onAddressChange = (address,type) => {
      const {checkoutForm} = this.state;
      let billingAddress = checkoutForm.billingAddress;
      let shippingAddress = checkoutForm.shippingAddress;
      let addressId = address.addressId;
      if(type == 'billing'){
        billingAddress = { addressId }
      }else if(type == 'shipping'){
        shippingAddress = { addressId }
      }

      let additional = {};
      if(type == 'billing'){
        additional = {shipping: checkoutForm.sameAddress ? address:checkoutForm.shipping}
      }
      
      this.setState({checkoutForm:{ ...checkoutForm, [type]: address, shippingAddress, billingAddress,...additional  }})
    }

    onSaveAs = (event) => {
      const {checkoutForm} = this.state;
      let sameAddress = event.target.checked;
      this.setState({
        checkoutForm:{
          ...checkoutForm, 
          shipping: sameAddress ? checkoutForm.billing:{},
          sameAddress: Boolean(sameAddress)
        }
      });
    }

    render(){
        const {classes} = this.props
        const {loading,items,expanded,checkoutForm} = this.state;
        return (
            <Container className={classes.root}>
               <CommonSnackbar ref={ ref => this._snackbar = ref }/>

               <Row>
                <Typography variant="h6">
                  Order Summary
                </Typography>
              </Row>
              <Row>
                
                <Grid container spacing={4}>

                <Grid item xs={12} sm={8}>
              {
                !loading && 
                <div>

                {/* <Accordion square expanded={expanded === 'panel1'} onChange={this.handleTabChange.bind(this,'panel1')}>
                  <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                    <Typography>Product Summary</Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                   
                  {items.length > 0 && !loading && (
                    items.map((product, i) => (
                      <CartItem
                        key={i}
                        updateCart={this.updateCartQty}
                        remove={this.removeProduct}
                        product={product}
                        cardType="checkout"
                      />
                    ))
                  )}

                   <NextStepper rootClassName={classes.stepperRoot} onClick={this.onChangeTab.bind(this,'panel2')} />   

                  </AccordionDetails>
                </Accordion> */}

                <Accordion square expanded={expanded === 'panel2'} onChange={this.handleTabChange.bind(this,'panel2')}>
                 
                  <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                    <Typography>ADDRESS</Typography>
                  </AccordionSummary>
                 
                  <AccordionDetails>

                    <AddressModal sameAddress={checkoutForm.sameAddress} onSaveAs={this.onSaveAs} onAddressChange={this.onAddressChange} billing={checkoutForm.billing} shipping={checkoutForm.shipping}  onOpen={this.openAddressModal.bind(this,'billing')} type={this.state.addressType} open={this.state.openAddressModal} onClose={this.closeAddressModal} /> 

                  <NextStepper rootClassName={classes.stepperRoot} onClick={this.onChangeTab.bind(this,'panel3')} />  

                  </AccordionDetails>
                </Accordion>

                <Accordion square expanded={expanded === 'panel3'} onChange={this.handleTabChange.bind(this,'panel3')}>
                  
                  <AccordionSummary aria-controls="panel4d-content" id="panel4d-header">
                    <Typography>PAYMENT OPTIONS</Typography>
                  </AccordionSummary>

                  <AccordionDetails>
                      
                  <FormControl component="fieldset">
                    <RadioGroup aria-label="paymentMode" name="paymentMode" value={checkoutForm.paymentMode} onChange={ (e) => this.onChangeForm(e,'paymentMode')} >
                      <FormControlLabel value="CARD" control={<Radio color="primary"  />} label="Credit / Debit / ATM Card" />
                      <FormControlLabel value="COD" control={<Radio color="primary"  />} label="COD" />
                    </RadioGroup>
                  </FormControl>

                  <Grid item xs={12} sm={8} md={3}>
                    <SpinnerButton title="Place Order" handleButtonClick={this.onPlaceOrder} loading={this.state.saving}  />
                  </Grid>

                  </AccordionDetails>
                </Accordion> 

              </div>

              }

                   
                {
                  loading && [1,2].map((item,index) => (
                    <Grid key={index} item xs={12} sm={12}>
                    <Skeleton animation="wave" height={216}  />
                    </Grid>
                  ))
                }

                   
              </Grid>

              
            <Grid item xs={12} sm={4}>

            {
             loading && 
              <Box pt={0.5}>
              <Skeleton height={100} />
              <Skeleton height={50}/>
            </Box>
            }
            {items.length !== 0 && !loading && (
              <div className={classes.checkoutPanel}>
                <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Items total
                    </Typography>
                    <Typography variant="caption"></Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                   Rs. {checkoutForm.grandTotal}
                  </Typography>
                </Hbox>

                <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Shipping
                    </Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                    Rs. {checkoutForm.deliveryCharge}
                  </Typography>
                </Hbox>

                <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Total
                    </Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                   Rs. {checkoutForm.totalAmmount}
                  </Typography>
                </Hbox>

                {/* <Hidden xsDown implementation="css">
                  <Row>
                    <Divider />
                  </Row>
                </Hidden> */}
                {/* {
                  items.length === 0 ? null:(
                    <SpinnerButton title="Place Order" handleButtonClick={this.onPlaceOrder} loading={this.state.saving}  />
                  )
                } */}
                {/* {items.length === 0 ? null : (
                  <Link to={this.isAuth ? '/orders':'/signin'}>
                    <Button
                      color="primary"
                      variant="contained"
                      className={clsx(classes.checkoutButton, classes.docked)}
                    >
                      Place Order
                    </Button>
                  </Link>
                )} */}
              </div>
               )}
            </Grid>
         


                </Grid>
              </Row>
              
            </Container>
        )
    }
}

export default withStyles(styles)(Checkout)
