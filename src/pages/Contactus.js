import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Grid,Card,CardHeader,Divider,CardContent} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {AccordionFull} from './../components/Accordion'

const AntTabs = withStyles({
    root: {
      borderBottom: '1px solid #e8e8e8',
    },
    indicator: {
      backgroundColor: '#1890ff',
    },
  })(Tabs);
  
  const AntTab = withStyles((theme) => ({
    root: {
      textTransform: 'none',
      minWidth: 72,
      fontWeight: theme.typography.fontWeightRegular,
      marginRight: theme.spacing(4),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:hover': {
        color: '#40a9ff',
        opacity: 1,
      },
      '&$selected': {
        color: '#1890ff',
        fontWeight: theme.typography.fontWeightMedium,
      },
      '&:focus': {
        color: '#40a9ff',
      },
    },
    selected: {},
  }))((props) => <Tab disableRipple {...props} />);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  rightContainer:{
    marginTop:48
  }
}));

export default function Contactus() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [opanel,setOpanel] = React.useState('opanel1')
  const [rpanel,setRpanel] = React.useState('')
  const [ppanel,setPpanel] = React.useState('')
  const [hpanel,setHpanel] = React.useState('')
  const [fpanel,setFpanel] = React.useState('')

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleFTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && fpanel == panel) expanded = false;
    setFpanel(expanded)
  };

  const handleHTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && hpanel == panel) expanded = false;
    setHpanel(expanded)
  };

  const handleOTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && opanel == panel) expanded = false;
    setOpanel(expanded)
  };

  const handleRTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && rpanel == panel) expanded = false;
    setRpanel(expanded)
  };

  const handlePTabChange = (panel) => {
    let expanded = panel ? panel : false;
    if(panel && ppanel == panel) expanded = false;
    setPpanel(expanded)
  };

  return (
    <div className={classes.root}>
     <Grid container > 
     <Grid item xs={12} sm={12} md={8}> 
        <AntTabs value={value} onChange={handleChange} aria-label="ant example">
          <AntTab label="Order" />
          <AntTab label="Return" />
          <AntTab label="Payment" />
          <AntTab label="History" />
        </AntTabs>
        
      <TabPanel value={value} index={0}>
       
            <AccordionFull name="opanel1" value={opanel} title="How can I place an order?" onChange={handleOTabChange} expandIcon={true}  >
                <Typography>
                You can place your order by logging on to our RCM website or mobile application.
                </Typography>
            </AccordionFull>

      </TabPanel>

      <TabPanel value={value} index={1}>
       
      <AccordionFull name="rpanel1" value={rpanel} title="What is the minimum value of the order?" onChange={handleRTabChange} expandIcon={true}  >
                <Typography>
                You can place the order of any minimum value but there may be some shipping charges applied.
                </Typography>
            </AccordionFull>
      </TabPanel>

      <TabPanel value={value} index={2}>
       <AccordionFull name="ppanel1" value={ppanel} title="What is the payment method?" onChange={handlePTabChange} expandIcon={true}  >
            <Typography>
            Currently we will deliver the product to your nearest PUC and you have to pay cash to get product from there.
            </Typography>
       </AccordionFull>
      </TabPanel>

      <TabPanel value={value} index={3}>
       
       <AccordionFull name="hpanel1" value={hpanel} title="How can I get more information about products?" onChange={handleHTabChange} expandIcon={true}  >
                 <Typography>
                 You can find the product information on our website and mobile application. You can also download the product catalog from this link http://www.rcmbusiness.com/route and also you can watch product videos in the media section of website.
                 </Typography>
             </AccordionFull>
       </TabPanel>

        </Grid>

        <Grid item xs={12} sm={12} md={4} className={classes.rightContainer}>
          <Card >
                <CardHeader subheader="Frequently Asked Questions" />
                <Divider/>
                <CardContent>

                <AccordionFull name="fpanel1" value={fpanel} title="Can I purchase RCM products without becoming a direct seller?" onChange={handleFTabChange} expandIcon={true}  >
                      <Typography>
                      Yes, you can.
                      </Typography>
                </AccordionFull>

                <AccordionFull name="fpanel2" value={fpanel} title="I am not a RCM Direct seller yet, can I attend its product training program conducted by any RCM leader?" onChange={handleFTabChange} expandIcon={true}  >
                      <Typography>
                      Yes, you can.
                      </Typography>
                </AccordionFull>

                <AccordionFull name="fpanel3" value={fpanel} title="What type of products range does RCM offers?" onChange={handleFTabChange} expandIcon={true}  >
                      <Typography>
                      We have developed a range of daily consumable products in FMCG – Food & Non-Food (Household, Home care, Personal care, Eatable and Edible products), Health & Nutrition, Fashion & Accessories, Stationary &Travel accessories, footwear categories that are used from “Good Morning to Good Night”.
                      </Typography>
                </AccordionFull>

                <AccordionFull name="fpanel4" value={fpanel} title="Is there is any cost to become RCM Direct Seller?" onChange={handleFTabChange} expandIcon={true}  >
                      <Typography>
                      No, There is no cost to become Direct Seller.
                      </Typography>
                </AccordionFull>

                </CardContent>
            </Card>
       
        </Grid>

        </Grid>  
    </div>
  );
}
