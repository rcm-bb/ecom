import React, { useState,useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import client from './../utils/client'
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
const useStyles = makeStyles(() => ({
  root: {}
}));

const ProfilePage = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const [loading,setLoading] = useState(true)

  const [values, setValues] = useState({
    name:'',
    email: '',
    mobileNo: '',
  });

  const fetchProfile = async () => {
    // const res = await client.post('pu/getFilterMapByCategoryId/248');
    // console.log('profile res',res)
    const user = await client.post('pu/user/me');
    const {mobileNo,email,name} = user;
    setLoading(false)
    setValues({mobileNo,email,name})
  }

  useEffect(() => {
      fetchProfile()
  },[])

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  if(loading) return <CircularProgress />
  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <form
        autoComplete="off"
        noValidate
      >
        <CardHeader
        //   subheader="The information can be edited"
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Name"
                // margin="dense"
                name="name"
                onChange={handleChange}
                required
                value={values.name}
                variant="outlined"
              />
            </Grid>

            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                // margin="dense"
                name="email"
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Phone Number"
                // margin="dense"
                name="phone"
                onChange={handleChange}
                type="number"
                value={values.mobileNo || ''}
                variant="outlined"
              />
            </Grid>

            </Grid>
        </CardContent>
        <Divider />
        <CardActions disableSpacing>
          <Button
            color="primary"
            variant="contained"
          >
            Save
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

ProfilePage.propTypes = {
  className: PropTypes.string
};

export default ProfilePage;