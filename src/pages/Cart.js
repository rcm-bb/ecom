import React from 'react'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box';
import Row from '../components/Row'
import clsx from 'clsx'
import CartItem from '../components/Cart/CartItem';
import { withStyles } from '@material-ui/core/styles'
import { Grid, Hidden, Divider, Container, Button } from '@material-ui/core'
import Spacer from '../components/Spacer'
import {Link} from 'react-router-dom'
import { Hbox } from '../components/Box';
import {sumBy} from 'lodash'
import client from './../utils/client'
import Skeleton from '@material-ui/lab/Skeleton';
import {isAuthenticated} from './../utils/auth';
import CommonSnackbar from '../components/Snackbar';
import {setCartCount} from './../store/actions';
import store from './../store'
import {isEmpty} from 'lodash'
import cookie from 'react-cookies'
import SpinnerButton from '../components/SpinnerButton';

const styles = theme => ({
  root: {
    paddingBottom: '64px',
  },
  checkoutPanel: {
    backgroundColor: theme.palette.grey['200'],
    borderRadius: theme.shape.borderRadius,
    padding: `${theme.spacing(2)}px`,
  },
  total: {
    fontWeight: 'bold',
  },
  checkoutButton: {
    width: '100%',
  },
  docked: {
    [theme.breakpoints.down('xs')]: {
      fontSize: theme.typography.subtitle1.fontSize,
      padding: `${theme.spacing(2)}px`,
      position: 'fixed',
      left: 0,
      bottom: 0,
      width: '100%',
      zIndex: 10,
      borderRadius: '0',
      boxShadow: 'none',
    },
  },
}) 


const dummyItem =  {
  description: "Aliqua proident et duis ut amet eiusmod id ipsum. Excepteur est eiusmod reprehenderit est officia elit ipsum voluptate proident elit. Ipsum voluptate ut incididunt ut aliquip. Veniam cillum magna in dolore quis incididunt Lorem non elit. Irure nostrud anim cupidatat nisi cillum esse. In Lorem non elit nostrud quis aliqua ipsum irure. Nisi dolor dolore sunt qui tempor. Ipsum anim incididunt laboris proident quis excepteur eu deserunt irure sint est aliquip quis. Non exercitation in voluptate non eiusmod adipisicing amet excepteur magna pariatur cillum. Reprehenderit voluptate minim ipsum eiusmod labore cupidatat eu ex et.",
  id: 1,
  name: "Banarsi Saree",
  price: 10.99,
  priceText: "Rs. 10.99",
  quantity: 1,
  rating: 4.5,
  thumbnail:{
    alt: "Product 1",
    src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU"
  }
};

const dummyItems = [
  {
    description: "Aliqua proident et duis ut amet eiusmod id ipsum. Excepteur est eiusmod reprehenderit est officia elit ipsum voluptate proident elit. Ipsum voluptate ut incididunt ut aliquip. Veniam cillum magna in dolore quis incididunt Lorem non elit. Irure nostrud anim cupidatat nisi cillum esse. In Lorem non elit nostrud quis aliqua ipsum irure. Nisi dolor dolore sunt qui tempor. Ipsum anim incididunt laboris proident quis excepteur eu deserunt irure sint est aliquip quis. Non exercitation in voluptate non eiusmod adipisicing amet excepteur magna pariatur cillum. Reprehenderit voluptate minim ipsum eiusmod labore cupidatat eu ex et.",
    id: 1,
    name: "Banarsi Saree",
    price: 10.99,
    priceText: "Rs. 10.99",
    quantity: 1,
    rating: 4.5,
    thumbnail:{
      alt: "Product 1",
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU"
    }
  },
  {
    description: "Aliqua proident et duis ut amet eiusmod id ipsum. Excepteur est eiusmod reprehenderit est officia elit ipsum voluptate proident elit. Ipsum voluptate ut incididunt ut aliquip. Veniam cillum magna in dolore quis incididunt Lorem non elit. Irure nostrud anim cupidatat nisi cillum esse. In Lorem non elit nostrud quis aliqua ipsum irure. Nisi dolor dolore sunt qui tempor. Ipsum anim incididunt laboris proident quis excepteur eu deserunt irure sint est aliquip quis. Non exercitation in voluptate non eiusmod adipisicing amet excepteur magna pariatur cillum. Reprehenderit voluptate minim ipsum eiusmod labore cupidatat eu ex et.",
    id: 1,
    name: "Saree",
    price: 10.99,
    priceText: "Rs. 10.99",
    quantity: 1,
    rating: 4.5,
    thumbnail:{
      alt: "Product 1",
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU"
    }
  },
]

class Cart extends React.Component{
    constructor(){
        super();
        this.state = {
            items:[],
            loading:true,
            checking:false,
            payment:{
              deliveryCharge:0,
              totalAmmount:0,
              grandTotal:0
            }
        }
        this._snackbar = null  
        this.isAuth = isAuthenticated();
        let cardId = cookie.load('clid')
        this.cardId = !isEmpty(cardId) ? cardId:0;
    }

    componentDidMount(){
      this.fetchCart();
    }
    
    async fetchCart(){
      let url = this.isAuth ? 'pu/getCartLine':`pu/getCartLineForUnsignedUser/${this.cardId}`
      let {objectMap} = await client.post(url);
      // console.log('objectMap',objectMap)
      let items = [];
      objectMap && objectMap.cartLineDto && objectMap.cartLineDto.map((item) => {
        let product = item.materialID;
        let price = product.mPrice ? product.mPrice:0;
        items.push({
          ...dummyItem,
          name:product.name,
          price:product.mPrice,
          id:product.materialId,
          quantity:item.quantity,
          priceText:`Rs. ${product.mPrice}`,
          CartLineId:item.cartLineId,
          totalAmmount: parseInt(item.quantity) * parseFloat(price)
        })
        return item;
      });
      let totalAmmount = sumBy(items,'totalAmmount');
      store.dispatch(setCartCount('equal',items.length))
      this.setState(state => ({items,loading:false,payment:{ ...state.payment,grandTotal:totalAmmount,totalAmmount }}))
    }

    removeProduct = async (product) => {
      let url = this.isAuth ? `pu/removeMaterialFromCart/${product.CartLineId}`:`pu/removeMaterialFromCartOfUnsignedUser/${product.CartLineId}/${this.cardId}`
      try{
        await client.post(url);
        let {items} = this.state;
        items = items.filter((item) => item.id != product.id)
        this._snackbar.openSnack('Item has been removed');
        store.dispatch(setCartCount('decrease'))
        this.setState({items})
      }catch(err){
        console.log('err',err)
        this._snackbar.openSnack('Item not removed from your card','error')
      }
    }

    updateCartQty = async (product,type) => {
     let qty = type == 'decrease' ? -1:1;
      try{
        await client.post(`pu/storeMaterialsToCartForNewUser/${product.id}/${qty}/${product.CartLineId}`);
        let {items} = this.state;
        items = items.map((item) => {
          if(item.id == product.id){
            if(type == 'decrease') item.quantity = item.quantity - 1;
            if(type == 'increase') item.quantity = item.quantity + 1;
            item.totalAmmount =  parseInt(item.quantity) * parseFloat(item.price)
          }
          return item;
        });
        let totalAmmount = sumBy(items,'totalAmmount');
        this._snackbar.openSnack('Cart quantity has been updated');
        this.setState({items,payment:{ ...this.state.payment,grandTotal:totalAmmount,totalAmmount }})
      }catch(err){
        console.log('err',err)
        this._snackbar.openSnack('Item not removed from your card','error')
      }
    }

    onCheckout = () => {
      let link = this.isAuth ? '/checkout':'/signin';
      this.props.history.push(link)
    }

    render(){
        const {classes} = this.props
        const {loading,items,payment} = this.state;
        return (
            <Container className={classes.root}>
               <CommonSnackbar ref={ ref => this._snackbar = ref }/>
               <Row>
                <Typography variant="h6">
                  My Cart ({items.length} {items.length === 1 ? 'item' : 'items'})
                </Typography>
              </Row>
              <Row>
                
                <Grid container spacing={4}>

                <Grid item xs={12} sm={8}>
                   
                {
                  loading && [1,2].map((item,index) => (
                    <Grid key={index} item xs={12} sm={12}>
                    <Skeleton animation="wave" height={216}  />
                    </Grid>
                  ))
                }

                {items.length > 0 && !loading && (
                  items.map((product, i) => (
                    <CartItem
                      key={i}
                      updateCart={this.updateCartQty}
                      remove={this.removeProduct}
                      product={product}
                    />
                  ))
                )}
                {
                 items.length == 0 && !loading &&  (
                    <Typography variant="body1">There are no items in your cart.</Typography>
                  )
                }
              </Grid>

              
            <Grid item xs={12} sm={4}>

            {
             loading && 
              <Box pt={0.5}>
              <Skeleton height={100} />
              <Skeleton height={50}/>
            </Box>
            }
            {items.length !== 0 && !loading && (
              <div className={classes.checkoutPanel}>
             
             <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Items total
                    </Typography>
                    <Typography variant="caption"></Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                   Rs. {payment.grandTotal}
                  </Typography>
                </Hbox>

                <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Shipping
                    </Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                    Rs. {payment.deliveryCharge}
                  </Typography>
                </Hbox>

                <Hbox alignItems="flex-start">
                  <div>
                    <Typography variant="subtitle2" className={classes.total}>
                    Total
                    </Typography>
                  </div>
                  <Spacer />
                  <Typography variant="subtitle2" className={classes.total}>
                   Rs. {payment.totalAmmount}
                  </Typography>
                </Hbox>


                <Hidden xsDown implementation="css">
                  <Row>
                    <Divider />
                  </Row>
                </Hidden>

                {items.length === 0 ? null : (
                  <Grid item xs={12} sm={12}>
                <SpinnerButton title="Checkout" handleButtonClick={this.onCheckout} loading={this.state.checking}  />
                </Grid>
                )}

              </div>
               )}
            </Grid>
         


                </Grid>
              </Row>
            </Container>
        )
    }
}

export default withStyles(styles)(Cart)
