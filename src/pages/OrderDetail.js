import React from 'react';
import { Grid,Container,Typography,Card,CardContent } from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles'
import OrderListCard from './../components/Order/OrderListCard';
import Row from '../components/Row';
import client from './../utils/client';
import OrderDetailCard from '../components/Order/OrderDetailCard'
import {CircularProgress} from '@material-ui/core'
import {isEmpty} from 'lodash'

const styles = () => ({
    root:{
        display:'flex',
        flexDirection: 'column',
    },
    cardRoot:{
        display:'flex',
        padding:15
    }
})

const dummyItem =  {
    description: "Aliqua proident et duis ut amet eiusmod id ipsum. Excepteur est eiusmod reprehenderit est officia elit ipsum voluptate proident elit. Ipsum voluptate ut incididunt ut aliquip. Veniam cillum magna in dolore quis incididunt Lorem non elit. Irure nostrud anim cupidatat nisi cillum esse. In Lorem non elit nostrud quis aliqua ipsum irure. Nisi dolor dolore sunt qui tempor. Ipsum anim incididunt laboris proident quis excepteur eu deserunt irure sint est aliquip quis. Non exercitation in voluptate non eiusmod adipisicing amet excepteur magna pariatur cillum. Reprehenderit voluptate minim ipsum eiusmod labore cupidatat eu ex et.",
    id: 1,
    name: "Banarsi Saree",
    price: 10.99,
    priceText: "Rs. 10.99",
    quantity: 1,
    rating: 4.5,
    thumbnail:{
      alt: "Product 1",
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU"
    }
  };

class OrderDetail extends React.Component{
    constructor(){
        super();
        this.state = {
            orderDetail:{},
            items:[],
            loading:true,
            order:{}
        }
    }

    async componentDidMount(){
        let {objectMap} = await client.post('pu/getCartLine');
        // let items = [];
        // objectMap && objectMap.cartLineDto && objectMap.cartLineDto.map((item) => {
        //   let product = item.materialID;
        //   items.push({
        //     ...dummyItem,
        //     name:product.name,
        //     price:product.mPrice,
        //     id:product.materialId,
        //     quantity:item.quantity,
        //     priceText:`Rs. ${product.mPrice}`,
        //     CartLineId:item.cartLineId
        //   })
        //   return item;
        // })
        const {history} = this.props;
        let order = history.location && !isEmpty(history.location.state) ? history.location.state.order:null;
        if(isEmpty(order)) return history.push('/');
        console.log('did moutn oder',order)
        this.setState({loading:false,order}); 
        console.log('history',this.props.history)
    }

    render(){
        const {orderDetail,items,loading,order} = this.state;
        const {classes} = this.props;
        console.log('order',order)
        // return null;
        if(loading) return <CircularProgress />
        return (
            <Container className={classes.root} >

                <Grid container spacing={4} >

                <Grid item xs={12} sm={12}>
                
                <Row>
                    <Typography variant="subtitle1">
                    Order Detail
                    </Typography>
                </Row>

                <OrderListCard order={order}/> 

                <Card className={classes.cardRoot}>
                    {!isEmpty(order.billingAddress) && 
                        <Grid item xs={6} sm={6} md={6}>
                    
                            <Typography variant="subtitle1">
                                Billing Address
                            </Typography>
                    

                        <div>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.billingAddress.name} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.billingAddress.mobNo} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.billingAddress.addressType}, {order.billingAddress.streetAddress}, {order.billingAddress.pincode} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.billingAddress.city} </Typography>
                        </div>

                        </Grid>
                    }

                    {!isEmpty(order.shippingAddress) && 
                        <Grid item xs={6} sm={6} md={6}>
                    
                            <Typography variant="subtitle1">
                                Billing Address
                            </Typography>
                    

                        <div>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.shippingAddress.name} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.shippingAddress.mobNo} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.shippingAddress.addressType}, {order.shippingAddress.streetAddress}, {order.shippingAddress.pincode} </Typography>
                            <Typography variant="body2" color="textSecondary" component="p"> {order.shippingAddress.city} </Typography>
                        </div>

                        </Grid>
                    }
                </Card>                


                <Row>
                    <Typography variant="subtitle1">
                    Products Detail
                    </Typography>
                </Row>

                <Grid container spacing={4}>
                  {order.orderLine.length > 0 && !loading && (
                    order.orderLine.map((product, i) => (
                      <OrderDetailCard
                        key={i}
                        item={product}
                        cardType="checkout"
                      />
                    ))
                  )}

                </Grid>

                </Grid>
                
                </Grid>

            </Container>
        )
    }
}

export default withStyles(styles)(OrderDetail);