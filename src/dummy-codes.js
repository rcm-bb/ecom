//
import React from 'react'
import {Grid} from '@material-ui/core'
import DataTable from '../components/Common/DataTable'
import DatePickerBox from '../components/Form/DatePickerBox'
import DialogBox from '../components/Common/DialogBox'
import client from '../utils/client';
import {isEmpty} from 'lodash'
import SelectBox from '../components/Form/SelectBox';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box >
            {children}
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  

const defaultFilter = {
    visitingLeader:'',
    stateId:0,
    fromDate: new Date()
  }

class ProductTraining extends React.Component {
    constructor(){
        super();
        this.state = {
            filterOpen:false,
            filters:defaultFilter,
            states:[],
            leaders:[],
            tab:0
        }
        this._table = null;
    } 

    async componentDidMount(){
        let {objectMap} = await client.post('pu/getState');
        let leaderRes = await client.post('pu/getVisitingLeaderList');
        console.log('state leader',objectMap,leaderRes);
        this.setState({states: objectMap.stateListDto, leaders: leaderRes.objectMap.visitingLeaderList })
    }

    onChange = (name,event) => this.setState(prevState => ({filters:{...prevState.filters,[name]:event.target.value}}))
    
    onDateChange = value => this.setState(prevState => ({filters:{...prevState.filters, fromDate: value }}))

    onFilterChanged = () => {
        console.log('onFilterChanged');
        this._table && this._table.onFilterChanged();
        this.closeFilter();
    }

    openFilter = () => this.setState({filterOpen:true});

    closeFilter = () => this.setState({filterOpen:false});

    clearFilter = () => this.setState({filterOpen:false,filters:defaultFilter})

    handleTabChange = (event, tab) => {
        this.setState({tab})
    };

    render(){
        const {filterOpen,states,leaders,filters,tab} = this.state
        return (
            <div style={{
                padding:20
            }}>

                <Paper>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleTabChange}
                    aria-label="disabled tabs example"
                >
                    <Tab label="Date wise" />
                    <Tab label="State wise" />
                    <Tab label="Leader wise" />
                </Tabs>
                </Paper>

                <TabPanel value={tab} index={0}>

                  <DataTable ref={ref => this._table = ref} title="Product Training Programs" fetchURL='pu/getPTPBySeacrhDto' filters={filters} showFilter={true} onFilterClick={this.openFilter} />

                </TabPanel>
 
                <TabPanel value={tab} index={1}>
                    
                    <DataTable ref={ref => this._table = ref} title="Product Training Programs" fetchURL='pu/getPTPBySeacrhDto' filters={filters} showFilter={true} onFilterClick={this.openFilter} />
  
                </TabPanel>

                <TabPanel value={tab} index={2}>
                    
                    <DataTable ref={ref => this._table = ref} title="Product Training Programs" fetchURL='pu/getPTPBySeacrhDto' filters={filters} showFilter={true} onFilterClick={this.openFilter} />
  
                </TabPanel>

               
                {/* <DialogBox open={filterOpen} title='Filters' onCancel={this.closeFilter} onOK={this.onFilterChanged} >
                    <Grid container spacing={2}> 
                      
                        <Grid  item xs={12} md={6}>
                            <DatePickerBox Label="From Date" value={filters.fromDate} name="fromDate" onChange={this.onDateChange} />
                        </Grid>

                        <Grid  item xs={12} md={6}>
                            <SelectBox items={states} label="Select State" name="stateId" value={filters.stateId} onChange={this.onChange} itemValue="stateId" itemLabel="name" />
                        </Grid>

                        <Grid  item xs={12} md={6}>
                            <SelectBox items={leaders} label="Select Leader" name="visitingLeader" value={filters.visitingLeader} onChange={this.onChange} />
                        </Grid>

                    </Grid>
                </DialogBox>  */}
            </div>
        )
    }
}

export default ProductTraining;