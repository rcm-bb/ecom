import axios from 'axios';
import { BASE_URL } from '../config';
import {getToken} from './auth'

const _axios = axios.create({
  baseURL: BASE_URL,
  headers:{
    'Accept':'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    'Access-Control-Allow-Headers': 'Content-Type'
  }
});

_axios.interceptors.request.use(function(config){
  config.headers['Authorization'] = 'Bearer ' + getToken();
  return config;
})

_axios.interceptors.response.use((response) => {
  return response.data;
}, (error) => {
  return Promise.reject(error);
});

export default _axios