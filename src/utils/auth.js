import { func } from "prop-types";

export function storeToken(data){
    localStorage.clear();
    localStorage.removeItem('auth');
    localStorage.setItem('auth',JSON.stringify(data))

}

export function getToken(){
    let token = JSON.parse(localStorage.getItem('auth'));
    if(!token) return true;
    else return token.accessToken
}

export function isAuthenticated(){
    let data = localStorage.getItem('auth');
    if(data) return true;
    else return false;
}

export function logout(){
    localStorage.removeItem('auth');
    localStorage.clear();
}