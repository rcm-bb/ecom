export default function withDefaultHandler(handler, defaultHandler) {
    return (e, ...args) => {
      if (handler) {
        handler(e, ...args)
      }
  
      if (!e.defaultPrevented) {
        defaultHandler(e, ...args)
      }
    }
  }