import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        fontWeight: 700
      }

    },
    MuiAppBar: {
      boxShadow: 'none'
    },
    MuiCardHeader: {
      titleTypographyProps: {
        variant: 'h2'
      }
    }
  },
  typography: {
    h1: {
      fontWeight: 600
    },
    useNextVariants: true,
    fontFamily: "Montserrat",
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
  palette: {
    primary: {
      main: '#FF7500',
      contrastText: '#603D20'
    },
    secondary: {
      main: '#FF9800',
      contrastText: '#603D20'
    },
    error: {
      main: red.A400,
    },

    price: {
      full: '#000',
      main: '#000',
      sale: '#900'
    },
    background: {
      default: '#F5F5F5',
      padding: '20px',
      margin: 0,
      minHeight: '100vh'
    },

  },
  // spacing: {
  //   container: 15,
  //   row: 15
  // },
  overrides: {
    MuiInputLabel:{
      formControl:{
        top:-4
      },
    },
    MuiOutlinedInput:{
      input:{
        padding:'14.5px 14px'
      }
    }
  }
});

export default theme;