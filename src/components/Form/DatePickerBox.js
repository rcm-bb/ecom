import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider,DatePicker } from '@material-ui/pickers';

import { isEmpty } from 'lodash';
const DatePickerBox = ({value,label,onChange,error,name,...props}) => {
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
        
            <DatePicker
                disableToolbar
                inputVariant="outlined"
                format="dd/MM/yyyy"
                name="applicantDob"
                fullWidth
                id="date-picker-inline"
                error={!isEmpty(error)}
                helperText={error}
                variant="outlined"
                {...props}
                value={value}
                label={label}
                name={name}
                onChange={onChange.bind(this,name)}
            />
        </MuiPickersUtilsProvider>
    )
}

DatePickerBox.defaultProps = {
    value:'',
    error:null,
    onChange: () => null,
    label:'Select'
}

export default DatePickerBox;