import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {isEmpty,isObject} from 'lodash'
const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%'
  },
}));

export default function SelectBox(props) {
  const classes = useStyles();

  const {itemLabel,itemValue,items,onChange,value,label,error,name,required} = props;
  return (
      <FormControl variant="outlined" className={classes.formControl} error={!isEmpty(error)} required={required}>
        <InputLabel id="demo-simple-select-outlined-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={value || ''}
          onChange={onChange.bind(this,name)}
          label={label}
        >
          <MenuItem value='0'>{label}</MenuItem>
        {
            items.map((item,i) => {
              if(isEmpty(item)) return null;
              let isObj = isObject(item) ? true:false;
              return <MenuItem key={i} value={ !isObj ? item:item[itemValue]}>{ !isObj ?item:item[itemLabel]}</MenuItem>
            })
        }
        </Select>
        {error && <FormHelperText>{error}</FormHelperText>}
      </FormControl>
  );
}

SelectBox.defaultProps = {
    items:[],
    label:'Name',
    value:null,
    itemValue:'value',
    itemLabel:'title',
    onChange: () => null,
    error:null,
    name:'name',
    required:false
}