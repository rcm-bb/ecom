import React,{useRef,useCallback} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Button,IconButton,TextField,InputAdornment } from '@material-ui/core';
import AttachmentIcon from '@material-ui/icons/Attachment';

const useStyles = makeStyles((theme) => ({
  root: {
    width:'100%'
  },
  input: {
    display: 'none',
  },
  rootInput:{
      width:'100%'
  }
}));

export default function FileUploadBox({label,...props}) {
  const classes = useStyles();
  let _file = useRef(null);
  const handleChange = useCallback((file) => {
      console.log('file',file,file.target.files);
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file.target.files[0]);
      fileReader.onload = (e) => console.log('e file',e);
  })  
  return (
    <div className={classes.root}>
      <input
        className={classes.input}
        id="contained-button-file"
        type="file"
        ref={_file}
        onChange={handleChange}
      />
   
      <TextField
          variant="outlined"
          id='password'
          label={label}
          disabled={true}
          type={'text'}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={() => {
                      console.log('on clicke',_file);
                      _file.current.click();
                    //   this.refs['file-upload'].click()  
                    }}
                  >
                  <AttachmentIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
          className={classes.rootInput}
        />

    </div>
  );
}

FileUploadBox.defaultProps = {
    label:'Choose file'
}