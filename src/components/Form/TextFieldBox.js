import React from 'react'
import {TextField} from '@material-ui/core'
import {isEmpty} from 'lodash'

const TextFieldBox = ({onChange,value,name,error,...props}) =>(
    <TextField
        variant="outlined"
        // margin="dense"
        {...props}
        fullWidth
        onChange={onChange.bind(this,name)}
        value={value}
        error={!isEmpty(error)}
        helperText={!isEmpty(error) ? error[0]:null}
    />
);
TextFieldBox.defaultProps = {
    onChange: () => null,
    value:'',
    name:'name',
    error:null
}
export default TextFieldBox;