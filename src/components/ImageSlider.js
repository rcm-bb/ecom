import React from 'react'
import { Fade } from 'react-slideshow-image';
import {connect} from 'react-redux';
import {setSlider} from './../store/actions'
import 'react-slideshow-image/dist/styles.css'
import './../pages/main.css'
   
  const fadeProperties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: false,
    indicators: true,
    onChange: (oldIndex, newIndex) => {
       
    }
  }
class ImageSlider extends React.Component {
    render() {
        return (
            <div className="slide-container">
             
            <Fade {...fadeProperties}>

            {
                this.props.sliders && this.props.sliders.map((item,index) => (
                  <div className="each-fade" key={index}>
                  <div className="image-container">
                    <img src={item.image} />
                  </div>
                  <h2>{item.content}</h2>
                </div>
                ))
              }
            </Fade>
          </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
    sliders: state.root.sliders
   }
}

const mapPropsToState = (dispatch) => ({
  setSlider: slider =>  dispatch(setSlider(slider))
})

export default connect(mapStateToProps,mapPropsToState)(ImageSlider)