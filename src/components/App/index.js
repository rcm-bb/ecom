import React, { Component ,Suspense} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navigation from '../Navigation';
import Account, { AccountView, AccountManage } from '../../pages/Account';
import Action from '../../pages/Action';
import SignIn from '../../pages/Signin';
import SignUp from '../../pages/Signup';
import Box from '@material-ui/core/Box';
import { withAuthentication } from '../../session';
import * as ROUTES from '../../constants/routes';
import Categories from '../../pages/Categories';
import ProductDetail from '../../pages/ProductDetail';
import Cart from '../../pages/Cart';
import ForgotPassword from '../../pages/ForgotPassword';
import ProfilePage from '../../pages/Profile'
import Checkout from '../../pages/Checkout';
import Orders from '../../pages/Orders';
import OrderDetail from '../../pages/OrderDetail';
import KYCStatus from '../../pages/DirectSeller/KycStatus';
import ForgotRefNumber from '../../pages/DirectSeller/ForgotRefNumber';
import ForgotRefPassword from '../../pages/DirectSeller/ForgotRefPassword';
import KycApplication from '../../pages/DirectSeller/KycApplication';
import MarketingPlan from '../../pages/WorldOfRCM/MarketingPlan';
import ProductTraining from '../../pages/ProductTraining';
import DeliveryCenter from '../../pages/DeliveryCenter';
import Contactus from '../../pages/Contactus';
import MediaGallery from '../../pages/MediaGallery';
import NoticeBoard from '../../pages/DirectSeller/NoticeBoard';
import AccountMain from '../../pages/AccountSection/AccountMain';
import userDashboardContainer from '../../pages/AccountSection/userDashboardContainer';

const Home = React.lazy(() => import('../../pages/Home'));
const Landing = React.lazy(() => import('../../pages/Landing'));
const PasswordForget = React.lazy(() => import('../../pages/PasswordForget'));

class App extends Component {
  render() {
    return(
      <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <Navigation />

        <Box pb={2}>
          <Route path={ROUTES.ACCOUNT} component={Account} /> 
          <Route path={ROUTES.ACCOUNT_VIEW} component={AccountView} />
          <Route path={ROUTES.ACCOUNT_MANAGE} component={AccountManage} />
          <Route path={ROUTES.ACTION} component={Action} />
          <Route path={ROUTES.HOME} component={Home} /> 
          <Route exact path={ROUTES.LANDING} component={Landing} />
          <Route path={ROUTES.PASSWORD_FORGET} component={PasswordForget} />
          <Route path='/signin' component={SignIn} />
          <Route path='/signup' component={SignUp} />
          <Route path='/forgot' component={ForgotPassword} />
          <Route path="/s/:id" component={Categories} />
          <Route path="/p/:id" component={ProductDetail} />
          <Route path="/cart" component={Cart} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/profile" component={ProfilePage} />
          <Route path="/orders" component={Orders} />
          <Route path="/orderDetail/:id" exact component={OrderDetail} /> 
          <Route path="/dc/kyc" exact component={KYCStatus} />
          <Route path="/dc/forgotrefnumber" exact component={ForgotRefNumber} />
          <Route path="/dc/forgotrefpassword" exact component={ForgotRefPassword} />
          <Route path="/dc/kycapplication" exact component={KycApplication} />
          <Route path="/worcm/marketingplan" exact component={MarketingPlan} />
          <Route path="/worcm/deliverycenter" exact component={DeliveryCenter} />
          <Route path="/worcm/producttraining" exact component={ProductTraining} />
          <Route path="/contactus" exact component={Contactus} />
          <Route path="/noticeboard" exact component={NoticeBoard} />
          <Route path="/account" exact component={AccountMain} />
          <Route path="/userDashboard" exact component={userDashboardContainer}/>
          
        </Box>
      </Router>
      </Suspense>
    );
  }
}

export default withAuthentication(App);
