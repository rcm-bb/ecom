import UpdateEmail from './UpdateEmail';
import UpdatePassword from './UpdatePassword';
import UpdateProfile from './UpdateProfile';

export { UpdateEmail, UpdatePassword, UpdateProfile };
