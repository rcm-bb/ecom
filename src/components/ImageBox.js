import React, { PureComponent } from 'react'
import { GridListTile,GridList,Typography } from '@material-ui/core'
import { Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        padding: '6%'
      },
      gridList: {
          width:'100%',
        transform: 'translateZ(0)',
      },

     image:{
         width:'100%',
         height:'100%'
     },
     backgroundImage:{
        height:'100%',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        textAlign: 'center'
     } 

});



class ImageBox extends PureComponent {
    render() {
        const { classes, images,title } = this.props
        return (
            <div className={classes.root} >
                <Typography variant="subtitle2" gutterBottom>
                    {title ||''}
                </Typography>
                <GridList cellHeight={200} spacing={1} className={classes.gridList}>
                    {images.map((tile,index) => (
                        <GridListTile  key={index} cols={index === 0 ? 2 : 1} rows={index === 0 ? 2 : 1}>
                            {/* <img className={classes.image} src={tile}  /> */}
                            <div style={{
                                backgroundImage:"url("+tile+")"
                            }} className={classes.backgroundImage}>

                            </div>
                            {/* <div className={classes.backgroundImage} style={{backgroundImage:`url(${tile})`}}></div> */}
                        </GridListTile>
                    ))}
                </GridList>
            </div>


        )
    }
}

export default withStyles(styles, { withTheme: true })(ImageBox)