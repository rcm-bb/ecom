import { Toolbar } from '@material-ui/core';
import { Link,useHistory } from 'react-router-dom'
import { useStyles } from './useStyles';
import { withStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box';
import React from 'react'
import CartButton from './../CartButton';
import {connect} from 'react-redux'
// import {compose} from 'recompose'
import PersonIcon from '@material-ui/icons/Person';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { isAuthenticated } from '../../utils/auth';
// import ToolbarButton from '../ToolbarButton';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = theme => ({
  title: {},
  logo: {
    position: 'absolute',
    left: 10,
    top: 0,
    [theme.breakpoints.down('xs')]: {
      left: '50%',
      top: 6,
      marginLeft: -60,
    },
  },
  toolbar: {
    padding: 0,
    margin: 0,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',

    [theme.breakpoints.down('xs')]: {
      padding: 5,
    },
  },
  icon:{
    color:'white'
  },  
  loginOutIcon:{
    margin:'0 5px'
  }
})


const Headers = ({ cartCount,onLogout }) => {
  const [open, setOpen] = React.useState(false);
  const isAuth = isAuthenticated();
  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setOpen(false);
  };

  const onLogoutApp = () => {
    setOpen(false);
    setAnchorEl(null);
    onLogout();
  };

  const onClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const onClose = () => {
    setAnchorEl(null);
  };

  const onLinkClick = React.useCallback((link) => {
    history.push(link); 
    setAnchorEl(null);
  })

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Logout"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure to logout?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={onLogoutApp} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>

        <Toolbar variant="dense" className={classes.toolbar}>
          <Link className={classes.logo} to="/"> 
             
              <Box component="div" m={1}>
                <img style={{width:'110px'}} src={require('./logo.png')}></img>
              </Box>
             
          </Link>
          <div className={classes.root} />

          <CartButton quantity={ cartCount || 0} />

          <div>

          <IconButton
            aria-label="more"
            aria-controls="long-menu"
            aria-haspopup="true"
            onClick={onClick}
          >
            <PersonIcon />
          </IconButton>

          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={onClose}
          >
            { isAuth ? 
              <div>
                <MenuItem onClick={onLinkClick.bind(this,'/profile')}>Profile</MenuItem>
                <MenuItem onClick={onLinkClick.bind(this,'/orders')}>Orders</MenuItem>
                <MenuItem onClick={handleClickOpen}>Logout</MenuItem>
              </div>
              :
              <MenuItem onClick={onLinkClick.bind(this,'/signin')}>SignIn</MenuItem>
            }
          </Menu>
        </div>

          {/* <Link to={isAuth ? '/profile':'/signin'} style={{color:'white !important'}} > <ToolbarButton> <PersonIcon /> </ToolbarButton> </Link> */}
          {/* {
            isAuth && <ToolbarButton> <ReorderIcon /> </ToolbarButton>
          }
          {
            isAuth && <ToolbarButton>  <ExitToAppIcon onClick={handleClickOpen} /> </ToolbarButton>
          } */}
          
        </Toolbar>

      
    </div>
  );
};

const mapStateToProps = state => ({
  cartCount: state.root.cartCount
})


export const Header = connect(mapStateToProps,null)(withStyles(styles)(Headers));
// export const Header = compose(
//   withStyles(styles),
//   connect(mapStateToProps,null)
// )(Headers)