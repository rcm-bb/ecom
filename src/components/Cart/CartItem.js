import React from 'react'

import { useState, useEffect, useContext } from 'react'
import { Paper, IconButton, Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Close as CloseIcon } from '@material-ui/icons'
import Row from './../Row'
import Link from './../Link/Link'
import QuantitySelector from './../QuantitySelector'
import { Hbox } from './../Box'
import Image from './../Image'
// import SessionContext from 'react-storefront/session/SessionContext'
import RemoveDialog from './RemoveDialog'

const styles = theme => ({
    root: {
      flex: 1,
      padding: theme.spacing(2, 5, 2, 2),
      marginBottom: theme.spacing(2),
      position: 'relative',
    },
    thumb: {
      marginRight: theme.spacing(2),
      width: 200,
      height:140,
      [theme.breakpoints.down('xs')]: {
        width: 100,
      },
    },
    label: {
      marginRight: theme.spacing(0.6),
    },
    remove: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
  })

  function CartItem({ product, updateCart, remove,classes,cardType = 'cart' }) {
    // const [product,] = useState(prod)
    const [open, setOpen] = React.useState(false)
    // const classes = useStyles()
    // const { actions, session } = useContext(SessionContext)
  
    const handleRemove = product => {
      remove(product)
      setOpen(false)
    //   actions.updateCartCount(session.itemsInCart - 1)
    }
  
    // useEffect(() => {
    //   updateCart(product)
    // }, [product.quantity])
  
    return (
      <>
        <Paper className={classes.root} elevation={3}>
          <Hbox alignItems="flex-start">
            <div className={classes.thumb}>
              <Image src={product.thumbnail.src} fill aspectRatio={1} quality={50} />
            </div>
            <div className={classes.info}>
              <Link as={product.url} href={`/p/${product.id}`} prefetch="visible" pageData={{ product }}>
                
                  <Typography variant="subtitle1">{product.name}</Typography>
              
              </Link>
              <Typography className={classes.price}>{product.priceText}</Typography>
              {product.size && product.size.selected && (
                <Hbox>
                  <Typography className={classes.label}>Size:</Typography>
                  <Typography>{product.size.selected.text}</Typography>
                </Hbox>
              )}
              
              <Row>
              <Typography>Quantity : {cardType != 'cart' &&   <span>{product.quantity}</span> } </Typography>
                { cardType == 'cart' &&  
                  <QuantitySelector
                    value={product.quantity}
                    onChange={(value,type) => {
                      updateCart(product,type) 
                    }}
                  />
                }

              </Row>
            </div>
          </Hbox>
          { cardType == 'cart' && 
          <IconButton className={classes.remove} onClick={() => setOpen(true)}>
            <CloseIcon />
          </IconButton>
          }
        </Paper>
        <RemoveDialog
          open={open}
          setOpen={setOpen}
          name={product.name}
          action={() => handleRemove(product)}
        />
      </>
    )
  }

  export default withStyles(styles)(CartItem)