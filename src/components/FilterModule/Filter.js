import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import {Accordion,AccordionSummary,AccordionDetails,Checkbox, FormGroup, Typography, FormControlLabel } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import {findIndex} from 'lodash';
import { FormatListBulletedTwoTone } from '@material-ui/icons';

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    root: {
        boxShadow: 'none',
        borderBottom: `1px solid ${theme.palette.divider}`,
        background: 'transparent',
    
        '&::before': {
          display: 'none',
        },
    
        '& > *:first-child': {
          padding: '0',
          minHeight: '0',
        },
      },
      detailsRoot:{
        overflow: 'auto',
        maxHeight: 300
      },
      expandedPanel: {
        '&$root': {
          margin: 0,
        },
      },

      title: {},
    
      expandIcon: {},

      matches: {
        marginLeft: '5px',
        display: 'inline',
      },

      groupLabel: {
        display: 'flex',
        alignItems: 'center',
      },
  })

function Filter(props) {
    const { classes,facetGroups,filters } = props;
    return (
        <>
            {
                facetGroups && facetGroups.map((item,index) => {
                    return (
                        <Accordion key={index} classes={{
                            root: clsx({
                              [classes.root]: true,
                            
                            }),
                            expanded: classes.expandedPanel,
                          }}
                          defaultExpanded={index == 0}
                          >
                            <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                            >
                            <Typography className={classes.heading}>{item.name}</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                            <div className={classes.detailsRoot} >
                                {
                                    <FormGroup>
                                        {item.attributeValues.map((facet, i) => {
                                          return (
                                            <FormControlLabel
                                                key={i}
                                                label={
                                                <div className={classes.groupLabel}>
                                                    <span>{facet.name}</span>
                                                    <Typography variant="caption" className={classes.matches} component="span">
                                                    {/* ({facet.name}) */}
                                                    </Typography>
                                                </div>
                                                }
                                                control={
                                                <Checkbox
                                                    checked={findIndex(props.filters,o => o.attributeValueId == facet.attributeValueId) >= 0 ? true:false}
                                                    color="primary"
                                                    onChange={props.onChangeFilter.bind(this,item,facet)}
                                                />
                                                }
                                            />
                                            )
                                        })}
                                    </FormGroup>
                                }
                            </div>
                            </AccordionDetails>
                        </Accordion>
                    )
                })
            }
           
        </>
    )
  }

  export default withStyles(styles)(Filter)
  
