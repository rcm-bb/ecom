import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Addresses from './Addresses';
import CommonSnackbar from './../Snackbar';
import client from './../../utils/client';
import SpinnerButton from './../SpinnerButton'
import AddressCard from './../Checkout/AddressCard'
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Row from '../Row';
import {Checkbox,FormControlLabel} from '@material-ui/core';

let cardActions = [
  { title:'Edit',action:'update'},
  { title:'Change',action:'change' }
]

export default class AddressModal extends React.Component {

    constructor(){
        super();
        this.state = {
            openForm:false,
            form:{},
            saving:false,
            loading:true,
            items:[],
            open:false,
            type:null
        }
        this._snackbar = null;
    }

    toggleModal(openForm = false){
        this.setState({openForm,form:{}});
    }

    async componentDidMount(){
        this.fetchData();
    }

    async fetchData(){
        let {objectMap} = await client.post('pu/getAddressList/18061925');
        this.setState({loading:true});
        if(objectMap &&  objectMap.addressDto) this.setState({items: objectMap.addressDto,loading:false })
    }

    onChangeAddressForm = (name,value) => {
        let form = {
          ...this.state.form,
            [name]:value
        }
        this.setState({form});
        
    }

    addNewAddress = async () => {
        let {form} = this.state;
        form = {
            ...form,
            addressType:this.props.addressType,
            user:{
                userId:18061925
            },
        }
        this.setState({saving:true})
        let {data} = await client.post('pu/addNewAddress',form);
        this._snackbar.openSnack('Add New Address Successfully')
        this.setState({saving:false,form:{},openForm:false,items: this.state.items.concat(data)})
    }

    onAction(i,data,action){
        let {items} = this.state;
        if(action == 'delete'){
             items.splice(i,1);
            this.setState({items})
            this._snackbar.openSnack('Address has been deleted successfully')
        }else if(action == 'edit'){
            this.setState({form:data,openForm:true})
        }else if(action == 'change'){
          this.setState({form:{},open:true})
        }else if(action == 'update'){
          this.setState({open:true}, () => this.setState({form:data,openForm:true}))

        }
    }

    onChangeAction(data,type,action){
     if(action == 'change'){
        this.setState({form:{},open:true,type})
      }else if(action == 'update'){
        this.setState({open:true,type}, () => this.setState({form:data,openForm:true}))
      }
    }


    openModal(type){
      this.setState({open:true,type})
    }

    onSelect(item){
      this.setState({open:false})
      this.props.onAddressChange(item,this.state.type)
    }

    onClose = () => this.setState({open:false})

  render(){
      const {form,saving,openForm,loading,items,open} = this.state;
      const {type,billing,shipping,sameAddress} = this.props;
      if(loading) return null;
  return (
    <div>
      <Row>
      <div style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}>

      <Typography variant="h6" gutterBottom>
        Billing Address
      </Typography>

      <IconButton aria-label="delete" onClick={this.openModal.bind(this,'billing')}>
        <AddIcon />
      </IconButton>

      </div>
      <AddressCard item={billing} onAction={this.onChangeAction.bind(this,billing,'billing')}  actions={cardActions} selected={true} />
      </Row>

     
      <Row>
      <div style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}>

      <Typography variant="h6" gutterBottom>
        Shipping Address
      </Typography>

      <IconButton aria-label="delete"  onClick={this.openModal.bind(this,'shipping')}>
        <AddIcon />
      </IconButton>

      </div>
      <AddressCard item={shipping} onAction={this.onChangeAction.bind(this,shipping,'shipping')}  actions={cardActions} selected={true} />
    
        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox color="secondary" name="sameAddress" value={sameAddress} checked={sameAddress} />}
            onChange={this.props.onSaveAs.bind(this)} label="same as billing address"
          />
        </Grid>

      </Row>

      
    <CommonSnackbar ref={ ref => this._snackbar = ref }/>
      <Dialog open={open} onClose={this.toggleModal.bind(this,false)} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Addresses {items.length > 0 ? `(${items.length})`:'' }</DialogTitle>
        <IconButton aria-label="close" style={{
            position:'absolute',
            top:10,
            right:10
        }} onClick={this.onClose.bind(this)} >
          <CloseIcon />
        </IconButton>
        <DialogContent>
        <div style={{
            display: 'flex',
            flex: 1,
            justifyContent: 'center',
            minHeight: 400,
            minWidth: 400
        
        }}>
        { loading ?  <CircularProgress />:
        <Grid container spacing={2}> 

            {
                items.map((item,i) => {
                  return <AddressCard item={item} key={i}  onAction={this.onAction.bind(this,i,item)} onSelect={this.onSelect.bind(this)} selected={false} />
                })
            }
            
            <div style={{
            position:'absolute',
            bottom: 20,
            right: 20
        }}>
        <Fab
          aria-label="save"
          color="primary"
          onClick={this.toggleModal.bind(this,true)}
        >
           <AddIcon />
        </Fab>
      </div>
      
        </Grid>
    }
        </div>
    <Dialog open={openForm} onClose={this.toggleModal.bind(this,false)} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add/Edit Address</DialogTitle>
        <DialogContent>

        <Addresses type={type}  form={form} onFormChange={this.onChangeAddressForm}  />


        </DialogContent>
        <DialogActions>

          <SpinnerButton variant="text" title="Cancel"  handleButtonClick={this.toggleModal.bind(this,false)} loading={saving} color="primary" />
          <SpinnerButton variant="text" title="Save"  handleButtonClick={this.addNewAddress} loading={saving} autoFocus/>

        </DialogActions>

      </Dialog>

        </DialogContent>
        {/* <DialogActions>

          <SpinnerButton variant="text" title="Cancel"  handleButtonClick={this.toggleModal.bind(this,false)} loading={saving} color="primary" />
          <SpinnerButton variant="text" title="Save"  handleButtonClick={this.addNewAddress} loading={saving} autoFocus/>
        </DialogActions> */}
      </Dialog>
    </div>
  );
 }
}

AddressModal.defaultProps = {
    title:'Shipping address',
    type:'shipping',
    onFormChange: () => null,
    onAddressChange: () => null,
    onSaveAs: () => null,
    sameAddress: false
}
