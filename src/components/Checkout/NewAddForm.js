import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Addresses from './Addresses';
import CommonSnackbar from './../Snackbar';
import client from './../../utils/client';
import SpinnerButton from './../SpinnerButton'
import AddressCard from './../Checkout/AddressCard'
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

export default class AddNewForm extends React.Component {

    constructor(){
        super();
        this.state = {
            open:false,
            form:{},
            saving:false
        }
        this._snackbar = null;
    }

    toggleModal(open = false){
        // this.setState({open,form:{}});
        console.log('onOpen')
        this.props.onOpen();
    }

    onChangeAddressForm = (name,value) => {
        let form = {
          ...this.state.form,
            [name]:value
        }
        this.setState({form});
        
    }

    addNewAddress = async () => {
        const {form} = this.state;

        console.log('form',form)
        return 
        let obj = {
          name:'kishan',
          code:999,
          description:'this is for home',
          houseNo:123,
          streetAddress:'near court choraha',
          landmark:'home town',
          addressType:'home',
          city:'udapiur',
          state:{
            stateId:70
          },
          country:{
            countryId:1
          },
          pincode:313324,
          mobNo:7737874922,
          email:'kishan@gmail.com',
          user:{
            userId:18061925
          },
          defaultAddress:'no',
          // district:{}
        }
        this.setState({saving:true})
        let {objectMap} = await client.post('pu/addNewAddress',obj);
        console.log('objectMap',objectMap)
        this._snackbar.openSnack('Add New Address Successfully')
        this.setState({saving:false,form:{},open:false})
      }


  render(){
      const {open,form,saving} = this.state;
      const {title,onFormChange,type} = this.props
  return (
    <div>
      
      <Addresses type={type} title={title} form={this.props.form} onFormChange={onFormChange.bind(this)} showTitle={true} onAddNew={this.toggleModal.bind(this,true)}  />
      <CommonSnackbar ref={ ref => this._snackbar = ref }/>
      <Dialog open={open} onClose={this.toggleModal.bind(this,false)} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
        <DialogContent>

        <Addresses type={type}  form={form} onFormChange={this.onChangeAddressForm}  />


        </DialogContent>
        <DialogActions>

          <SpinnerButton variant="text" title="Cancel"  handleButtonClick={this.toggleModal.bind(this,false)} loading={saving} color="primary" />
          <SpinnerButton variant="text" title="Save"  handleButtonClick={this.addNewAddress} loading={saving} autoFocus/>

        </DialogActions>

      </Dialog>
    </div>
  );
 }
}

AddNewForm.defaultProps = {
    title:'Shipping address',
    type:'shipping',
}
