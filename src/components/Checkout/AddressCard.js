import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import classnames from 'classnames'
import {isEmpty} from 'lodash';

const styles = (theme) => ({
  root: {
      cursor:'pointer'
  },
  selected:{
    border:`1px solid ${theme.palette.primary.main}`
  },
  media: {
    height: 0,
    // paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  cardHeader:{
      paddingBottom:'0px !important'
  }
});


class AddressCard extends React.Component{
    constructor(){
        super();
        this.state = {
            anchorEl:null
        }
    }

    handleClose(action,event){
        this.setState({anchorEl:null},this.props.onAction.bind(this,action))
    }

    onCloseMenu =  () => this.setState({anchorEl:null})

    handleClick = (event) => {
        this.setState({anchorEl:event.currentTarget})
    }

    render(){
        const {classes,item,selected,onSelect,actions} = this.props;
        const {anchorEl} = this.state;
        if(isEmpty(item)) return null;
        return (
            <Grid item xs={12} sm={12} md={6}>
            <Card className={classnames(classes.root, selected && classes.selected)}>
            <CardHeader
                action={
                <div>
                <IconButton aria-label="settings" onClick={this.handleClick}>
                    <MoreVertIcon />
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={this.onCloseMenu}
                >
                    {
                        actions.map((item,index) => <MenuItem onClick={this.handleClose.bind(this,item.action)}>{item.title}</MenuItem>)
                    }
            
                </Menu>
                </div>

                }
                subheader={item.name}
                classes={{
                    root:classes.cardHeader
                }}
            />

            <CardContent onClick={onSelect.bind(this,item)}>
                <Typography variant="body2" color="textSecondary" component="p"> {item.name} </Typography>
                <Typography variant="body2" color="textSecondary" component="p"> {item.mobNo} </Typography>
                <Typography variant="body2" color="textSecondary" component="p"> {item.streetAddress} </Typography>
                <Typography variant="body2" color="textSecondary" component="p"> {item.city} </Typography>
            </CardContent>
            </Card>
            </Grid>
        )
    }
}

AddressCard.defaultProps = {
    onAction:() => null,
    onSelect: () => null,
    selected:false,
    actions:[
        { title:'Edit',action:'edit' },
        { title:'Delete',action:'delete'}
    ]
}


export default withStyles(styles,{withTheme:true})(AddressCard);