import React from 'react';
import Typography from '@material-ui/core/Typography';
import Row from '../Row';
import { Grid,TextField,FormControlLabel,Checkbox } from '@material-ui/core'
import Autocomplete from './../AutoComplete';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';

class Addresses extends React.Component{
    constructor(){
        super();
    }

    onChange = (event) => {
        this.props.onFormChange(event.target.name,event.target.value)
    }

    onAutoCompleteChange = (name,value) => {
      this.props.onFormChange(name,value)
    }

    render(){
        const {title,type,form,onAddNew,showTitle,isEditable} = this.props;
        return (
            <Row>
            <React.Fragment>
            { showTitle && 
           <div style={{
             display: 'flex',
             justifyContent: 'space-between',
             alignItems: 'center'
           }}>

           <Typography variant="h6" gutterBottom>
              {title}
            </Typography>

            <IconButton aria-label="delete" onClick={onAddNew.bind(this)}>
              <AddIcon />
            </IconButton>

           </div>
           }
           
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="name"
                  label="Full Name"
                  value={form.name}
                  fullWidth
                  autoComplete="given-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.name ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>
              
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="email"
                  label="Email"
                  value={form.email}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.email ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="mobNo"
                  label="Mobile Number"
                  value={form.mobNo}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.mobNo ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="houseNo"
                  label="House No."
                  value={form.houseNo}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.houseNo ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>


              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="streetAddress"
                  label="Street Address"
                  value={form.streetAddress}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.streetAddress ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>



              <Grid item xs={12} sm={6}>
                <TextField
                  name="landmark"
                  label="Landmark"
                  value={form.landmark}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.landmark ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>


              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="city"
                  label="City"
                  value={form.city}
                  fullWidth
                  autoComplete="family-name"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.city ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>
  
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  name="code"
                  label="Zip / Postal code"
                  value={form.code}
                  fullWidth
                  autoComplete="shipping postal-code"
                  onChange={this.onChange}
                  InputLabelProps={{
                    shrink: form.code ? true:false
                  }}
                  disabled={isEditable}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  required
                  fullWidth
                  name='country'
                  value={form.country}
                  onChange={this.onAutoCompleteChange}
                  disabled={isEditable}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Autocomplete
                  required
                  name='state'
                  fetchURL="/pu/getState"
                  label="Choose a State"
                  mapField="stateListDto"
                  itemValue="stateId"
                  itemLable="name"
                  fullWidth
                  value={form.state}
                  onChange={this.onAutoCompleteChange}
                  disabled={isEditable}
                />
              </Grid>
            { type == 'shipping' &&
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox color="secondary" name="sameAddress" value={form.sameAddress == 'yes' ? 'no':'yes'} checked={form.sameAddress === 'yes' ? true:false} />}
                  onChange={this.onChange} label="same as billing address"
                  
                />
              </Grid>
              }
            </Grid>
          </React.Fragment>
            </Row>
        )
    }
}

Addresses.defaultProps = {
    title:'Shipping address',
    type:'shipping',
    onFormChange: () => null,
    form:{},
    onAddNew: () => null,
    showTitle:false,
    isEditable:false
}

export default Addresses;
