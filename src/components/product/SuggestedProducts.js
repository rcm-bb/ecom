import React, { useState, useEffect } from 'react'
import {withStyles} from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import client from './../../utils/client'
import ProductItem from './productItem'
import { Typography } from '@material-ui/core'
import LoadMask from './../LoadMask'
import { Tune } from '@material-ui/icons'
import isEmpty from 'lodash/isEmpty'
export const styles = theme => ({
  products: {
    minHeight: 250,
    position: 'relative',
    margin: theme.spacing(0, -2),
    overflowX: 'auto',
    maxWidth: '100%',
    [theme.breakpoints.down('xs')]: {
      maxWidth: '100vw',
    },
  },
  wrap: {
    padding: theme.spacing(0, 0, 0, 2),
    display: 'flex',
    flexDirection: 'row',
    width: 'max-content',
  },
  product: {
    margin: theme.spacing(0, 2, 0, 0),
    minWidth: 150,
  },
})

class SuggestedProducts extends React.Component{
    constructor(){
        super();
        this.state = {
            suggestedProducts:[]
        }
    }
    async componentDidMount(){
        let catId = 248;
        let limit = 7;
        let page = 1;
        let level = 1;
        let {objectMap} = await client.get(`pu/getProductsREInit?catId=${catId}&limit=${limit}&page=${page}&lev=${level}`)
        this.setState({suggestedProducts:objectMap.pcWrapper.products})
    }
    render(){
        const {suggestedProducts} = this.state;
        const {classes,theme} = this.props;
        let isProductsEmpty = isEmpty(suggestedProducts)
        return (
          <div>
            <Typography variant="h6" component="h3">
              Suggested Products
            </Typography>
            <div className={classes.products}>
              <LoadMask show={isProductsEmpty} />
              <div className={classes.wrap}>
                {!isProductsEmpty && !isEmpty(suggestedProducts) && 
                  suggestedProducts.map((product, i) => (
                    <ProductItem
                      product={product}
                      index={i}
                      key={i}
                      colorSelector={false}
                      className={classes.product}
                    />
                  ))}
              </div>
            </div>
          </div>
        )
    }
}

SuggestedProducts.propTypes = {
  /**
   * The product being displayed.
   */
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }),
}

export default  withStyles(styles)(SuggestedProducts)
