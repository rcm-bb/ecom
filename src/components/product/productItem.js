import React from 'react'
import { Link } from "react-router-dom";
import client from './../../utils/client'
import { Vbox } from './../Box'
import { Typography,Button,Grid } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import ForwardThumbnail from './../ForwardThumbnail'
import Image from './../Image'
import clsx from 'clsx'
import ProductOptionSelector from './../option/ProductOptionSelector' 
import CommonSnackbar from '../Snackbar';
import SpinnerButton from '../SpinnerButton';
import store from './../../store'
import { setCartCount } from '../../store/actions';
import QuantitySelector from './../QuantitySelector'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Hbox } from './../Box'
import Label from './../Label'
import { isAuthenticated } from '../../utils/auth';
import cookie from 'react-cookies'
const styles = theme => ({
  root: {
    padding: `${theme.spacing(2)}px 0`,
  },
  thumbnail: {
    marginBottom: theme.spacing(1),
  },
  link: {
    textDecoration: 'none',
    color: 'inherit',
  },
  price: {
    marginTop: '5px',
  },
  reviews: {
    marginTop: '5px',
  },
  reviewCount: {
    marginLeft: '2px',
  },
  info: {
    margin: '0',
  },
})

let dummyColor = {
  options:[
    {
      id: "cccccc",
      image: "https://via.placeholder.com/350/cccccc/cccccc",
      text: "Neutral Gray",
      thumbnail: "https://via.placeholder.com/200/cccccc?text=Product%201"
    },
    {
      id: "d32f2f",
      image: "https://via.placeholder.com/350/d32f2f/d32f2f",
      text: "Candy Apple Red",
      thumbnail: "https://via.placeholder.com/200/d32f2f/fff?text=Product%201"
    },
    {
      id: "388E3C",
      image: "https://via.placeholder.com/350/388E3C/388E3C",
      text: "Forest Green",
      thumbnail: "https://via.placeholder.com/200/388E3C/fff?text=Product%201"
    },
    {
      id: "1565c0",
      image: "https://via.placeholder.com/350/1565c0/1565c0",
      text: "Azure Blue",
      thumbnail: "https://via.placeholder.com/200/1565c0`/fff?text=Product%201"
    }
  ],
  selected:{
    id: "cccccc",
    image: "https://via.placeholder.com/350/cccccc/cccccc",
    text: "Neutral Gray"
  }
}

class ProductItem extends React.Component{
  constructor(){
    super();
    this.state = {
      loading:false,
      dialog:false,
      quantity:1
    }
    this._snackbar = null
  }

  closeModal = () =>  this.setState({dialog :false})

  openModal = () =>  this.setState({dialog :true})

  addUpdateCart = async () => {
    let {product} = this.props;
    const {quantity} = this.state
    this.setState({loading:true})
    try{
      console.log('isAuthenticated',isAuthenticated())
      let isAuth = isAuthenticated();
      let cardId = 0;
      if(!isAuth){
        cardId = cookie.load('clid')
        cardId = cardId ? cardId:0;
      }
      let url = isAuth ? `pu/storeMaterialsToCart/${product.id}/${quantity}`:`pu/storeMaterialsToCartForNewUser/${product.id}/${quantity}/${cardId}`
      let {objectMap} = await client.post(url)
      if(!isAuth){
        cookie.save('clid',objectMap.cartId)
      }
      this.setState({loading:false,dialog:false})
      store.dispatch(setCartCount('increase'))
      this._snackbar.openSnack('Item added in your card successfully')
    }catch(err){
      this.setState({loading:false,dialog:false})
      this._snackbar.openSnack('Item not added in your card','error')
    }
  }

  updateQty = (quantity) => {
    this.setState({quantity})
  }

  render(){
    const { product, index, classes, className, colorSelector } = this.props;
    let imageSrc = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQc0xdbBLsixbYvXHrMqy3eXDKYfXQUbZWMig&usqp=CAU';
    let colors = product.colors ? product.colors:[
      {
        disabled: false,
        id: "red",
        image: { src: "https://via.placeholder.com/48x48/f44336?text=%20", alt: "red" },
        media: {
          full: [
            {
              alt: "Product 1",
              magnify: { height: 1200, width: 1200, src: "https://via.placeholder.com/1200x1200/f44336/ffffff?text=Product%201" },
              src: "https://via.placeholder.com/600x600/f44336/ffffff?text=Product%201"
            }
          ],
          thumbnail: { src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSV2xad0CRt03g90EcMwRYtuEg9zG80o7hs6Q&usqp=CAU", alt: "Product 1" },
          thumbnails: [
            {
              alt: "red",
              src: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSV2xad0CRt03g90EcMwRYtuEg9zG80o7hs6Q&usqp=CAU"
            }
          ]
        },
      },
      {
        disabled: false,
        id: "green",
        image: { src: "https://via.placeholder.com/48x48/4caf50?text=%20", alt: "green" },
        media: {
          full: [
            {
              alt: "Product 1",
              magnify: { height: 1200, width: 1200, src: "https://via.placeholder.com/1200x1200/4caf50/ffffff?text=Product%201" },
              src: "https://via.placeholder.com/600x600/4caf50/ffffff?text=Product%201"
            },
            {
              alt: "Product 1",
              magnify:{
              height: 800,width: 1200,src: "https://via.placeholder.com/1200x800/4caf50/ffffff?text=Product%201"},
              src: "https://via.placeholder.com/600x400/4caf50/ffffff?text=Product%201"
            }
          ],
          thumbnail: {src: "https://via.placeholder.com/400x400/4caf50/ffffff?text=Product%201", alt: "Product 1" },
          thumbnails: [
            {
              alt: "green",
              src: "https://via.placeholder.com/400x400/4caf50/ffffff?text=Product%201"
            }
          ]
        },
      }
    ];
    return (
      <div id={`item-${index}`} className={clsx(className, classes.root)}>
        <CommonSnackbar ref={ref => this._snackbar = ref} />

        <Dialog
              open={this.state.dialog}
              onClose={this.closeModal}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{product.name}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <Grid item xs={12}>
                        <Hbox>
                          <Label>QUANTITY:</Label>
                          <QuantitySelector
                            value={this.state.quantity}
                            onChange={this.updateQty}
                          />
                        </Hbox>
                      </Grid>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeModal} color="primary">
                  Cancel
                </Button>
                {/* <Button onClick={this.addUpdateCart} color="primary" autoFocus> 
                  Add To Cart
                </Button> */}

                <SpinnerButton variant="text" title="Add To Cart" handleButtonClick={this.addUpdateCart} loading={this.state.loading} color="primary" autoFocus/>

              </DialogActions>
            </Dialog>
            
        <Vbox alignItems="stretch">
          <ForwardThumbnail>
            <Link
              to={product.url}
              className={classes.link}
                     
            >
                <Image
                  className={classes.thumbnail}
                  src={imageSrc
                    // (product.color && product.color.media.thumbnail.src) ||
                    // (product.thumbnail && product.thumbnail.src)
                  }
                  alt={
                    imageSrc
                  }
                  alt={
                    imageSrc
                  }
                  optimize={{ maxWidth: 200 }}
                  lazy={index >= 4 && index < 20 ? 'ssr' : false}
                  aspectRatio={1}
                />
            </Link>
          </ForwardThumbnail>
          <div className={classes.info}>
            <Typography variant="subtitle1" className={classes.name}>
              {product.name}
            </Typography>
            {colorSelector && (
              <ProductOptionSelector
                options={colors}
                value={product.color}
                onChange={value => this.setState({ ...product, color: value })}
                optionProps={{
                  size: 'small',
                  showLabel: false,
                }}
              />
            )}
            
            <Typography className={classes.price}>{product.price} Rs.</Typography>
              <SpinnerButton title="Add To Cart" handleButtonClick={this.openModal} loading={this.state.loading} />
            {/* <Button variant="contained" color="primary" onClick={this.addUpdateCart}>Add To Cart</Button> */}
          </div>
        </Vbox>
      </div>
    )
  }
}

ProductItem.defaultProps = {
  colorSelector: true,
}

export default withStyles(styles)(ProductItem)