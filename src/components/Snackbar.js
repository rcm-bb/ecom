import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {withStyles} from '@material-ui/core/styles';
const styles = {
    error: {
        background: 'red'
    },
    success: {
        background: 'green'
    },
    
};

 class CommonSnackbar extends React.Component{
    constructor(){
        super();
        this.state = {
            open:false,
            type:'success',
            text:'success'
        }
    }

    handleClose = () => this.setState({open:false})

    openSnack = (text = 'success',type = 'success') => this.setState({open:true,text,type})

    render() {
        const {classes} = this.props;
        const {open,text,type} = this.state
        return (
            <div>
                 <Snackbar
                    anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                    }}
                    ContentProps={{
                        classes: {
                            root: classes[type]
                        }
                    }}
                    open={open}
                    autoHideDuration={3000}
                    onClose={this.handleClose}
                    message={text}
                    action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleClose}>
                        <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                    }
                    
                />
            </div>
        )
    }
}

export default withStyles(styles)(CommonSnackbar)

// export default function CommonSnackbar({text}) {

//   const [open, setOpen] = React.useState(false);

//   const handleClose = (event, reason) => {
//     if (reason === 'clickaway') {
//       return;
//     }
//     setOpen(false);
//   }; 

//   const openBar = () => setOpen(false);

//   console.log('defaultOpen',open)
//   return (

//       <Snackbar
//         anchorOrigin={{
//           vertical: 'bottom',
//           horizontal: 'left',
//         }}
//         open={open}
//         autoHideDuration={2000}
//         onClose={handleClose}
//         message={text}
//         action={
//           <React.Fragment>
//             <Button color="secondary" size="small" onClick={handleClose}>
//               {text}
//             </Button>
//             <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
//               <CloseIcon fontSize="small" />
//             </IconButton>
//           </React.Fragment>
//         }
//       />
//   );
// }
