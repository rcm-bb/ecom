import React, { useState, useRef, useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import VisibilitySensor from 'react-visibility-sensor' 
import { makeStyles } from '@material-ui/core/styles'
import PWAContext from './../PWAContext'
export const styles = theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 1,
    minWidth: 1,
  },
  image: {},
  fit: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'block',
    width: '100%',
    height: '100%',
  },
  contain: {
    '& img': {
      objectFit: 'fill',
      maxHeight: '100%',
      maxWidth: '100%',
    },
  },
  fill: {
    width: '100%',
    height: '100%',
    '& img': {
      display: 'block',
      objectFit: 'contain',
      maxHeight: '100%',
      maxWidth: '100%',
      width: '100%',
      height: '100%',
    },
  },
})

const useStyles = makeStyles(styles, { name: 'RSFImage' })
export default function Image({
  lazy,
  lazyOffset,
  notFoundSrc,
  height,
  width,
  fill,
  bind,
  contain,
  classes,
  className,
  aspectRatio,
  alt,
  src,
  amp,
  optimize,
  onChange,
  onSrcChange,
  onAltChange,
  value,
  ImgElement,
  ...imgAttributes
}) {
  function lazyLoad(visible) {
    if (!loaded && visible) {
      setLoaded(true)
    }
  }

  classes = useStyles({ classes })

  const { hydrating } = useContext(PWAContext) || {}
  const [loaded, setLoaded] = useState(lazy === false || (lazy === 'ssr' && !hydrating))
  const [primaryNotFound, setPrimaryNotFound] = useState(false)
  const ref = useRef()

  useEffect(() => {
    const img = ref.current
    if (img && img.complete && img.naturalWidth === 0) {
      setPrimaryNotFound(true)
    }
  }, [])

  if (src == null) return null

  contain = contain || aspectRatio

  if (primaryNotFound && notFoundSrc) {
    src = notFoundSrc
  }

  let result = (
    <div
      className={clsx(className, {
        [classes.root]: true,
        [classes.contain]: contain,
        [classes.fill]: fill,
      })}
    >
      {aspectRatio && <div style={{ paddingTop: `${aspectRatio * 100}%` }} />}
      {loaded && (
        <ImgElement
          ref={ref}
          src={src}
          key={src}
          height={height}
          width={width}
          alt={alt}
          className={clsx({
            [classes.image]: true,
            [classes.fit]: aspectRatio != null,
          })}
          {...imgAttributes}
          onError={() => setPrimaryNotFound(true)}
        />
      )}
    </div>
  )

  result = (
    <VisibilitySensor
      active={!loaded}
      onChange={lazyLoad}
      partialVisibility
      offset={{ top: -lazyOffset, bottom: -lazyOffset }}
    >
      {result}
    </VisibilitySensor>
  )

  return result
}

Image.propTypes = {
  src: PropTypes.string,
  notFoundSrc: PropTypes.string,
  aspectRatio: PropTypes.number,
  contain: PropTypes.bool,
  fill: PropTypes.bool,
  lazy: PropTypes.oneOf(['ssr', true, false]),
  lazyOffset: PropTypes.number,
}

Image.defaultProps = {
  contain: false,
  fill: false,
  lazy: false,
  lazyOffset: 100,
  ImgElement: 'img',
}
