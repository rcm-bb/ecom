import React from 'react';
import {connect} from 'react-redux';
import { AppBar } from '@material-ui/core';
import {Header} from './../Header';
import NavBar from './NavBar';
import {setMenu,setCartCount} from './../../store/actions'
import client from './../../utils/client'
import { logout, isAuthenticated } from '../../utils/auth';
import {withRouter} from 'react-router-dom'
import cookie from 'react-cookies'
import {isEmpty} from 'lodash'
class HeaderTop extends React.Component{
    constructor(){
        super();
        this.isAuth = isAuthenticated();
        let cartId = cookie.load('clid');
        this.cartId = !isEmpty(cartId) ? cartId:0;
    }

    async componentWillMount(){ 
        let url = this.isAuth ? 'pu/cartLineCount':`pu/cartLineCountForUnsignUser/${this.cartId}`;
        // let {objectMap} = await client.get('pu/categoriesRE')
        // console.log('objectMap.categories',objectMap.categories)
        // this.props.setMenu(objectMap.categories)
        // return 
        const menus = [
            {
                text:'Direct Seller',
                href:'/dc/kyc',
                items:[
                    {
                        text:'e-KYC Application for New Direct Seller',
                        href:'/dc/kyc'
                    },
                    {
                        text:'Application Form Status',
                        href:'/dc/kycapplication'
                    },
                    {
                        text:'Forgot Reference Number',
                        href:'/dc/forgotrefnumber'
                    },
                    {
                        text:'Forgot Reference Password',
                        href:'/dc/forgotrefpassword'
                    },
                    {
                        text:'Mobile Number Change',
                        href:'/static/media/MobileDeclaration.7f3bad2d.pdf'
                    },
                    {
                        text:'Message for Direct Seller',
                        href:'/dc/noticeboard'
                    },
                ]
            },
            {
                text:'World of RCM',
                href:'',
                items:[
                    {
                        text:'Marketing Plan',
                        href:'/worcm/marketingplan'
                    },
                    {
                        text:'Delivery Centers',
                        href:'/worcm/deliverycenter'
                    },
                    {
                        text:'Product Training Programs',
                        href:'/worcm/producttraining'
                    },
                    {
                        text:'visit Harit Sanjivani Website',
                        href:''
                    },
                    {
                        text:'Beauty Product Catalouge',
                        href:'/static/media/Beauty-Products-Catalogue-New.5d0afb84.pdf'
                    }
                ]
            },
            {
                text:'Media',
                href:'/mediagallery',
            },
            {
                text:'Contact Us',
                href:'/contactus',
            }
        ]
        this.props.setMenu(menus)
       try{
        let countRes = await client.post(url)
        this.props.setCartCount('equal',countRes.data ? countRes.data:0)
       }catch(error){
           console.log('error',error)
       }
    }

    onLogout = () => {
        logout();
        this.props.setCartCount('equal',0)
        this.props.history.push('/')
    }

    render(){
        const {menu} = this.props;
        return (
            <AppBar position="sticky" >
                <Header onLogout={this.onLogout}/>
                <NavBar tabs={menu} /> 
            </AppBar>
        );
    }
}
const mapStateToProps = (state) => ({
    menu: state.root.menu
});

const mapDispatchToProps = dispatch => ({
    setMenu: menu => dispatch(setMenu(menu)),
    setCartCount: (value,count) => dispatch(setCartCount(value,count))
})

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(HeaderTop));