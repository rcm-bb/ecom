import React, { memo } from 'react'
import NavTab from './../Nav/NavTab'
import NavTabs from './../Nav/NavTabs'
import Link from './../Link/Link'
import { Container, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {isEmpty, isElement} from 'lodash';

const useStyles = makeStyles(theme => ({
  container: {
    [theme.breakpoints.down('xs')]: {
      padding: 0,
    },
  },
  link: {
    display: 'block',
    marginTop: theme.spacing(2),
    '&:first-child': {
      marginTop: 0,
    },
    fontSize:14,
    fontWeight:600,
    paddingTop:2,
    paddingBottom:2,
    textDecoration:'none',
    textDecoration:'none'
  },
}))

function NavBar({ tabs,...props }) {
  const classes = useStyles()
  return (
    <Paper square elevation={0}>
      <Container maxWidth="lg" className={classes.container}>
      <NavTabs>

        {!isEmpty(tabs) && tabs.map((tab,i) => {
          return(
            <NavTab key={i} href={`${tab.href}`} as={`${tab.as}?title=${tab.text}`} label={tab.text}>
              {!isEmpty(tab.items) && 
              <div style={{ padding: 20 }}>
                { !isEmpty(tab.items) && tab.items.map((subcategory,index) => {
                  return (
                    <Link 
                    href={subcategory.href}
                    key={subcategory.as}
                    as={`${subcategory.as}?title=${subcategory.text}`}
                    className={classes.link}
                  >
                    {subcategory.text}
                  </Link>
                )
                })}
              </div>
              }
            </NavTab>
          )})}
      </NavTabs>
      </Container>
    </Paper>
  )
}

NavBar.defaultProps = {
  tabs: [],
}

export default memo(NavBar)
