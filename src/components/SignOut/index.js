import React, { Component } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';



class SignOut extends Component {
  render() {
    const { firebase } = this.props;
    
    return(
      <ListItem button onClick={firebase.doSignOut} aria-label="Sign Out">
        <ListItemText primary="Sign Out" />
      </ListItem>
    );
  }
}

export default (SignOut);
