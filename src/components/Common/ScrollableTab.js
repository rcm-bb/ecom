import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {isEmpty} from 'lodash';

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: 'white',
  },
}));

export default function ScrollableTabs({onTabChanged,tabs}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, tab) => {
    setValue(tab);
    onTabChanged(tab)
  };

  if(isEmpty(tabs)) return null;
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollabletabs"
        >
            {
               tabs.map((tab,index) => <Tab label={tab} {...a11yProps(index)} />)
            }
          
        </Tabs>
      </AppBar>
    </div>
  );
}

ScrollableTabs.defaultProps = {
    tabs:[],
    onTabChanged: () => null
}