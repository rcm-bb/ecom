import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import { isEmpty } from "lodash";
const AnyReactComponent = ({ text }) => <div>{text}</div>;

class GoogleMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      center: {
        lat: 59.95,
        lng: 30.33
      },
      key:"AIzaSyBEMUe7AHyh6nslGI--DuWHBGV0WUeyfZ0"
    };
  }
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };

  render() {
    
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: "100vh", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyC-ixlwpH6J9S8zPMHKuS1bDw-jMUMKZe8"}}
          defaultCenter={this.state.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent lat={74.6313} lng={25.3407} text="My Marker" />
        </GoogleMapReact>
      </div>
    );
  }
}

export default GoogleMap;
