import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from '@material-ui/core/styles';

const styles = () => ({
  root:{
    flex:1
  }
})

function DialogBox({open,title,children,onCancel,onOK,onClose,okButtonText,cancelButtonText,classes}) {
  return (
    <div>
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        open={open}
        onClose={onClose.bind(this)}
        scroll='paper'
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
        classes={{
          paper: classes.root
        }}
      >
        <DialogTitle id="scroll-dialog-title">{title}</DialogTitle>
        <DialogContent dividers={true}>
          {/* <DialogContentText
            id="scroll-dialog-description"
            tabIndex={-1}
            component="div"
          > */}
           {children}
          {/* </DialogContentText> */}
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel.bind(this)} color="primary">
            {cancelButtonText}
          </Button>
          <Button onClick={onOK.bind(this)} color="primary">
            {okButtonText}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

DialogBox.defaultProps = {
    open:false,
    title:'title',
    onClose: () => null,
    onCancel: () => null,
    onOK: () => null,
    children: null,
    cancelButtonText:'Cancel',
    okButtonText:'OK'
}

export default withStyles(styles)(DialogBox)