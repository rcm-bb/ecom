import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles,withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import client from './../../utils/client'
import {isEmpty,get} from 'lodash';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CancelIcon from '@material-ui/icons/Cancel';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}


function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort,headers } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {/* <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell> */}
        {headers.map((headCell,i) => (
          <TableCell
            key={i}
            align={'left'}
            // padding={i == 0 ? 'none' : 'default'}
            sortDirection={orderBy === headCell.value ? order : false}
          >
            <TableSortLabel
              // active={orderBy === headCell.id}
              direction={orderBy === headCell.value ? order : 'asc'}
              onClick={createSortHandler(headCell.value)}
            >
              {headCell.label}
              {orderBy === headCell.value ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = ({title,showFilter,onFilterClick,rightHeaderChildren,...props}) => {
  const classes = useToolbarStyles();
  const { numSelected } = props;
  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          {title}
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <>
          {
            rightHeaderChildren ? rightHeaderChildren:null
          }
        {/* {showFilter ?
          <Tooltip title="Filter list" onClick={onFilterClick.bind(this)}>
            <IconButton aria-label="filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
          :null
        } */}
        </>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.defaultProps = {
  title:'Table Title',
  showFilter:false,
  onFilterClick: () => null,
  rightHeaderChildren:null
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const styles = (theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  tableContainer:{
    marginTop:10
  }
});


class DataTable extends React.Component {
  
  constructor(){
    super();
    this.state = {
      order:'asc',
      orderBy:'calories',
      selected:[],
      page:0,
      dense:false,
      rowsPerPage:5,
      data:[],
      fetching:false
    }
  }

  componentDidMount(){
    this.fetch();
  }

  handlePucTypeName = type => {
    let typeName = "";
    if (type === "1") {
      typeName = "Wonder World PUC";
    } else if (type === "2" || type === "3") {
      typeName = "KeySoul Store";
    }
    return typeName;
  };

  async fetch(){
    if(!this.props.fetchURL) return null; 
    this.setState({fetching:true})
    let res = await client.post(this.props.fetchURL,this.props.filters);
    if(this.props.inData){
      return this.setState({data: res.data,fetching:false })
    }else{
      const {objectMap} = res;
      // console.log('objectMap else',objectMap,this.props.resField,objectMap[this.props.resField])
      this.setState({data:objectMap[this.props.resField],fetching:false});
    }
  }

  onFilterChanged = () => this.fetch();

  handleRequestSort = (event, property) => {
    const {orderBy,order} = this.state;
    const isAsc = orderBy === property && order === 'asc';
    this.setState({order:isAsc ? 'desc' : 'asc',orderBy:property})
  };
  
  handleSelectAllClick = (event) => {
    const {data} = this.state
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.name);
      return this.setState({selected:newSelecteds})
    }
    this.setState({selected:[]})
  };

  handleClick = (event, name) => {
    const {selected,} = this.state;
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({selected:newSelected});
  };

  handleChangePage = (event, newPage) => {
    this.setState({page:newPage})
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({page:0,rowsPerPage: parseInt(event.target.value, 10)})
  };

  getData(row,head,index){
    let value = get(row,head.value);
    if(head.type && head.type == 'date'){
      value = new Date(value).toLocaleDateString();
    }else if(head.type == 'index'){
      value = index + 1;
    }else if(head.type == 'puc'){
      value = this.handlePucTypeName(value)
    }else if(head.button){
      let button = <CheckBoxIcon />
      if(head.button == 'CancelIcon') button = <CancelIcon />
        value = (<IconButton color="primary" component="span" onClick={() => {
          this.props.onActionClick(row,head);
          console.log('this.props.onActionClick.bind(this,row,head) end')
        }}> {button} </IconButton>)
    }
    return value;
  }

  render(){
    const {classes,title,headCells,showFilter,onFilterClick,rightHeaderChildren,showTitleBar} = this.props;
    const {rowsPerPage,selected,data,page,order,orderBy,dense,fetching} = this.state;
    const isSelected = (name) => selected.indexOf(name) !== -1;
    const emptyRows =  rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <div className={classes.root}>
      <Paper className={classes.paper}>
        { showTitleBar && <EnhancedTableToolbar title={title} numSelected={selected.length} rightHeaderChildren={rightHeaderChildren} onFilterClick={onFilterClick.bind(this)} showFilter={showFilter}/> }
        <TableContainer className={classes.tableContainer}>
        {fetching && <LinearProgress /> }
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              headers={headCells}
            
            />
    
            <TableBody>
          
              {stableSort(data, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.value);
                  // const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={(event) => this.handleClick(event, row.value)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={index}
                      selected={isItemSelected}
                    >
                      {/* <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </TableCell> */}
                      {
                        headCells.map((head,ind) => {
                          return ( <TableCell align="left" key={ind}>{this.getData(row,head,index)}</TableCell> )
                        })
                      }
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
   
    </div>
    )
  }
}

DataTable.defaultProps = {
  headCells:[
    { value: 'created',label: 'Date',type:'date' },
    { value: 'time', label: 'Time' },
    { value: 'programVenue', label: 'Venue' },
    { value: 'programCity', label: 'City' },
    { value: 'programState.name', label: 'State' },
    { value: 'contactNo1', label: 'Phone No.' },
    { value: 'visitingLeader', label: 'Visiting Leader' },
  ],
  title:null,
  fetchURL:null,
  filters:{
    visitingLeader:'Hitesh',
    stateId:0,
    fromDate:''
  },
  resField:'programList',
  showFilter:false,
  onFilterClick: () => null,
  rightHeaderChildren:null,
  inData:false,
  onActionClick:() => null,
  showTitleBar: true
}

export default withStyles(styles)(DataTable)
