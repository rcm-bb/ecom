import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    minHeight:140
  },
  details: {
    display: 'flex', 
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

export default function MediaControlCard({item}) {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={12} md={4}>
    <Card className={classes.root}>
        <CardMedia
        className={classes.cover}
        image="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCqa3f0HXO1hG5NsCb9I0wRBiLlFfiegaZJg&usqp=CAU"
        title="Live from space album cover"
      />
      
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="subtitle2">
            {item.material.name}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            Price: Rs. {item.rate}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            Quantity: {item.confirmQuantity}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            Amount: Rs. {item.totalAmount}
          </Typography>
        </CardContent>
        <div className={classes.controls}>
         
        </div>
      </div>
      
    </Card>
    </Grid>
  );
}
