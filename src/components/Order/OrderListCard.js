import React from 'react';
import {withStyles} from '@material-ui/core/styles'
import { Grid } from '@material-ui/core'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const styles = () => ({
    root: {
        display: 'flex',
        marginBottom:20,
        cursor:'pointer'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
})

const OrderListCard = ({classes,onClick,order}) => (
    <Grid item xs={12} sm={12} md={12}>
    <Card className={classes.root} onClick={onClick.bind(this,order)}>
    <div className={classes.details}>
      <CardContent className={classes.content}>
        <Typography variant="subtitle2" color="textSecondary">
          ORDER PLACED:  {new Date(order.created).toDateString()}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">
         ORDER: {order.orderId}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">
         ORDER ITEMS: {order.orderLine.length}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">
          TOTAL:  Rs. {order.totalAmmount}
        </Typography>
      </CardContent>

    </div>
  </Card>
  </Grid>
);

OrderListCard.defaultProps = {
    onClick:() => null,
    order: {}
}
export default withStyles(styles)(OrderListCard)