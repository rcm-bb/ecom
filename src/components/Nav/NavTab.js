import React, { useState, useCallback, useRef, useEffect } from 'react'
import Link from './../Link/Link'
import { makeStyles } from '@material-ui/core/styles'
import { Hidden, Fade, Tab, Popover, Paper } from '@material-ui/core'
import PropTypes from 'prop-types'
const styles = theme => ({
  popover: {
    pointerEvents: 'none',
    width:'20%'
  },
  tab: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  link: {
    textDecoration: 'none !important',
    color: 'inherit',
    fontWeight: 600,
    fontSize:14
  },
  /**
   * Styles applied to the root element of the `Tab`'s `TouchRippleProps` classes.
   */
  ripple: {
    zIndex: 2,
    textDecoration: 'none !important',

  },
  /**
   * Styles applied to the Popover element's `Paper` element for desktop users.
   */
  paper: {
    width:'80%',
    pointerEvents: 'all',
    paddingTop: 2, // we add 2 pixels of transparent padding and move the menu up two pixels to cover the tab indicator
    marginTop: -2, // so that the user doesn't temporarily mouse over the indicator when moving between the tab and the menu, causing the menu to flicker.
    background: 'transparent',
    boxShadow: '0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0.14)',
  },
  /**
   * Styles applied to the Popover element's `Paper` element for desktop users.
   */
  innerPaper: {
    borderTop: `1px solid ${theme.palette.divider}`,
  },
})

const useStyles = makeStyles(styles, { name: 'RSFNavTab' })

function NavTab({ classes, href, as, prefetch, children, ...props }) {
  classes = useStyles({ classes })

  const [overTab, setOverTab] = useState(false)
  const [overMenu, setOverMenu] = useState(false)
  const [anchorEl, setAnchorEl] = useState(null)
  const [focused, setFocused] = useState(false)

  const showMenu = useCallback(event => {
    setOverTab(true)
    setAnchorEl(event.currentTarget)
  }, [])

  const hideMenu = useCallback(() => setTimeout(() => setOverTab(false)), [])
  const leaveMenu = useCallback(() => setTimeout(() => setOverMenu(false)), [])
  const enterMenu = useCallback(() => setOverMenu(true), [])
  const menuItemBlurPending = useRef(false)

  // accessibility: open the menu when the user presses enter with the tab focused
  const handleEnterKeyDown = useCallback(e => {
    if (e.key === 'Enter') {
      e.preventDefault()
      setAnchorEl(e.currentTarget)
      setFocused(true)
    }
  }, [])

  const handleMenuItemFocus = useCallback(() => {
    menuItemBlurPending.current = false
  }, [])

  const handleMenuItemBlur = useCallback(() => {
    menuItemBlurPending.current = true

    setTimeout(() => {
      if (menuItemBlurPending.current) {
        setFocused(false)
      }
    })
  }, [])

  const open = overTab || overMenu || focused


  return (
    <>
      <Link
        className={classes.link}
        href={href}
        as={as}
        onClick={hideMenu} // Does not work in dev, because next consumes focus in production everything is good
        onMouseEnter={showMenu}
        onMouseLeave={hideMenu}
        prefetch={prefetch}
      >
        <Tab
          onKeyDown={handleEnterKeyDown}
          classes={{ root: classes.tab }}
          aria-haspopup={children != null}
          aria-expanded={open}
          {...props}
          TouchRippleProps={{
            classes: {
              root: classes.ripple,
            },
          }}
        />
      </Link>
      {!children ? null : (
        <Hidden xsDown>
          <Popover
            open={open}
            className={classes.popover}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            TransitionComponent={Fade}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            PaperProps={{
              onMouseEnter: enterMenu,
              onMouseLeave: leaveMenu,
              onClick: leaveMenu,
              square: true,
              className: classes.paper,
            }}
          >
            <Paper
              className={classes.innerPaper}
              onBlurCapture={handleMenuItemBlur}
              onFocusCapture={handleMenuItemFocus}
              square
            >
              {children}
            </Paper>
          </Popover>
        </Hidden>
      )}
    </>
  )
}

NavTab.propTypes = {
  /**
   * The link path
   */
  as: PropTypes.string.isRequired,
  /**
   * The next.js route pattern
   */
  href: PropTypes.string.isRequired,
  /**
   * Override or extend the styles applied to the component. See [CSS API](#css) below for more details.
   */
  classes: PropTypes.object,
}

export default React.memo(NavTab)