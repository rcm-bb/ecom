import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { withStyles } from '@material-ui/core/styles';
import clinet from './../utils/client'
const styles = () =>  ({
  option: {
    fontSize: 15,
    '& > span': {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

class CountrySelect extends React.Component {
    constructor(){
        super();
        this.state = {
            data:[]
        }
    }

    async componentDidMount(){
        const {fetchURL,mapField} = this.props;
        let {objectMap} = await clinet.post(fetchURL);
        let data = objectMap ? objectMap[mapField]:[];
        this.setState({data})
    }

    onChange(item){
        let {itemValue} = this.props
        this.props.onChange(this.props.name,{[itemValue]:item[itemValue],name:item.name})
    }

    render(){
        const {classes,itemLable,label,value,disabled} = this.props
        return (
            <Autocomplete
            id="country-select-demo"
            options={this.state.data}
            classes={{
                option: classes.option,
            }}
            onChange={(event, newValue) => this.onChange(newValue)}
            autoHighlight
            getOptionLabel={(option) => option[itemLable]}
            renderOption={(option) => (
                <React.Fragment>
                {option[itemLable]}
                </React.Fragment>
            )}
            value={value}
            renderInput={(params) => (
                <TextField
                {...params}
                label={label}
                inputProps={{
                    ...params.inputProps,
                    autoComplete: 'new-password'
                }}
                disabled={disabled}
                />
            )}
            />
        )
    }
}

CountrySelect.defaultProps = {
    itemLable:'name',
    itemValue:'countryId',
    fetchURL:'/pu/getCountry',
    mapField:'countryListDto',
    label:'Choose a Country',
    onChange: () => null,
    disabled:false
}

export default  withStyles(styles)(CountrySelect)