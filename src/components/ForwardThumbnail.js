import React, { useCallback, useRef, useContext, useEffect } from 'react'
import PWAContext from './PWAContext'
  
export default function ForwardThumbnail({ children }) {
  const ref = useRef(null)
  let context = useContext(PWAContext)
  const srcRef = useRef(null)

  const setSrcRef = useCallback(() => {
    if (ref.current.querySelector('img')) {
      srcRef.current = ref.current.querySelector('img').getAttribute('src')
    }
  }, [])

  useEffect(setSrcRef, [children])

  const handleClick = useCallback(() => {
    if (!srcRef.current) {
      setSrcRef()
    }
    context = context ? context : {thumbnail:{}}
    context.thumbnail.current = { src: srcRef.current }
  }, [])

  return (
    <div ref={ref} onClick={handleClick}>
      {children}
    </div>
  )
}