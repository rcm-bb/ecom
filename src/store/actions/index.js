import {SET_MENU,SET_SLIDER,SET_CART_COUNT} from './../constants'

export function setSlider(sliders= {}){
    return {
        type:SET_SLIDER,
        sliders
    }
}

export function setMenu(menu=[]){
    return {
        type:SET_MENU,
        menu
    }
}

export function setCartCount(value,count = 0){
    return {
        type:SET_CART_COUNT,
        value,
        count
    }
}