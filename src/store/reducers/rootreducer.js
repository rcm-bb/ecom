import { SET_MENU,SET_SLIDER,SET_CART_COUNT } from "../constants"

const initialState = {
    menu:[],
    sliders:[
        {
            image:'https://secureservercdn.net/160.153.138.177/x2z.90e.myftpupload.com/wp-content/uploads/2019/07/HB3.jpg?time=1593421342',
            content:'First Slider'
        },
        {
            image: 'https://secureservercdn.net/160.153.138.177/x2z.90e.myftpupload.com/wp-content/uploads/2019/07/HB1.jpg?time=1593421342',
            content: '2nd slider'
        },
        {
            image: 'https://secureservercdn.net/160.153.138.177/x2z.90e.myftpupload.com/wp-content/uploads/2019/07/HB2.jpg?time=1593421342',
            content: 'third Slider'
        }
      ],
      cartCount:0
}

export default function rootReducer(state = initialState,action){
    switch(action.type){
        case SET_SLIDER:
            return {
                ...state,
                sliders:action.sliders
            }
        case SET_MENU:
            return {
                ...state,
                menu:action.menu
            }
        case SET_CART_COUNT:
            let cartCount = state.cartCount;
            if(action.value == 'increase') cartCount = state.cartCount+1;
            else if(action.value == 'decrease') cartCount = state.cartCount-1;
            else if(action.value == 'equal') cartCount = action.count;
            return {
                ...state,
                cartCount
            }
        default: 
        return state;
    }
}